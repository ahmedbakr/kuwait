<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="google" content="notranslate" />
    <title>{{$meta_title}}</title>
    <meta name="description" content="<?php echo $meta_desc ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords ?>"/>

    <?php if(isset($edit_index_page->icon)): ?>
        <link rel="shortcut icon" href="{{get_image_or_default($edit_index_page->icon->path)}}" type="image/x-icon">
    <?php endif; ?>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="{{url("public_html/front")}}/apple-icon-180x180.png" rel="apple-touch-icon">
    <link href="{{url("public_html/front")}}/favicon.ico" rel="icon">


    <link href="{{url("public_html/front")}}/main.82cfd66e.css" rel="stylesheet"></head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <script src="<?= url('public_html/jscode/config.js') ?>"></script>
    <script src="<?= url('public_html/jscode/homepage.js') ?>"></script>
    <script src="<?= url('public_html/jscode/general_load_more.js') ?>"></script>
    <script src="<?= url('public_html/jscode/admin/utility.js') ?>"></script>

<body>

<!-- hidden csrf -->
<input type="hidden" class="csrf_input_class" value="{{csrf_token()}}">
<!-- /hidden csrf -->
<!-- hidden base url -->
<input type="hidden" class="url_class" value="<?= url("/") ?>">
<!-- /hidden base url -->
<input type="hidden" class="lang_url_class" value="<?= $lang_url_segment ?>">

<!-- Add your content of header -->
<header class="">
    <div class="navbar navbar-default visible-xs">
        <button type="button" class="navbar-toggle collapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="{{url($lang_url_segment)}}" class="navbar-brand">اسم الموقع</a>
    </div>

    <nav class="sidebar">
        <div class="navbar-collapse" id="navbar-collapse">
            <div class="site-header hidden-xs">
                <a class="site-brand" href="{{url($lang_url_segment)}}" title="">
                        <img class="img-responsive site-logo" style="width: 100px;"
                             src="{{get_image_or_default($edit_index_page->logo->path)}}"
                             alt="{{$edit_index_page->logo->alt}}" title="{{$edit_index_page->logo->title}}" />
                    {{show_content($edit_index_page,"website_name")}}
                </a>
                <p>{{show_content($edit_index_page,"website_info")}}</p>
            </div>
            <ul class="nav">
                <li>
                    <a href="{{url($lang_url_segment)}}" title="">{{show_content($edit_index_page,"menu_homepage")}}</a>
                </li>
                <?php if(count($menu_pages)): ?>
                    <?php foreach($menu_pages as $key => $page): ?>
                    <li>
                        <a href="{{url($lang_url_segment."/".urlencode($page->page_slug))}}">
                            {{$page->page_title}}
                        </a>
                    </li>
                    <?php endforeach; ?>
                <?php endif; ?>
                <li>
                    <a href="{{url($lang_url_segment."/contact")}}">{{show_content($edit_index_page,"menu_contact")}}</a>
                </li>

            </ul>

            <nav class="nav-footer">
                <p class="nav-footer-social-buttons">
                    <?php if(!empty($edit_index_page->fb_link)): ?>
                        <a class="fa-icon" href="{{$edit_index_page->fb_link}}" target="_blank" title="تابعنا علي الفيسبوك">
                            <i class="fa fa-facebook"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(!empty($edit_index_page->gplus_link)): ?>
                        <a class="fa-icon" href="{{$edit_index_page->gplus_link}}" target="_blank" title="تابعنا علي جوجل بلس">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(!empty($edit_index_page->instagram_link)): ?>
                        <a class="fa-icon" href="{{$edit_index_page->instagram_link}}" target="_blank" title="تابعنا علي انستجرام">
                            <i class="fa fa-instagram"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(!empty($edit_index_page->youtube_link)): ?>
                        <a class="fa-icon" href="{{$edit_index_page->youtube_link}}" target="_blank" title="تابعنا علي اليوتيوب">
                            <i class="fa fa-youtube"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(!empty($edit_index_page->twitter_link)): ?>
                        <a class="fa-icon" href="{{$edit_index_page->twitter_link}}" target="_blank" title="تابعنا علي تويتر">
                            <i class="fa fa-twitter"></i>
                        </a>
                    <?php endif; ?>
                </p>
                <p>
                    © All Rights Reserved to
                    <br>
                    <a href="#">BTM-Team</a>
                </p>
            </nav>
        </div>
    </nav>
</header>

<main class="" id="main-collapse">

    <!-- Add your site or app content here -->
    @yield('subview')

    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            masonryBuild();
        });
    </script>

</main>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        navbarToggleSidebar();
        navActivePage();
    });

    $(function () {
       if ($('.hero-full-wrapper').eq(1).length > 0)
       {
           $('.hero-full-wrapper').eq(1).remove();
       }
    });

</script>
<script type="text/javascript" src="{{url("public_html/front")}}/main.85741bff.js"></script></body>


<?php if(false): ?>
<body class="header-fixed">

    <div class="wrapper">
        <div class="header-v5 header-static">
            <!-- Topbar v3 -->
            <div class="topbar-v3">
                <div class="search-open">
                    <div class="container">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="search-close"><i class="icon-close"></i></div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <!-- Topbar Navigation -->
                            <ul class="left-topbar">

                            </ul><!--/end left-topbar-->
                        </div>

                    </div>
                </div><!--/container-->
            </div>
            <!-- End Topbar v3 -->
            <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?php if(isset($edit_index_page->logo)): ?>
                        <a class="navbar-brand" href="{{url($lang_url_segment."/")}}">
                            <img src="{{get_image_or_default($edit_index_page->logo->path)}}" alt="{{$edit_index_page->logo->alt}}" title="{{$edit_index_page->logo->title}}" />
                        </a>
                        <?php endif; ?>

                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Shopping Cart -->

                        <!-- End Shopping Cart -->
                        <!-- Nav Menu -->
                        <ul class="nav navbar-nav">
                            <!-- Pages -->
                            <li>
                                <a href="{{url($lang_url_segment."/")}}">{{show_content($general_static_keywords,"homepage")}}</a>
                            </li>



                            <li>
                                <a href="{{url($lang_url_segment."/contact")}}">{{show_content($edit_index_page,"contact_menu_txt")}}</a>
                            </li>

                        </ul>
                        <!-- End Nav Menu -->
                    </div>
                </div>
            </div>
            <!-- End Navbar -->
        </div>


<?php endif; ?>
