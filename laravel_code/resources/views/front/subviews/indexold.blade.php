@extends('front.main_layout')

@section('subview')

    @include('front.main_components.slider')

    <?php if(count($parent_cats)): ?>
    <div class="packeage">
        <div class="row">


        <?php foreach($parent_cats as $key => $parent_cat): ?>
            <div class="col s6 m4 l2 padd-right padd-left">
                <div class="item-pack">
                    <img width="40px" src="{{get_image_or_default($parent_cat->small_img_path)}}" alt="{{$parent_cat->small_img_alt}}"
                            title="{{$parent_cat->small_img_title}}"/>
                    <a href="{{url($lang_url_segment."/".urlencode($parent_cat->cat_slug))}}">
                        {{$parent_cat->cat_name}}
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>




    <div class="container">
        <div class="row">
            <div class="col s12 m12 l9 padd-left padd-right">

                <?php if(count($homepage_last_trip)): ?>

                    <div class="col s12 m12 l12">
                        <div class="tit-main color-2">
                            <h3>{{show_content($edit_index_page,"latest_tours_header")}}</h3>
                        </div>
                    </div>

                    <div class="col s12 m12 l12 padd-left padd-right">
                        <div class="owl-carousel">
                            <?php foreach($homepage_last_trip as $key => $trip_obj): ?>
                                <div class="col s12 m12 l12 center">
                                    @include('blocks.homepage_trip_block')
                                </div>	<!--------- end item ---------->
                            <?php endforeach; ?>
                        </div><!--------- end owl-carousel ---------->
                    </div>

                <?php endif; ?>


                <?php if(isset($edit_index_page->video_thumbnail)): ?>
                <div class="col s12 m12 l12">
                    <div class="vedio-home">
                        <div class="img-vedio">
                            <img src="{{get_image_or_default($edit_index_page->video_thumbnail->path)}}"
                                title="{{$edit_index_page->video_thumbnail->title}}" alt="{{$edit_index_page->video_thumbnail->alt}}"/>
                            <a href="#video_thumbnail_modal"><i class="material-icons dp48">play_circle_outline</i></a>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <!-- Modal Trigger -->


            </div>


            <div class="col s12 m12 l3 padd-left padd-right">
                <div class="col s12 m12 l12">
                    <div class="search-site center color-2">
                        <h3>{{show_content($edit_index_page,"search_header")}}</h3>
                        <form action="{{url("/search_for_trip")}}" method="get">
                            <input id="search" type="search" name="search_keyword" class="validate">
                            <button type="submit" class="waves-effect waves-light btn indigo color-1">{{show_content($edit_index_page,"search_btn")}}</button>
                        </form>
                    </div>

                    <div class="build-your-trip center color-2 contact_us_parent_div">
                        <h3>{{show_content($edit_index_page,"build_your_trip_header")}}</h3>
                        <p>{{show_content($edit_index_page,"build_your_trip_desc")}}</p>
                        <input type="hidden" class="msg_type" id="msg_type" value="build_trip">
                        <input type="text" placeholder="{{show_content($support,"form_name")}}" id="name" class="validate">
                        <input type="email" placeholder="{{show_content($support,"form_email")}}" id="email" class="validate">
                        <input type="tel" placeholder="{{show_content($support,"form_phone")}}" id="phone" class="validate">
                        <textarea id="msg" placeholder="{{show_content($support,"form_msg")}}" class="materialize-textarea"></textarea>
                        <div class="col s12 m12 l12 display_msgs"></div>
                        <a class="waves-effect waves-light btn indigo color-1 contact_us_btn">{{show_content($support,"form_btn")}}</a>
                    </div>
                </div>
            </div>


        </div>
    </div>


    <?php if(count($homepage_trips)): ?>

        <div class="container">
            <div class="row">

                <div class="flexslider">
                    <ul class="slides">
                        <?php foreach($homepage_trips as $key => $trip_obj): ?>
                            <li>
                                @include('blocks.homepage_trip_block')
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
        </div>

    <?php endif; ?>


    <div class="container">
        <div class="row">
            <div class="col s12 m12 l4">
                <div class="follwo-facebook">
                    <h3>{{show_content($edit_index_page,"follow_facebook_header")}}</h3>
                    <div class="box-share">
                        {!! show_content($edit_index_page,"follow_facebook_script") !!}
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <?php if(isset($edit_index_page->related_sites_text)&&is_array($edit_index_page->related_sites_text) && count($edit_index_page->related_sites_text)): ?>
                <div class="related">
                    <h3>{{show_content($edit_index_page,"related_sites_header")}}</h3>
                    <div class="box-related">
                        <?php foreach($edit_index_page->related_sites_text as $key => $related_site): ?>
                            <a href="{{$edit_index_page->related_sites_url[$key]}}">{{$related_site}}</a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="col s12 m12 l4">
                <div class="Currencies">
                    <h3>{{show_content($edit_index_page,"internation_currencies_header")}}</h3>
                    <div class="box-Currencies">
                        <h4>{{show_content($edit_index_page,"internation_currencies_desc")}}</h4>
                        <?php if(is_array($converted_currencies) && count($converted_currencies)): ?>
                            <table>
                                <tbody>
                                    <?php foreach($converted_currencies as $key => $value): ?>
                                        <tr>
                                            <td><img src="{{get_image_or_default($value->cur_img_path)}}" /></td>
                                            <td>{{$value->cur_to}}</td>
                                            <td>{{($egp_to_usd / $value->cur_rate )}}</td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
