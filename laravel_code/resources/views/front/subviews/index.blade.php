@extends('front.main_layout')

@section('subview')


    <div class="hero-full-wrapper">
        <div class="grid">

            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>

            <?php if(count($homepage_pages)): ?>
                <?php foreach($homepage_pages as $key => $page): ?>
                    <div class="grid-item">
                        <img class="img-responsive"
                             alt="{{$page->small_img_alt}}"
                             title="{{$page->small_img_title}}"
                             src="{{get_image_or_default($page->small_img_path)}}">
                        <a href="{{url($lang_url_segment."/".urlencode($page->page_slug))}}" class="project-description">
                            <div class="project-text-holder">
                                <div class="project-text-inner">
                                    <h3>{{$page->page_title}}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>


        </div>
    </div>


    <?php if(false): ?>

    <div class="container content-md">
        <!--=== Illustration v1 ===-->
        <?php if(isset($edit_index_page->slider4->imgs) &&
            is_array($edit_index_page->slider4->imgs) && count($edit_index_page->slider4->imgs)): ?>
        <div class="row margin-bottom-60">
            <?php foreach($edit_index_page->slider4->imgs as $key => $img_obj): ?>
                <div class="col-md-6 md-margin-bottom-30">
                    @include('blocks.homepage_first_section_block')
                </div>
            <?php endforeach; ?>

        </div>
        <?php endif; ?>

        <?php if(count($homepage_trips)): ?>
        <div class="heading heading-v1 margin-bottom-20">
            <h2>{{show_content($edit_index_page,"popular_packages")}}</h2>
            <p></p>
        </div>

        <div class="illustration-v2 margin-bottom-60">
            <div class="customNavigation margin-bottom-25">
                <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
            </div>

            <ul class="list-inline owl-slider">
                <?php foreach($homepage_trips as $key => $trip_obj): ?>
                <li class="item">
                    @include('blocks.homepage_trip_block')
                </li>
                <?php endforeach; ?>

                <div class="owl-controls clickable">
                    <div class="owl-pagination">
                        <div class="owl-page active"><span class=""></span></div>
                        <div class="owl-page"><span class=""></span></div>
                    </div>
                </div>
            </ul>
        </div>
        <?php endif; ?>


        <?php if(isset($edit_index_page->slider3->imgs) &&
            is_array($edit_index_page->slider3->imgs) && count($edit_index_page->slider3->imgs)): ?>

        <div class="row margin-bottom-50">
            <?php foreach($edit_index_page->slider3->imgs as $key => $img_obj): ?>
            @include('blocks.homepage_country_block')
            <?php endforeach; ?>
        </div>

        <?php endif; ?>

        <?php if(count($homepage_last_trip)): ?>
        <div class="heading heading-v1 margin-bottom-40">
            <h2>{{show_content($edit_index_page,"latest_packages")}}</h2>
        </div>

        <!--=== Illustration v2 ===-->
        <div class="row illustration-v2">

            <?php foreach($homepage_last_trip as $key => $trip_obj): ?>
                <div class="col-md-3 col-sm-6 md-margin-bottom-30">
                    @include('blocks.homepage_trip_block')
                </div>
            <?php endforeach; ?>

        </div>

        <?php endif; ?>
        <!--=== End Illustration v2 ===-->
    </div>

    <?php endif; ?>

@endsection
