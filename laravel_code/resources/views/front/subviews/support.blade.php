@extends('front.main_layout')

@section('subview')

    <div class="hero-full-wrapper">
        <div class="grid">

            <div id="map" class="map">
                {!! show_content($support,"map") !!}
            </div><!---/map-->

            <!--=== Content Part ===-->
            <div class="row margin-bottom-30" style="direction: rtl;">

                <div class="col-md-4">
                    <!-- Contacts -->
                    <?php if(count($support->contact_data_label)): ?>
                    <div class="headline"><h2>{{show_content($support,"sidebar_header1")}}</h2></div>
                    <ul class="list-unstyled who margin-bottom-30">

                        <?php foreach($support->contact_data_label as $key => $label): ?>
                        <li>
                            <a href="{{$support->contact_data_url[$key]}}">{!! $label !!} {{$support->contact_data_value[$key]}}</a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>


                <!-- Business Hours -->
                    <?php if(count($support->sidebar_section2_data_label)): ?>
                    <div class="headline"><h2>{{show_content($support,"sidebar_header2")}}</h2></div>
                    <ul class="list-unstyled margin-bottom-30">

                        <?php foreach($support->sidebar_section2_data_label as $key => $label): ?>
                        <li><strong>{!! $label !!} </strong>{{$support->sidebar_section2_data_value[$key]}}</li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>

                </div><!--/col-md-3-->

                <div class="col-md-8 mb-margin-bottom-30 contact_us_parent_div">
                    <div class="headline"><h2>{{show_content($support,"header")}}</h2></div>
                    <p>{{ show_content($support,"form_header") }}</p><br/>

                    <fieldset class="no-padding">

                        <div class="row sky-space-20">
                            <div class="col-md-6 col-md-offset-0">
                                <label>{{show_content($support,"form_name")}} <span
                                            class="color-red">*</span></label>
                                <div>
                                    <input type="text" name="name" id="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-0">
                                <label>{{show_content($support,"form_email")}} <span
                                            class="color-red"></span></label>
                                <div>
                                    <input type="text" name="email" id="email" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-0">
                                <label>{{show_content($support,"form_phone")}} <span
                                            class="color-red"></span></label>
                                <div>
                                    <input type="text" name="email" id="phone" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-0">
                                <label>{{show_content($support,"form_title")}} <span
                                            class="color-red"></span></label>
                                <div>
                                    <input type="text" name="email" id="title" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-0">
                                <label>{{show_content($support,"form_msg")}} <span
                                            class="color-red">*</span></label>
                                <div>
                                    <textarea rows="8" name="message" id="msg" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <input type="hidden" class="msg_type" id="msg_type" value="support">
                        <p>
                            <button type="submit" class="btn btn-primary btn-lg contact_us_btn">{{show_content($support,"form_btn")}}</button>
                        </p>
                    </fieldset>

                    <div class="message">
                        <p class="display_msgs"></p>
                    </div>
                </div><!--/col-md-9-->

            </div><!--/row-->
            <!--=== End Content Part ===-->

        </div>
    </div>

@endsection