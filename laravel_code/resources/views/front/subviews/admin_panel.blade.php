@extends('front.main_layout')

@section('subview')


    {!! \Session::get("msg") !!}

    <form method="POST" action="{{url('/admin_panel')}}">
        <div class="log-input">
            <div class="log-input-left">
                <input type="text" class="user input-field" name="email"/>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="log-input">
            <div class="log-input-left">
                <input type="password" class="lock" name="password"/>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="signin-rit">
            <span class="checkbox1">
                 <label class="checkbox">
                     <input type="checkbox" name="remember" checked="">Remember Me ?
                 </label>
            </span>
        </div>
        {{csrf_field()}}
        <input type="submit" class="btn" value="Login to your account">
    </form>

@endsection
