@extends('front.main_layout')

@section('subview')

    <div class="hero-full-wrapper">
        <div class="grid">

            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-offset-2 col-md-8 col-xs-12" style="text-align: center;">

                    <div class="section-container-spacer">
                        <h1>{{$page_data->page_title}}</h1>
                    </div>

                    <div class="section-container-spacer">
                        <p>
                            <img class="img-responsive"
                                 alt="{{$page_data->big_img_alt}}"
                                 title="{{$page_data->big_img_title}}"
                                 src="{{get_image_or_default($page_data->big_img_path)}}">
                        </p>
                    </div>

                    <p> {!! $page_data->page_body !!}</p>
                </div>

            </div>

        </div>
    </div>
@endsection