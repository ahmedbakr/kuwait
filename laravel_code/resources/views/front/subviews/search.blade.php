@extends('front.main_layout')

@section('subview')



    <div class="row">
        <nav class="color-2">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="{{url($lang_url_segment."/")}}" class="breadcrumb">{{show_content($general_static_keywords,"homepage")}}</a>
                    <a class="breadcrumb">{{show_content($search_page,"search_header")}}</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="container">
        <div class="row">

            <div class="col s12 m12 l12">
                <?php foreach($trips as $key=>$trip_obj): ?>
                @include("blocks.sub_cat_trip_block")
                <?php endforeach;?>
            </div>

            <div class="col s12 m12 l12 center">
                {{$trips_pagination->links()}}
            </div>



        </div>
    </div>


@endsection