@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            {{$page_type}}
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="    overflow-x: scroll;">

                <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>الصورة</td>
                        <td>الأسم</td>
                        <td>زيارة الصفحه</td>
                        <td>مشاهدة في الرئيسية</td>
                        <td>مشاهدة في القائمة</td>
                        <td>إخفاء من الموقع</td>
                        <td>تعديل</td>
                        <td>مسح</td>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($pages as $key => $page): ?>
                        <?php
                            $img_url = url('public_html/img/no_img.png');
                            if (file_exists($page->small_img_path))
                            {
                                $img_url = url($page->small_img_path);
                            }
                            $url=url("/$page->page_slug");

                        ?>

                    <tr id="row<?= $page->page_id ?>">
                        <td><?= $key+1; ?></td>

                        <td><img src="<?= url($img_url) ?>" width="50"></td>
                        <td><?= $page->page_title ?></td>
                        <td>
                            <a href="<?=$url  ?>">
                                زيارة
                            </a>
                        </td>

                        <td>
                            <?php
                            echo generate_multi_accepters(
                                $accepturl="",
                                $item_obj=$page,
                                $item_primary_col="page_id",
                                $accept_or_refuse_col="show_in_homepage",
                                $model='App\models\pages\pages_m',
                                $accepters_data=["0"=>"لا","1"=>"نعم"]
                            );
                            ?>
                        </td>
                        <td>
                            <?php
                            echo generate_multi_accepters(
                                $accepturl="",
                                $item_obj=$page,
                                $item_primary_col="page_id",
                                $accept_or_refuse_col="show_in_menu",
                                $model='App\models\pages\pages_m',
                                $accepters_data=["0"=>"لا","1"=>"نعم"]
                            );
                            ?>
                        </td>

                        <td>
                            <?php
                                echo generate_multi_accepters(
                                    $accepturl="",
                                    $item_obj=$page,
                                    $item_primary_col="page_id",
                                    $accept_or_refuse_col="hide_page",
                                    $model='App\models\pages\pages_m',
                                    $accepters_data=["1"=>"نعم","0"=>"لا"]
                                );
                            ?>
                        </td>
                        <td><a href="<?= url("admin/pages/save_page/$page->page_type/$page->page_id") ?>"><span class="label label-info">تعديل <i class="fa fa-edit"></i></span></a></td>
                        <td><a href='#' class="general_remove_item" data-tablename="App\models\pages\pages_m" data-deleteurl="<?= url("/admin/pages/remove_page") ?>" data-itemid="<?= $page->page_id ?>"><span class="label label-danger">مسح <i class="fa fa-remove"></i></span></a></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>

                </table>

            </div>
        </div>
    </div>



@endsection
