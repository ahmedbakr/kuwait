@extends('admin.main_layout')

@section('subview')

    <input type="hidden" class="all_langs_titles" value="{{json_encode(convert_inside_obj_to_arr($all_langs,"lang_title"))}}">

	<script src="{{url("/public_html/tinymce/tinymce.min.js")}}" type="text/javascript"></script>
	<script>
		tinymce.init({
			selector: '.tinymce'
		});
	</script>


	<style>
		hr{
			width: 100%;
			height:1px;
		}
		.select_related_pages{
			width: 50%;
		}

		.select_related_sites{
			width: 50%;
		}
        .hide_input{
            display: none;
        }
        .selected_trip{
            margin: 5px;
        }
	</style>



	<?php

		if (count($errors->all()) > 0)
		{
			$dump = "<div class='alert alert-danger'>";
			foreach ($errors->all() as $key => $error)
			{
				$dump .= $error." <br>";
			}
			$dump .= "</div>";

			echo $dump;
		}


		if (isset($success)&&!empty($success)) {
				echo $success;
		}

		$header_text="إضافة جديد ";
		$page_id="";

		if ($page_data!="")
		{
			$header_text="تعديل '".$page_data->page_title."'";
			$page_id=$page_data->page_id;
		}




	?>




	<!--new_editor-->
	<script src="{{url("/public_html/ckeditor/ckeditor.js")}}" type="text/javascript"></script>
	<script src="{{url("/public_html/ckeditor/adapters/jquery.js")}}" type="text/javascript"></script>

    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!-- END Select 2 -->

	<!--END new_editor-->
	<div class="panel panel-primary">
		<div class="panel-heading">
            <?=$header_text?>
		</div>
		<div class="panel-body">
			<div class="">
				<form id="save_form" action="<?=url("admin/pages/save_page/$page_type/$page_id")?>" method="POST" enctype="multipart/form-data">


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            بيانات الخدمه
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <?php foreach ($all_langs as $lang_key => $lang): ?>
                                    <?php
                                        $lang_id=$lang->lang_id;
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$lang_id}}">
                                        <h4 class="panel-title">
                                            <a>
                                                <img src="<?=url('/').'/'.$lang->lang_img_path?>" width="25"> {{$lang->lang_title}}
                                            </a>
                                        </h4>
                                    </div>


                                        <?php

                                            $lang_id=$lang->lang_id;

                                            $collapse_in = "";
                                            if ($lang_key == 0)
                                            {
                                                $collapse_in = "in";
                                            }

                                            $lang_img_url = url($lang->lang_img_path);
                                        ?>

                                        <div id="collapse{{$lang_id}}" class="panel-collapse  collapse <?php echo ($lang_key==0)?"in":""; ?>">
                                            <div class="panel-body">
                                                <div>

                                                    <?php

                                                    $translate_data=array();


                                                    $current_row = $all_page_translate_rows->filter(function ($value, $key) use($lang_id) {
                                                        if ($value->lang_id == $lang_id)
                                                        {
                                                            return $value;
                                                        }

                                                    });

                                                    if(is_object($current_row->first())){
                                                        $translate_data=$current_row->first();
                                                    }


                                                    $required=($lang_key==0)?"required":"";

                                                    $normal_tags=[
                                                        "page_title",
                                                        "page_body","page_meta_title","page_meta_desc",
                                                        "page_meta_keywords"
                                                    ];



                                                    $attrs = generate_default_array_inputs_html(
                                                        $normal_tags,
                                                        $translate_data,
                                                        "yes",
                                                        $required
                                                    );


                                                    foreach ($attrs[1] as $key => $value) {
                                                        $attrs[1][$key].="[]";
                                                    }


                                                    $attrs[0]["page_title"]="اسم الخدمة";
                                                    $attrs[0]["page_body"]="محتوي الخدمة";
                                                    $attrs[0]["page_meta_title"]="عنوان ال meta";
                                                    $attrs[0]["page_meta_desc"]="تفاصيل ال meta";
                                                    $attrs[0]["page_meta_keywords"]="الكلمات الدلالية";

                                                    $attrs[3]["page_title"]="textarea";
                                                    $attrs[3]["page_short_desc"]="textarea";
                                                    $attrs[3]["page_body"]="textarea";
                                                    $attrs[3]["page_meta_desc"]="textarea";
                                                    $attrs[3]["page_meta_keywords"]="textarea";

                                                    $attrs[5]["page_body"].=" ckeditor";


                                                    echo
                                                    generate_inputs_html(
                                                        reformate_arr_without_keys($attrs[0]),
                                                        reformate_arr_without_keys($attrs[1]),
                                                        reformate_arr_without_keys($attrs[2]),
                                                        reformate_arr_without_keys($attrs[3]),
                                                        reformate_arr_without_keys($attrs[4]),
                                                        reformate_arr_without_keys($attrs[5])
                                                    );
                                                    ?>

                                                </div>
                                            </div>
                                        </div>


                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Images
                        </div>
                        <div class="panel-body">
                            <?php
                                if(is_array($small_img_width_height)){
                                    $img_obj = isset($page_data->page_small_img) ? $page_data->page_small_img : "";

                                    echo generate_img_tags_for_form(
                                        $filed_name="small_img_file",
                                        $filed_label="small_img_file",
                                        $required_field="",
                                        $checkbox_field_name="small_img_checkbox",
                                        $need_alt_title="yes",
                                        $required_alt_title="",
                                        $old_path_value="",
                                        $old_title_value="",
                                        $old_alt_value="",
                                        $recomended_size=implode(",",array_keys($small_img_width_height))." ".implode(",",$small_img_width_height),
                                        $disalbed="",
                                        $displayed_img_width="100",
                                        $display_label="Upload Small Image",
                                        $img_obj
                                    );
                                }
                            ?>


                            <hr>
                            <?php
                                if(is_array($big_img_width_height)){
                                    $img_obj = isset($page_data->page_big_img) ? $page_data->page_big_img : "";

                                    echo generate_img_tags_for_form(
                                        $filed_name="big_img_file",
                                        $filed_label="big_img_file",
                                        $required_field="",
                                        $checkbox_field_name="big_img_checkbox",
                                        $need_alt_title="yes",
                                        $required_alt_title="",
                                        $old_path_value="",
                                        $old_title_value="",
                                        $old_alt_value="",
                                        $recomended_size=implode(",",array_keys($big_img_width_height))." ".implode(",",$big_img_width_height),
                                        $disalbed="",
                                        $displayed_img_width="100",
                                        $display_label="Upload Big Image",
                                        $img_obj
                                    );
                                }
                            ?>


                            <?php if(false): ?>
                                <hr>

                                <?php
                                    echo
                                    generate_slider_imgs_tags(
                                        $slider_photos=(isset($page_data->slider_imgs)&&isset_and_array($page_data->slider_imgs))?$page_data->slider_imgs:"",
                                        $field_name="page_slider_file",
                                        $field_label="Slider Images",
                                        $field_id="page_slider_file_id",
                                        $accept="image/*",
                                        $need_alt_title="yes"
                                    );
                                ?>
					        <?php endif; ?>


                            
                        </div>
                    </div>

					{{csrf_field()}}
					<input id="submit" type="submit" value="Save" class="col-md-3 col-md-offset-2 btn btn-primary btn-lg">

				</form>
			</div>
		</div>
	</div>



@endsection


	
