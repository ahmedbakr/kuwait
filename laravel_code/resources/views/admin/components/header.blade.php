<div class="header-section">

    <!--toggle button start-->
    <a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
    <!--toggle button end-->

    <!--notification menu start -->
    <div class="menu-right">
        <div class="user-panel-top">
            <div class="profile_details_left">
                <ul class="nofitications-dropdown">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                                    class="fa fa-bell"></i><span class="badge blue">{{count($notifications)}}</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>إشعارات اليوم</h3>
                                </div>
                            </li>
                            <?php if(is_array($notifications) && count($notifications)): ?>
                                <?php foreach($notifications as $key => $not): ?>
                                    <?php
                                        if($key == 5)
                                        {
                                            break;
                                        }
                                    ?>
                                    <li>
                                        <a href="#">
                                            <div class="notification_desc alert alert-{{$not->not_type}}">
                                                <p>{{$not->not_title}}</p>
                                                <p><span>{{\Carbon\Carbon::createFromTimestamp(strtotime($not->created_at))->diffForHumans()}}</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <li>
                                <div class="notification_bottom">
                                    <a href="{{url("admin/notifications/show_all")}}">مشاهدة جميع الإشعارات</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                <span style="background:url({{get_profile_logo_or_default($current_user->path)}}) no-repeat center"> </span>
                                <div class="user-name">
                                    <p>{{$current_user->full_name}}<span>{{$current_user->role}}</span></p>
                                </div>
                                <i class="lnr lnr-chevron-down"></i>
                                <i class="lnr lnr-chevron-up"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li><a href="{{url('admin/users/save/'.$current_user->user_id)}}"><i class="fa fa-cog"></i> تعديل البيانات</a></li>
                            <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> تسجيل خروج</a></li>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--notification menu end -->
</div>