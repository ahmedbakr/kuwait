<div class="">
    <!--logo and iconic logo end-->
    <div class="left-side-inner">

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
            <li class="active"><a href="{{url('/admin/dashboard')}}"><i class="lnr lnr-power-switch"></i><span>الرئيسية</span></a>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-bookmark"></i>
                    <span>الخدمات</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/pages/show_all')}}">مشاهدة الكل</a></li>
                    <li><a href="{{url('/admin/pages/save_page')}}">إضافة جديد</a></li>
                </ul>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-cog"></i>
                    <span>محتوي الموقع</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/show_methods')}}">تعديل</a></li>
                </ul>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-list"></i>
                    <span>رسائل الدعم</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/support_messages/support')}}">مشاهدة الكل</a></li>
                </ul>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-upload"></i>
                    <span>رافع الملفات</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/uploader')}}">ارفع ملفات</a></li>
                </ul>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-users"></i>
                    <span>الأدمنز</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/users/get_all_admins')}}">مشاهدة الكل</a></li>
                    <li><a href="{{url('/admin/users/save')}}">أضف جديد</a></li>
                </ul>
            </li>

            <li class="menu-list">
                <a>
                    <i class="lnr lnr-inbox"></i>
                    <span>الاشعارات</span>
                </a>
                <ul class="sub-menu-list">
                    <li><a href="{{url('/admin/notifications/show_all')}}">كل الاشعارات</a></li>
                </ul>
            </li>

        </ul>
        <!--sidebar nav end-->
    </div>
</div>