<div class="sub-section">
    <div class="img-sub-section">
        <img src="{{get_image_or_default($child_cat_obj->small_img_path)}}"
             title="{{$child_cat_obj->small_img_title}}" alt="{{$child_cat_obj->small_img_alt}}"/>
        <a href="{{url($lang_url_segment."/".urlencode($parent_cat->cat_slug)."/".urlencode($child_cat_obj->cat_slug))}}">{{show_content($general_static_keywords,"more")}}</a>
    </div>
    <div class="des-sun-section">
        <h3>{{$child_cat_obj->cat_name}}</h3>
        <p>{{$child_cat_obj->cat_short_desc}}</p>

    </div>
</div>