<div class="item-travel">
    <div class="col s12 m12 l4">
        <div class="img-item-travel">
            <img class="responsive-img" src="{{get_image_or_default($trip_obj->small_img_path)}}" title="{{$trip_obj->small_img_title}}" alt="{{$trip_obj->small_img_alt}}">

        </div>
    </div>
    <div class="col s12 m12 l8 padd-right padd-left">

        <div class="col s12 m12 l12">
            <div class="des-item-travel">
                <h3>
                    <a href="{{url($lang_url_segment."/".urlencode($trip_obj->parent_cat_slug)."/".urlencode($trip_obj->child_cat_slug)."/".urlencode($trip_obj->page_slug))}}">
                        {{$trip_obj->page_title}}
                    </a>
                </h3>
                <p>{{$trip_obj->page_short_desc}}</p>

            </div>
        </div>

        <div class="col s12 m12 l12">
            <div class="more-item-travel">
                <label>
                    <span>{{show_content($general_static_keywords,"trip_price")}} : </span>
                    <span class="currency_value" data-original_price="{{$trip_obj->page_price}}">{{$trip_obj->page_price}} </span>
                    <span class="currency_sign">$</span>
                </label>
                <span>{{$trip_obj->page_period}}</span>
                <a href="{{url($lang_url_segment."/".urlencode($trip_obj->parent_cat_slug)."/".urlencode($trip_obj->child_cat_slug)."/".urlencode($trip_obj->page_slug))}}">
                    {{show_content($general_static_keywords,"more")}}
                </a>

            </div>
        </div>

    </div>
</div>
