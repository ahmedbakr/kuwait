<div class="col-md-4 md-margin-bottom-30">
    <div class="overflow-h">
        <a class="illustration-v3 illustration-img1" style="background-image: url({{url($img_obj->path)}});"
           href="{{$edit_index_page->slider3->other_fields->link[$key]}}">
            <span class="illustration-bg">
                <span class="illustration-ads">
                    <span class="illustration-v3-category">
                        <span class="product-category">{{$edit_index_page->slider3->other_fields->header1[$key]}}</span>
                        <span class="product-amount">{{$edit_index_page->slider3->other_fields->header2[$key]}}</span>
                    </span>
                </span>
            </span>
        </a>
    </div>
</div>