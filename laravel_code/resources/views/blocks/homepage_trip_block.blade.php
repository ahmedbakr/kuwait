<?php
    $trip_path = url($lang_url_segment."/".urlencode($trip_obj->parent_cat_slug)."/".urlencode($trip_obj->page_slug));
?>

<div class="product-img">
    <a href="{{$trip_path}}">
        <img class="full-width img-responsive" style="height: 350px;" src="{{get_image_or_default($trip_obj->small_img_path)}}"
                     title="{{$trip_obj->small_img_title}}" alt="{{$trip_obj->small_img_alt}}">
    </a>
    <a class="product-review" href="{{$trip_path}}">{{show_content($general_static_keywords,"more")}}</a>
    <a class="add-to-cart" href="{{$trip_path}}"><i class="fa fa-shopping-cart"></i>{{show_content($edit_index_page,"book")}}</a>
</div>
<div class="product-description product-description-brd" style="height: 200px;">
    <div class="overflow-h margin-bottom-5">
        <div class="pull-left">
            <h4 class="title-price"><a href="{{$trip_path}}">{{$trip_obj->page_title}}</a></h4>
            <span class="gender text-uppercase">{{$trip_obj->page_period}}</span>
            <span class="gender">{{$trip_obj->parent_cat_name}} | {{$trip_obj->child_cat_name}}</span>
        </div>
        <div class="product-price">
            <span class="title-price">
                <label class="currency_value" data-original_price="{{$trip_obj->page_price}}">{{$trip_obj->page_price}}</label>
                <label class="currency_sign">$</label>
            </span>
        </div>
    </div>

    <?php if(false): ?>
    <ul class="list-inline product-ratings">
        <li><i class="rating-selected fa fa-star"></i></li>
        <li><i class="rating-selected fa fa-star"></i></li>
        <li><i class="rating-selected fa fa-star"></i></li>
        <li><i class="rating fa fa-star"></i></li>
        <li><i class="rating fa fa-star"></i></li>
    </ul>
    <?php endif; ?>

</div>