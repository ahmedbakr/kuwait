<div class="block-travel-menu">
    <div class="img-block-menu">
        <img class="responsive-img" src="{{get_image_or_default($trip_obj->small_img_path)}}"
                title="{{$trip_obj->small_img_title}}" alt="{{$trip_obj->small_img_alt}}">
        <a href="{{url($lang_url_segment."/".urlencode($trip_obj->parent_cat_slug)."/".urlencode($trip_obj->child_cat_slug)."/".urlencode($trip_obj->page_slug))}}">{{show_content($general_static_keywords,"more")}}</a>
    </div>
    <div class="dec-block-menu">
        <p>{{$trip_obj->page_short_desc}}</p>
    </div>

    <div class="location-tra">
        <strong>{{show_content($general_static_keywords,"trip_price")}} : <label class="currency_value" data-original_price="{{$trip_obj->page_price}}">{{$trip_obj->page_price}}</label> <label class="currency_sign">$</label></strong>
        <?php if(!empty($trip_obj->page_city)): ?>
            <label>{{$trip_obj->page_city}}</label>
        <?php endif; ?>
        <?php if(!empty($trip_obj->page_period)): ?>
            <span>{{$trip_obj->page_period}}</span>
        <?php endif; ?>


    </div>

</div>