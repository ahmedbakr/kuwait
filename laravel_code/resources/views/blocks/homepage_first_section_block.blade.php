<div class="overflow-h">
    <div class="illustration-v1 illustration-img1" style="background-image: url({{url($img_obj->path)}});">
        <div class="illustration-bg">
            <div class="illustration-ads ad-details-v1">
                <h3>{{$edit_index_page->slider4->other_fields->section_header[$key]}}</h3>
                <a class="btn-u btn-brd btn-brd-hover btn-u-light" href="{{$edit_index_page->slider4->other_fields->section_url[$key]}}">{{show_content($general_static_keywords,"more")}}</a>
            </div>
        </div>
    </div>
</div>