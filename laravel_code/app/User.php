<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;

    protected $fillable = [
        'email', 'password',
        'user_type','related_id','username','full_name','role','logo_id','user_active',
        'user_can_login','contacts','allowed_lang_ids','allowed_champ_ids'
    ];

    protected $primaryKey = 'user_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $dates = ["deleted_at"];

    static function get_users($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
             select u.*
             , attach.id, attach.path, attach.alt , attach.title
             
             #joins
             from users as u
             LEFT OUTER JOIN attachments as attach on (u.logo_id = attach.id)

             #where
             where u.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;

    }


    public function getContactsAttribute($value)
    {
        if (json_decode($value) != false) {
            return json_decode($value);
        }
        return [];
    }


}
