<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class settings_m extends Model
{

    protected $table = "settings";
    protected $primaryKey = "set_id";
    public $timestamps = false;
    protected $fillable = [
        'general_currency','rate'
    ];

}
