<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

date_default_timezone_set("Africa/Cairo");




Route::group(['middleware' => 'check_admin'], function () {


    // Start Admin Category Routing
    Route::get('/admin/category/save_cat/{cat_type?}','admin\category@save_cat');
    Route::get('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');
    Route::post('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');
    Route::post('/admin/category/check_validation_for_save_cat/{cat_id?}','admin\category@check_validation_for_save_cat');
    Route::post('/admin/category/delete_cat','admin\category@delete_cat');

    Route::get('/admin/category/{cat_type?}/{parent_id?}','admin\category@index')->where('parent_id', '[0-9]+');


    // End Admin Category Routing


    //pages

    Route::get('/admin/pages/save_page/{page_type?}/{page_id?}','admin\pages@save_page');
    Route::post('/admin/pages/save_page/{page_type?}/{page_id?}','admin\pages@save_page');
    Route::post('/admin/pages/check_validation_for_save_page/{page_id?}','admin\pages@check_validation_for_save_page');
    Route::post('/admin/pages/remove_page','admin\pages@remove_page');

    Route::get('/admin/pages/show_all/{page_type?}/{cat_id?}','admin\pages@index');

    Route::post('/admin/pages/search_for_page_name','admin\pages@search_for_page_name');

    //END pages


    //edit_content
    Route::get('/admin/show_methods','admin\edit_content@show_methods');
    Route::get('admin/edit_content/{lang_id}/{slug}','admin\edit_content@check_function');
    Route::post('admin/edit_content/{lang_id}/{slug}','admin\edit_content@check_function');
    //END edit_content


    Route::get('admin/dashboard', 'admin\dashboard@index');

    // Start notifications

    Route::get('/admin/notifications/show_all','admin\notifications@index');
    Route::post('/admin/notifications/delete_notification','admin\notifications@delete_notification');

    // End notifications


    // Start Admin Langs Routing

    Route::get('/admin/langs','admin\langs@index');
    Route::get('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');
    Route::post('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');
    Route::post('/admin/langs/delete_lang','admin\langs@delete_lang');

    // End Admin Langs Routing


    // Start Admin currencies Routing

    Route::get('/admin/currencies','admin\currencies@index');
    Route::get('/admin/currencies/save_currency/{id?}','admin\currencies@save_currency');
    Route::post('/admin/currencies/save_currency/{id?}','admin\currencies@save_currency');
    Route::post('/admin/currencies/delete_currency','admin\currencies@delete_currency');

    // End Admin currencies Routing



    // Start Admin users Routing

    Route::get('admin/users/get_all_admins', 'admin\users@get_all_admins');

    Route::get('admin/users/get_all_users', 'admin\users@get_all_users');
    Route::post('admin/users/change_user_can_login', 'admin\users@change_user_can_login');

    Route::get('admin/users/save/{user_id?}', 'admin\users@save_user');
    Route::post('admin/users/save/{user_id?}', 'admin\users@save_user');

    Route::get('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');
    Route::post('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');



    Route::post('/admin/users/remove_admin','admin\users@remove_admin');

    // End Admin users Routing


    // Start Admin support_messages Routing

    Route::get('/admin/support_messages/{msg_type?}','admin\support_messages@index');
    Route::post('/admin/delete_support_messages','admin\support_messages@remove_msg');

    // End Admin support_messages Routing


    // Start Admin subscribe Routing

    Route::get('/admin/subscribe','admin\subscribe@index');
    Route::post('/admin/subscribe/send_custom_email','admin\subscribe@send_custom_email');
    Route::post('/admin/subscribe/send_all_subscribers_email','admin\subscribe@send_all_subscribers_email');
    Route::get('/admin/subscribe/stop','admin\subscribe@stop');
    Route::get('/admin/subscribe/pause','admin\subscribe@pause');
    Route::get('/admin/subscribe/resume','admin\subscribe@resume');

    Route::get('/admin/subscribe/save_email','admin\subscribe@save_email');
    Route::post('/admin/subscribe/save_email','admin\subscribe@save_email');

    Route::get('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::post('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::get('/admin/subscribe/export_subscribe','admin\subscribe@export_subscribe');

    Route::post('/admin/subscribe/remove_email','admin\subscribe@remove_email');


    // End Admin subscribe Routing


    // Start menus Routing
    Route::get('/admin/menus','admin\menus@index');
    Route::get('/admin/menus/save_menu/{lang_id?}/{menu_id?}','admin\menus@get_menu');
    Route::post('/admin/menus/save_sortable_menu','admin\menus@save_sortable_menu');
    Route::post('/admin/menus/delete_menu','admin\menus@delete_menu');
    // End menus Routing

    //manage ads
    Route::get("/admin/ads",'admin\ads@index');
    Route::get("/admin/ads/save_ad/{ad_id?}",'admin\ads@save_ad');
    Route::post("/admin/ads/save_ad/{ad_id?}",'admin\ads@save_ad');
    Route::post("/admin/ads/remove_ads",'admin\ads@remove_ads');
    //END manage ads


    //uploader
    Route::get('/admin/uploader','admin\uploader@index');
    Route::post('/upload_files','admin\uploader@load_files');

    //END uploader



});


// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

//Route::auth();
//
//Route::get('/home', 'HomeController@index');
