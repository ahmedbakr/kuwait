<?php

date_default_timezone_set("Africa/Cairo");


// Web Routing
Route::group(['middleware' => ['web']], function () {

    #region all Pages

    $all_pages=\App\models\pages\pages_m::get_pages(" 
                                                        AND page.page_type = 'default'
                                                        AND page.hide_page=0  
                                                    ");
    foreach ($all_pages as $key => $page) {

        if($page->lang_id==1){
            Route::get("/".($page->page_slug),'front\pages@show_item');
        }
        else{
            Route::get("/".'{lang_title?}/'.($page->page_slug),'front\pages@show_item');
        }
    }

    #endregion

});










