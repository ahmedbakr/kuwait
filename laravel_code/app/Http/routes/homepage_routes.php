<?php

date_default_timezone_set("Africa/Cairo");

// Default Routing
Route::get('/{lang_title?}','front\HomeController@index')->where("lang_title","[a-z][a-z]");


// Web Routing
Route::group(['middleware' => ['web']], function () {

    // logout ebn el teeeeeeeeeeeeeeeeeeeeeeeet
    Route::get("/logout","logout@index");

    Route::get('/admin_panel', 'front\admin_panel@index');
    Route::post('/admin_panel', 'front\admin_panel@try_login');

    Route::get("/search_for_trip",'front\search@index');
    Route::get("/{lang_title?}/search_for_trip",'front\search@index')->where("lang_title","[a-z][a-z]");


    #region cats

    Route::get("/all_cats",'front\category@show_all_cats');
    Route::get("/{lang_title?}/all_cats",'front\category@show_all_cats')->where("lang_title","[a-z][a-z]");


    $all_cats=\App\models\category_translate_m::get_all_translate_cats(" AND cat.parent_id=0 AND cat.cat_type in ('trip','article','country') AND cat_t.cat_slug!=''");

    foreach ($all_cats as $key => $cat) {

        if($cat->lang_id==1){
            Route::get("/".($cat->cat_slug),'front\category@show_cat');
            Route::get("/".($cat->cat_slug)."/{item_slug}",'front\category@show_item');
        }
        else{
            Route::get("/".'{lang_title?}/'.($cat->cat_slug),'front\category@show_cat');
            Route::get("/".'{lang_title?}/'.($cat->cat_slug)."/{item_slug}",'front\category@show_item');
        }
    }
    #endregion


    #region all Pages

    Route::get("default/{item_id}/{item_name}",'front\pages@show_item');
    Route::get("{lang_title?}/default/{item_id}/{item_name}",'front\pages@show_item')->where("lang_title","[a-z][a-z]");

    Route::get("article/{item_id}/{item_name}",'front\pages@show_item');
    Route::get("{lang_title?}/article/{item_id}/{item_name}",'front\pages@show_item')->where("lang_title","[a-z][a-z]");


    Route::get("news/{item_id}/{item_name}",'front\pages@show_item');
    Route::get("{lang_title?}/news/{item_id}/{item_name}",'front\pages@show_item')->where("lang_title","[a-z][a-z]");

    Route::get("photo_gallery/{item_id}/{item_name}",'front\pages@show_item');
    Route::get("{lang_title?}/photo_gallery/{item_id}/{item_name}",'front\pages@show_item')->where("lang_title","[a-z][a-z]");

    Route::get("video/{item_id}/{item_name}",'front\pages@show_item');
    Route::get("{lang_title?}/video/{item_id}/{item_name}",'front\pages@show_item')->where("lang_title","[a-z][a-z]");

    Route::get("tag/{tag_id}/{tag_name}/{page_type}",'front\pages@show_tag_item');
    Route::get("{lang_title?}/tag/{tag_id}/{tag_name}/{page_type}",'front\pages@show_tag_item')->where("lang_title","[a-z][a-z]");


    Route::get("all/{page_type}",'front\pages@show_all_items');
    Route::get("{lang_title?}/all/{page_type}",'front\pages@show_all_items')->where("lang_title","[a-z][a-z]");


    #endregion


    #region Subscribe & Support

    Route::get('/contact','front\subscribe_contact@index');
    Route::get('/support','front\subscribe_contact@index');
    Route::post('/subscribe_contact/subscribe', 'front\subscribe_contact@subscribe');
    Route::post('/subscribe_contact/make_a_contact', 'front\subscribe_contact@make_a_contact');

    Route::get("/{lang_title?}".'/contact','front\subscribe_contact@index')->where("lang_title","[a-z][a-z]");
    Route::get("/{lang_title?}".'/support','front\subscribe_contact@index')->where("lang_title","[a-z][a-z]");
    Route::post("/{lang_title?}".'/subscribe_contact/subscribe', 'front\subscribe_contact@subscribe')->where("lang_title","[a-z][a-z]");
    Route::post("/{lang_title?}".'/subscribe_contact/make_a_contact', 'front\subscribe_contact@make_a_contact')->where("lang_title","[a-z][a-z]");

    #endregion



    #region Register & Login

    Route::get('/register','front\register@index');
    Route::post('/register','front\register@index');
    Route::get('/login', 'front\register@login_page');
    Route::post('/login', 'front\register@login');

    Route::get("/{lang_title?}".'/register','front\register@index')->where("lang_title","[a-z][a-z]");
    Route::post("/{lang_title?}".'/register','front\register@index')->where("lang_title","[a-z][a-z]");
    Route::get("/{lang_title?}".'/login', 'front\register@login_page')->where("lang_title","[a-z][a-z]");
    Route::post("/{lang_title?}".'/login', 'front\register@login')->where("lang_title","[a-z][a-z]");

    #endregion


    #region comment

    Route::post('/load_more_comments','front\category@load_more_comments');
    Route::post('/category/apply_comment','front\category@apply_comment');
    Route::post("/{lang_title?}".'/category/apply_comment','front\category@apply_comment')->where("lang_title","[a-z][a-z]");

    #endregion


    #region change_currency

    Route::post('/change_currency','front\change_currency@index');

    #endregion

    //subscribe_cron_jop
    Route::get('/subscribe_cron_jop','subscribe_cron_jop@index');
    Route::get('/subscribe_cron_jop/show_email','subscribe_cron_jop@show_email');
    //END subscribe_cron_jop
});










