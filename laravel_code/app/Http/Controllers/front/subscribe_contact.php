<?php

namespace App\Http\Controllers\front;

use App\models\notification_m;
use App\models\subscribe_m;
use App\models\support_messages_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class subscribe_contact extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $this->data["meta_title"]=$this->data["support"]->meta_title;
        $this->data["meta_desc"]=$this->data["support"]->meta_desc;
        $this->data["meta_keywords"]=$this->data["support"]->meta_keywords;


        return view("front.subviews.support",$this->data);
    }

    public function subscribe(Request $request)
    {
        $output["error"] = "";

        $request["email"] = clean($request["email"]);

        $validator = Validator::make(
            [
                "email" => $request["email"]
            ],
            [
                "email" => "email|required|unique:subscribe,email"
            ]
        );

        if (count($validator->messages()) == 0)
        {
            $check = subscribe_m::create($request->all());
            if(is_object($check))
            {
                $output["error"] = "";
            }
            else{
                $output["error"] = "error";
                $output["error_msg"] = show_content($this->data["validation_messages"],"error_occured");
            }
        }
        else{
            $output["error"] = "error";
            $output["error_msg"] = $validator->messages();
//            $output["error_msg"] = $output["error_msg"]->email;
        }

        return json_encode($output);

    }

    public function make_a_contact(Request $request)
    {

        $output=array();
        \Debugbar::disable();


        $validator = Validator::make(

            [
//                "email" => $request["email"],
                "message" => $request["message"],
                "name" => $request["name"],
            ],

            [
//                "email" => "required|email",
                "message" => "required",
                "name" => "required",
            ]

        );


        //Input::all();

        $this->general_get_content(array("email_page"));

        if (count($validator->messages()) == 0)
        {
            $inputs=Input::all();

            try{
                $ip=get_client_ip();
                $inputs["country"] = ip_info($ip)['country'];
            }catch(Exception $e){
                $inputs["country"]="";
            }

            $inputs["source"]=Cookie::get('source');
            if($inputs["source"]==null){
                $inputs["source"]="";
            }

            $inputs["name"]=(isset($inputs["name"]))?clean($inputs["name"]):"";
            $inputs["tel"]=(isset($inputs["tel"]))?clean($inputs["tel"]):"";
            $inputs["message"]=(isset($inputs["message"]))?clean($inputs["message"]):"";
            $inputs["title"]=(isset($inputs["title"]))?clean($inputs["title"]):"";
            $inputs["phone"]=(isset($inputs["phone"]))?clean($inputs["phone"]):"";
            $inputs["msg_type"]=(isset($inputs["msg_type"]))?clean($inputs["msg_type"]):"";
            $inputs["current_url"]=(isset($inputs["current_url"]))?clean($inputs["current_url"]):"";
            $inputs["source"]=(isset($inputs["source"]))?clean($inputs["source"]):"";

            $inputs["other_data"]=[];
            $inputs["other_data"]["fax"]=(isset($inputs["fax"]))?clean($inputs["fax"]):"";
            $inputs["other_data"]["address"]=(isset($inputs["address"]))?clean($inputs["address"]):"";

            $inputs["other_data"]=json_encode($inputs["other_data"]);

            $support_message_obj=support_messages_m::create($inputs);


//            $subscribe_option=$inputs["subscribe_option"];
            //if subscribe_option== true then add
            //emdil to subscribe if uniqe
//            if ($subscribe_option=="true") {
//
//                $old_row=subscribe_m::where("email","=",$inputs["email"])->first();
//
//                if (!is_object($old_row)) {
//                    subscribe_m::create([
//                        "email" => $inputs["email"]
//                    ]);
//                }
//            }


            $admin_emails=User::where("user_type","admin")->get();
            $admin_emails=convert_inside_obj_to_arr($admin_emails->all(),"email");

            unset($inputs["_token"],$inputs["source"]);

            //send email to admins and to user email

            if(is_object($support_message_obj)){

                // send notification
                notification_m::create([
                    "not_title" => $inputs["name"]." أرسل رساله للدعم الفني للموقع ",
                    "not_date" => Carbon::now()
                ]);

                $email_data_for_user=[
                    "obj"=>$this->data["email_page"],
                    "msg"=>"لقد تم إرسال رسالتك بنجاح"
                ];

                $email_data_for_admins=[
                    "obj"=>$this->data["email_page"],
                    "msg"=>"لقد تم إرسال رسالة الي الدعم الفني للموقع",
                    "email_data"=>$inputs
                ];


                //1)admin
                $this->_send_email_to_custom(
                    $emails = $admin_emails,
                    $data = $email_data_for_admins ,
                    $subject = "لقد تم إرسال رسالة الي الدعم الفني للموقع "
                );

                $output["msg"] = "<div class='alert alert-success'>".show_content($this->data["support"],"form_success_msg")."</div>";

            }

        }
        else{
            $output["msg"]="<div class='alert alert-danger'>";

            foreach ($validator->messages()->all() as $key => $msg) {
                $output["msg"].=$msg."<br>";
            }

            $output["msg"] .= "</div>";
        }

        echo json_encode($output);
    }

}
