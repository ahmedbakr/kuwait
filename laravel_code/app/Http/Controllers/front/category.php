<?php

namespace App\Http\Controllers\front;

use App\models\category_m;
use App\models\pages\pages_m;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class category extends Controller
{
    //
    public $lang_seg_1=false;
    public function __construct(){
        parent::__construct();

        $url_seg_1=\Request::segment(1);
        $all_langs_titles=convert_inside_obj_to_arr($this->data["all_langs"],"lang_title");

        if(in_array($url_seg_1,$all_langs_titles)) {
            $this->lang_seg_1=true;
        }

    }

    public function show_all_cats(){

        $slider_arr = array();
        $slider_arr["all_cats_page"]=["slider1"];
        $this->general_get_content(["all_cats_page"],$slider_arr);

        $this->data["meta_title"]=show_content($this->data["all_cats_page"],"meta_title");
        $this->data["meta_desc"]=show_content($this->data["all_cats_page"],"meta_desc");
        $this->data["meta_keywords"]=show_content($this->data["all_cats_page"],"meta_keywords");


        $this->data["all_parent_cats"]=category_m::get_all_cats(
            $additional_where = " AND cat.parent_id=0  AND cat.hide_cat=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        return view('front.subviews.category.all_parent_cats',$this->data);
    }


    public function show_cat($lang_title=""){

        $slug_segment=1;

        if($this->lang_seg_1){
            $slug_segment=2;
        }

        $cat_slug=\Request::segment($slug_segment);
        $cat_slug=urldecode($cat_slug);
        $cat_slug=clean($cat_slug);

        $cat_data=category_m::get_all_cats(
            $additional_where = " AND cat_translate.cat_slug='$cat_slug' AND cat.hide_cat=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($cat_data)&&count($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["cat_data"]=$cat_data;

        $this->data["meta_title"]=$cat_data->cat_meta_title;
        $this->data["meta_desc"]=$cat_data->cat_meta_desc;
        $this->data["meta_keywords"]=$cat_data->cat_meta_keywords;


        //get this cat childs
        $child_cats=category_m::get_all_cats(
            $additional_where = " AND cat.parent_id=$cat_data->cat_id  AND cat.hide_cat=0",
            $order_by = " order by cat_order " ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        $this->data["child_cats"]=$child_cats;


        $this->data["cat_trips"]=[];
        if(isset_and_array($child_cats)){
            $child_cats_ids=convert_inside_obj_to_arr($child_cats,"cat_id");

            $cat_trips=pages_m::get_pages(
                $additional_where = " AND page.cat_id in (".implode(",",$child_cats_ids).") ",
                $order_by = "" ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id
            );

            $this->data["cat_trips"]=$cat_trips;
        }



        return view('front.subviews.category.trip.parent_cat',$this->data);
    }

    public function show_item(){

        $slug_segment=2;
        if($this->lang_seg_1){
            $slug_segment=3;
        }

        $item_slug=\Request::segment($slug_segment);
        $item_slug=urldecode($item_slug);
        $item_slug=clean($item_slug);

        //page_data
        $page_data=pages_m::get_pages(
            $additional_where = " AND page_trans.page_slug='$item_slug' AND page.hide_page=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($page_data)&&count($page_data)){
            $page_data=$page_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["meta_title"]=$page_data->page_meta_title;
        $this->data["meta_desc"]=$page_data->page_meta_desc;
        $this->data["meta_keywords"]=$page_data->page_meta_keywords;

        $this->data["trip_data"]=$page_data;

        //get related trips depend on category
        $this->data["related_trips"]=pages_m::get_pages(
            $additional_where = " AND page.cat_id=$page_data->cat_id AND page.page_id!=$page_data->page_id",
            $order_by = "" ,
            $limit = "limit 10",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );


        return view('front.subviews.category.trip.trip',$this->data);
    }



}
