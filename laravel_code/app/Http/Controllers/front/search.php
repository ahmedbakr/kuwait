<?php

namespace App\Http\Controllers\front;

use App\models\category_m;
use App\models\pages\pages_m;
use App\models\pages\pages_translate_m;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class search extends Controller
{
    //
    public $lang_seg_1=false;
    public function __construct(){
        parent::__construct();

        $url_seg_1=\Request::segment(1);
        $all_langs_titles=convert_inside_obj_to_arr($this->data["all_langs"],"lang_title");

        if(in_array($url_seg_1,$all_langs_titles)) {
            $this->lang_seg_1=true;
        }

    }

    public function index(){

        $slider_arr = array();
        $this->general_get_content(["search_page"],$slider_arr);

        $this->data["meta_title"]=$this->data["search_page"]->meta_title;
        $this->data["meta_desc"]=$this->data["search_page"]->meta_desc;
        $this->data["meta_keywords"]=$this->data["search_page"]->meta_keywords;


        $search_keyword=\Request::get("search_keyword");
        $search_keyword=urldecode($search_keyword);
        $search_keyword=clean($search_keyword);


        //pagination
        $trips_pagination=pages_translate_m::
        join('pages', 'pages.page_id', '=', 'pages_translate.page_id')->
        where("page_title","like","%$search_keyword%")->
        where("lang_id",$this->lang_id)->
        where("page_type",'trip')->
        paginate(10);

        $trips_pagination->appends(Input::all());

        $this->data["trips_pagination"]=$trips_pagination;

        $trips=[];
        if(isset_and_array($trips_pagination->all())){
            $trips_pagination=$trips_pagination->all();
            $trips_pagination=convert_inside_obj_to_arr($trips_pagination,"page_id");

            $trips=pages_m::get_pages(
                $additional_where = " AND page.page_id in (".implode(",",$trips_pagination).") AND page.hide_page=0 ",
                $order_by = "" ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id
            );
        }
        $this->data["trips"]=$trips;


        return view('front.subviews.search',$this->data);
    }


    public function show_cat($lang_title=""){

        $slug_segment=1;

        if($this->lang_seg_1){
            $slug_segment=2;
        }

        $cat_slug=\Request::segment($slug_segment);
        $cat_slug=urldecode($cat_slug);
        $cat_slug=clean($cat_slug);

        $cat_data=category_m::get_all_cats(
            $additional_where = " AND cat_translate.cat_slug='$cat_slug' AND cat.hide_cat=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($cat_data)&&count($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["cat_data"]=$cat_data;

        $this->data["meta_title"]=$cat_data->cat_meta_title;
        $this->data["meta_desc"]=$cat_data->cat_meta_desc;
        $this->data["meta_keywords"]=$cat_data->cat_meta_keywords;


        //get this cat childs
        $this->data["child_cats"]=category_m::get_all_cats(
            $additional_where = " AND cat.parent_id=$cat_data->cat_id  AND cat.hide_cat=0",
            $order_by = " order by cat_order " ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );


        //send user to sub cat if there is just one child for this cat
        if(is_array($this->data["child_cats"])&&count($this->data["child_cats"])==1){
            $redirect_url=url("/")."/".$this->data["lang_url_segment"].$cat_slug."/".$this->data["child_cats"][0]->cat_slug;
            return Redirect::to($redirect_url)->send();
        }

        if($cat_data->cat_type=='trip'){
            return view('front.subviews.category.trip.parent_cat',$this->data);
        }
        elseif ($cat_data->cat_type=='country'){
            return view('front.subviews.category.article.parent_cat',$this->data);
        }
    }

    public function show_sub_cat(){


        $slug_segment=2;
        if($this->lang_seg_1){
            $slug_segment=3;
        }

        $cat_slug=\Request::segment($slug_segment);
        $cat_slug=urldecode($cat_slug);
        $cat_slug=clean($cat_slug);

        $cat_data=category_m::get_all_cats(
            $additional_where = " AND cat_translate.cat_slug='$cat_slug'  AND cat.hide_cat=0 ",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($cat_data)&&count($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["cat_data"]=$cat_data;


        $this->data["meta_title"]=$cat_data->cat_meta_title;
        $this->data["meta_desc"]=$cat_data->cat_meta_desc;
        $this->data["meta_keywords"]=$cat_data->cat_meta_keywords;


        if($cat_data->cat_type=='trip'){
            return $this->trip_sub_category($cat_data);
        }
        elseif($cat_data->cat_type=='city'){
            return $this->article_sub_category($cat_data);
        }

    }

    public function trip_sub_category($cat_data){
        $this->data["cat_trips_count"]=pages_m::select(DB::raw("count(*) as trips_count"))->
        where("page_type","trip")->
        where("hide_page","0")->
        where("cat_id",$cat_data->cat_id)->get()->first()->trips_count;


        //sort_by
        $sort_by="page_id,desc";

        $this->data["selected_sort_option"]="";
        $sort_by_get=Input::get("sort_by");
        if(in_array($sort_by_get,["page_price,asc","page_price,desc","created_at,asc","created_at,desc"])){
            $sort_by=$sort_by_get;
            $this->data["selected_sort_option"]=$sort_by_get;
        }

        $sort_by=explode(",",$sort_by);


        //pagination
        $cat_trips_pagination=pages_m::
        where("cat_id",$cat_data->cat_id)->
        where("hide_page","0")->
        orderBy($sort_by[0],$sort_by[1])->
        paginate(10);

        $cat_trips_pagination->appends(Input::all());

        $this->data["cat_trips_pagination"]=$cat_trips_pagination;

        $cat_trips=[];
        if(isset_and_array($cat_trips_pagination->all())){
            $cat_trips_pagination=$cat_trips_pagination->all();
            $cat_trips_pagination=convert_inside_obj_to_arr($cat_trips_pagination,"page_id");

            $cat_trips=pages_m::get_pages(
                $additional_where = " AND page.page_id in (".implode(",",$cat_trips_pagination).") ",
                $order_by = "" ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id
            );
        }
        $this->data["cat_trips"]=$cat_trips;


        return view('front.subviews.category.trip.sub_cat',$this->data);
    }

    public function article_sub_category($cat_data){

        //pagination
        $cat_items_pagination=pages_m::
        where("cat_id",$cat_data->cat_id)->
        where("hide_page","0")->
        paginate(10);
        $cat_items_pagination->appends(Input::all());

        $this->data["cat_items_pagination"]=$cat_items_pagination;

        $cat_items=[];
        if(isset_and_array($cat_items_pagination->all())){
            $cat_items_pagination=$cat_items_pagination->all();
            $cat_items_pagination=convert_inside_obj_to_arr($cat_items_pagination,"page_id");

            $cat_items=pages_m::get_pages(
                $additional_where = " AND page.page_id in (".implode(",",$cat_items_pagination).") ",
                $order_by = "" ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id
            );
        }
        $this->data["cat_items"]=$cat_items;


        return view('front.subviews.category.article.sub_cat',$this->data);
    }

    public function show_item(){

        $slug_segment=3;
        if($this->lang_seg_1){
            $slug_segment=4;
        }

        $item_slug=\Request::segment($slug_segment);
        $child_cat_slug=\Request::segment($slug_segment-1);

        $item_slug=urldecode($item_slug);
        $child_cat_slug=urldecode($child_cat_slug);

        $item_slug=clean($item_slug);
        $child_cat_slug=clean($child_cat_slug);

        $cat_data=category_m::get_all_cats(
            $additional_where = " AND cat_translate.cat_slug='$child_cat_slug' AND cat.hide_cat=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );
        if(is_array($cat_data)&&count($cat_data)){
            $cat_data=$cat_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["cat_data"]=$cat_data;

        //page_data
        $page_data=pages_m::get_pages(
            $additional_where = " AND page_trans.page_slug='$item_slug' AND page.cat_id=$cat_data->cat_id AND page.hide_page=0",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($page_data)&&count($page_data)){
            $page_data=$page_data[0];
        }
        else{
            return abort(404);
        }

        $this->data["meta_title"]=$page_data->page_meta_title;
        $this->data["meta_desc"]=$page_data->page_meta_desc;
        $this->data["meta_keywords"]=$page_data->page_meta_keywords;

        if($page_data->page_type=="trip"){
            return $this->show_trip_item($page_data);
        }
        elseif($page_data->page_type=="article"){
            return $this->show_article_item($page_data);
        }

    }


    public function show_trip_item($page_data){

        $page_data->page_header_arr=json_decode($page_data->page_header_arr);
        $page_data->page_body_arr=json_decode($page_data->page_body_arr);

        $this->data["trip_data"]=$page_data;

        //get related trips depend on category
        $this->data["related_trips"]=pages_m::get_pages(
            $additional_where = " AND page.cat_id=$page_data->cat_id AND page.page_id!=$page_data->page_id",
            $order_by = "" ,
            $limit = "limit 10",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );


        return view('front.subviews.category.trip.trip',$this->data);

    }

    public function show_article_item($page_data){
        $this->data["page_data"]=$page_data;
        $this->data["related_trips"]=$page_data->related_pages;


        return view('front.subviews.category.article.article',$this->data);
    }





}
