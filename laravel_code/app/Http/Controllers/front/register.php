<?php

namespace App\Http\Controllers\front;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;



use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;




class register extends Controller
{
    public function __construct()
    {
        parent::__construct();



    }

    public function index(Request $request)
    {

        $slider_arr = array();

        $this->general_get_content([
            "register_page"
        ],$slider_arr);

        $this->data["meta_title"]=$this->data["register_page"]->meta_title;
        $this->data["meta_desc"]=$this->data["register_page"]->meta_desc;
        $this->data["meta_keywords"]=$this->data["register_page"]->meta_keyowrds;

        if(is_object($this->data["current_user"]))
        {
            return redirect()->intended('/');
        }

        if ($request->method() == "POST")
        {
            \Debugbar::disable();

            $user_id = null;
            $this->validate($request,
                [

                    "username" => "required|min:3|unique:users,username,".$user_id.",user_id,deleted_at,NULL",
                    "email" => "required|email|unique:users,email,".$user_id.",user_id,deleted_at,NULL",
                    "password" => "required|min:3|confirmed",
                    'password_confirmation' => 'required|min:3',

                ]
            );

            // clean inputs
            $request["username"] = clean($request["username"]);
            $request["email"] = clean($request["email"]);
            $request["password"] = clean($request["password"]);
            $request["password_confirmation"] = clean($request["password_confirmation"]);
            $request["mobile"] = clean($request["mobile"]);
            $request["tel"] = clean($request["tel"]);
            $request["country"] = clean($request["country"]);
            $request["city"] = clean($request["city"]);
            $request["user_info"] = clean($request["user_info"]);

            $request["user_type"] = "user";
            $email = $request["email"];
            $password = $request["password"];
            $request["password"] = bcrypt($request["password"]);

            $check = User::create($request->all());
            if ($check)
            {
                $login=\Auth::attempt([
                    "email"=>$email,
                    "password"=>$password,
                    "user_type"=>"user",
                ],$request->get("remember"));

                if($login){
                    \Auth::login(\Auth::user());
                    $request->session()->save();
                    return redirect()->intended('/');
                }else{
                    $msg="<div class='alert alert-danger'>".show_content($this->data["validation_messages"],"invalid_username_password")."</div>";
                    return redirect()->back()->with(["msg"=>$msg]);
                }
            }

        }

        return view('front.subviews.register',$this->data);
    }

    public function login(Request $request)
    {

        if(is_object($this->data["current_user"]))
        {
            return redirect()->intended('/');
        }

        $output = array();
        $output["success"] = "";
        $output["msg"] = "";

        if ($request->method() == "POST")
        {
            \Debugbar::disable();
//            $check = User::create($request->all());
//            if ($check)
//            {
                $email_login=\Auth::attempt([
                    "email"=>clean($request->get('email')),
                    "password"=>clean($request->get('password')),
                    "user_type"=>"user",
                ],clean($request->get("remember")));

                $username_login=\Auth::attempt([
                    "username"=>clean($request->get('email')),
                    "password"=>clean($request->get('password')),
                    "user_type"=>"user",
                ],clean($request->get("remember")));

                if($email_login || $username_login){
                    \Auth::login(\Auth::user());
                    $request->session()->save();
                    $output["success"] = "success";

                }else{
                    $output["success"] = "error";
                    $output["msg"] = show_content($this->data["validation_messages"],"invalid_username_password");
                }
//            }

        }

        return json_encode($output);

    }

    public function login_page()
    {
        $slider_arr = array();

        $this->general_get_content([
            "login_page"
        ],$slider_arr);

        $this->data["meta_title"]=$this->data["login_page"]->meta_title;
        $this->data["meta_desc"]=$this->data["login_page"]->meta_desc;
        $this->data["meta_keywords"]=$this->data["login_page"]->meta_keyowrds;

        if(is_object($this->data["current_user"]))
        {
            return redirect()->intended('/');
        }


        return view('front.subviews.login',$this->data);
    }

}
