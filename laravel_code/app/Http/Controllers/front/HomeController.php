<?php

namespace App\Http\Controllers\front;


use App\Http\Controllers\admin\matches;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\ads_m;
use App\models\category_m;
use App\models\pages\pages_m;
use App\models\settings_m;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang_title="")
    {

        $this->data["homepage_pages"] = pages_m::get_pages(
            " 
                                AND page.show_in_homepage = 1
                                AND page.page_type = 'default'
                                AND page.hide_page=0 ",
            $order_by = "" , $limit = "",$check_self_translates = false,$default_lang_id=$this->lang_id);

        return view("front.subviews.index",$this->data);
    }

}
