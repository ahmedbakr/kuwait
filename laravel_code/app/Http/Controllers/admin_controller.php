<?php

namespace App\Http\Controllers;

use App\models\attachments_m;
use App\models\notification_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SSP;

class admin_controller extends dashbaord_controller
{

    public $current_user_data;
    public $user_permissions;


    public function __construct()
    {
        parent::__construct();
        $this->middleware("check_admin");
        $this->current_user_data=$this->data["current_user"];



        $date = date('Y-m-d');

        $this->user_permissions = $this->get_user_permissions();
        $this->data["user_permissions"] = $this->user_permissions;
        $this->data["notifications"] = notification_m::get_notifications(" where date(created_at) = '$date' AND not_to_userid = $this->user_id order by created_at desc");


        $this->data["display_lang"]=\Session::get("display_lang","en");
    }

}
