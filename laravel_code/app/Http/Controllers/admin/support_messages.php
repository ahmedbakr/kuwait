<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\support_messages_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class support_messages extends admin_controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index($msg_type="support")
    {
        //check_availability,support,build_trip

        $this->data["msg_type"]=$msg_type;

        $this->data["all_messages"] = support_messages_m::where("msg_type","$msg_type")->orderBy("id","desc")->get()->all();
        return view("admin.subviews.support_messages.show")->with($this->data);
    }

    public function remove_msg(Request $request){

        $this->general_remove_item($request,'App\models\support_messages_m');
    }


}
