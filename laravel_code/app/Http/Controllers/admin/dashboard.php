<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;

use App\Http\Requests;
use App\models\product\product_materials_m;
use App\models\product\products_m;
use App\User;

class dashboard extends admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        return view("admin.subviews.dashboard",$this->data);
    }


}
