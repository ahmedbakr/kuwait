jQuery(function ($) {

    // Start Subscribe

    $(".subscribe_submit_email").keyup(function(event){
        
        if (event.which==13) {
            $(".subscribe_submit_btn").click();
        }

    });

    $(".subscribe_submit_btn").click(function(){

        var email_element=$(".subscribe_submit_email");

        var this_element = $(this);
        var email = email_element.val();

        email_element.attr("disabled","disabled");
        this_element.attr("disabled","disabled");

        if (typeof (email) == "undefined" || email == "") {
            $(".subscribe_submit_email").css({"border": "2px solid red"});
            this_element.removeAttr("disabled");
            email_element.removeAttr("disabled");
        } else {

            $.ajax({
                url: base_url2 + lang_url_class + "subscribe_contact/subscribe",
                data: {"_token": _token, "email": email},
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    if (typeof (return_data.error) != "undefined" && return_data.error != "error") {
                        $(".subscribe_submit_email").css({"border": "2px solid green"});
                        $(".subscribe_msg").html("");
                    } else {
                        $(".subscribe_submit_email").css({"border": "2px solid red"});
                        $(".subscribe_msg").html(return_data.error_msg.email);
                        //$(".ajax_loader_class").remove();
                        this_element.removeAttr("disabled");
                        email_element.removeAttr("disabled");
                    }
                }

            });
        }

        return false;
    });

    // End Subscribe

    // Start Support 
    $("body").on("click", ".contact_us_btn", function () {

        var parent_div=$(this).parents(".contact_us_parent_div");

        var object = {};
        object.name = $("#name",parent_div).val();
        object.tel = $("#phone",parent_div).val();
        object.email = $("#email",parent_div).val();
        object.title = $("#title",parent_div).val();
        object.msg_type = $("#msg_type",parent_div).val();
        object.message = $("#msg",parent_div).val();

        object.address = $("#address",parent_div).val();
        object.fax = $("#fax",parent_div).val();

        object.arrival_date = $("#arrival_date",parent_div).val();
        object.departure_date = $("#departure_date",parent_div).val();
        object.adults_number = $("#adults_number",parent_div).val();
        object.children_number = $("#children_number",parent_div).val();


        object.trip_id = $("#check_availability_trip_id",parent_div).val();

        object.current_url = location.href;
        // object.subscribe_option = $("#subscribe_option_id").is(":checked");
        object._token = _token;

        var this_element = $(this);
        this_element.append(ajax_loader_img_func("15px"));

        console.log(object);
        this_element.attr("disabled","disabled");

        $.ajax({
            url: base_url2 + lang_url_class + "subscribe_contact/make_a_contact",
            data: object,
            type: 'POST',
            success: function (data) {
                console.log(data);
                var json_data=JSON.parse(data);
                if(typeof (json_data)!="undefined"){
                    $(".display_msgs",parent_div).html(json_data.msg);
                    this_element.removeAttr("disabled");
                    this_element.children(".ajax_loader_class").remove();
                }

            }

        });
        return false;
    });
    // End Support


    $("#search").keyup(function (e) {
        if(e.which==13){
            $("#search_btn").click();
        }
    });

    $("#search_btn").click(function () {
        location.href=base_url2+lang_url_class+"search/"+$("#search").val();
    });

    var selected_currency_data = $('.selected_currency_data');
    if(typeof(selected_currency_data) != "undefined")
    {
        var code = selected_currency_data.attr("data-code");
        var sign = selected_currency_data.attr("data-sign");
        var rate = selected_currency_data.attr("data-rate");

        if(typeof(code) != "undefined" && typeof(sign) != "undefined" && typeof(rate) != "undefined" )
        {

            $.each($('.currency_value'),function (ind,val) {

                var origin_price = $(this).attr("data-original_price");
                if(typeof(origin_price) != "undefined")
                {

                    $(this).html((parseFloat(origin_price) * parseFloat(rate)).toFixed(2));
                    $(this).parents().find('.currency_sign').html(sign);

                }

            });
        }


    }

    $('.menu_select_currency').click(function () {

        var this_element = $(this);
        var code = this_element.attr("data-code");
        var sign = this_element.attr("data-sign");
        var rate = this_element.attr("data-rate");

        console.log(code);
        console.log(sign);
        console.log(rate);

        if(typeof(code) != "undefined" && typeof(sign) != "undefined" && typeof(rate) != "undefined" )
        {

            $.ajax({
                url: base_url2 + lang_url_class + "change_currency",
                data: {"_token": _token, "code": code, "sign": sign, "rate":rate},
                type: 'POST',
                success: function (data) {
                    var return_data = JSON.parse(data);
                    if (typeof (return_data.error) != "undefined" && return_data.error != "error") {
                        location.href = window.location.href;
                    } else {
                        return false;
                    }
                }

            });

        }


        return false;
    });

});

