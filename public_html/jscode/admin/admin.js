$(function () {

    $("body").on("click",".general_remove_element",function () {

        var item = $(this).attr("data-removed_class");

        if (typeof (item) != "undefined" && item != "")
        {
            var confirm_res = confirm("Are You Sure?");
            if (confirm_res == true) {

                $(this).parents().find("."+item).remove();

            }//end confirmation if
        }

        return false;
    });

    $(".datepicker_input input").keydown(function(){
        return false;
    });


    $(".search_for_trip_title").keydown(function(e){

        if(e.which=="13"){
            var this_element = $(this);


            var object = {};
            object._token = _token;
            object.page_title = this_element.val();

            this_element.parent().append(ajax_loader_img_func("15px"));

            this_element.attr("disabled","disabled");

            $.ajax({
                url: base_url2 + "/admin/pages/search_for_page_name",
                data: object,
                type: 'POST',
                success: function (data) {
                    console.log(data);
                    var json_data=JSON.parse(data);
                    if(typeof (json_data)!="undefined"){
                        $(".select_trip").html(json_data.options);
                        this_element.removeAttr("disabled");
                        this_element.parent().children(".ajax_loader_class").remove();
                    }

                }

            });

            return false;
        }

    });

    $(".select_trip").change(function(){

        var this_element=$(this);
        var selected_option=$("option:checked",this_element);
        var trip_id=selected_option.attr("data-pageid");
        var trip_title=selected_option.attr("data-pagetitle");

        var is_exsit_before=$(".selected_trip[data-tripid='"+trip_id+"']");
        if(is_exsit_before.length==0&&typeof (trip_id)!="undefined"){

            var html="";
            html+='<label class="label label-success selected_trip" data-tripid="'+trip_id+'" style="font-size: 100%;">';
                html+=trip_title;
                html+='<a href="#" class="remove_related_trip">x</a>';
                html+='<input type="hidden" name="related_pages[]" value="'+trip_id+'">';
            html+="</label>";


            $(".selected_trips_div").append(html);
        }

    });

    $(".selected_trips_div").on("click",".remove_related_trip",function () {

        if(confirm("Are You Sure?")){
            $(this).parents(".selected_trip").remove();

        }

        return false;
    });


    $("#price_table_type_id").change(function(){

        var select_value=$(this).val();
        console.log(select_value);
        if (select_value=="type1") {
            $(".table_type").hide();
            $(".table_type1").show();
        }
        if (select_value=="type2") {
            $(".table_type").hide();
            $(".table_type2").show();
        }
        if (select_value=="type3") {
            $(".table_type").hide();
            $(".table_type3").show();
        }

    });


});