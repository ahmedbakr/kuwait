$(function () {


    var base_url2 = $(".url_class").val()+"/";
    var lang_url_class = $(".lang_url_class").val();
    var base_url = base_url2 + "/public_html/";
    var _token = $(".csrf_input_class").val();
    var ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";
    var ajax_loader_img_func=function(img_width){
        return "<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' style='width:"+img_width+";height:"+img_width+";'>";
    };




    var event_xhr;

    var get_event_ajax_func=function(){

        //get last event li from .match_events_ul
        var last_li=$(".match_events_ul").children("li").last();

        var obj={};
        obj._token=_token;
        obj.match_id=last_li.data("matchid");
        obj.event_id=last_li.data("eventid");


        $.ajax({
            url:base_url2+lang_url_class+"get_last_match_event",
            type:"POST",
            data:obj,
            success:function(data){
                var json_data=JSON.parse(data);

                console.info(json_data);
                if(typeof (json_data.events)!="undefined"){
                    $(".match_events_ul").append(json_data.events);
                }

                if(typeof (json_data.goals)!="undefined"){
                    $(".match_result").html(json_data.goals);
                }


            }
        });
    };


    //every 60 seconds
    setInterval(function(){
        get_event_ajax_func();
    },60000);


    setInterval(function(){

        var seconds=parseInt($(".match_timer .seconds").html());
        var minutes=parseInt($(".match_timer .minutes").html());

        seconds=seconds+1;
        if(seconds>=60){
            seconds=0;
            minutes=minutes+1;
        }

        $(".match_timer .seconds").html(seconds);
        $(".match_timer .minutes").html(minutes);

    },1000);





});