-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2017 at 10:58 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kuwait`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL,
  `ads_title` varchar(200) NOT NULL,
  `page_name` varchar(200) NOT NULL,
  `ad_img` int(11) DEFAULT NULL,
  `ad_link` varchar(200) NOT NULL,
  `ad_script` text NOT NULL,
  `ad_show` varchar(200) NOT NULL,
  `position` varchar(200) NOT NULL COMMENT 'body | sidebar',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `ads_title`, `page_name`, `ad_img`, `ad_link`, `ad_script`, `ad_show`, `position`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test123', 'homepage', 78, 'www', 'asdasd asdkjasdjalsjdk', 'image', 'sidebar', '2016-08-21 04:00:00', '2016-08-25 09:38:12', '2016-08-25 09:38:12'),
(3, 'newads', 'homepage', 0, '', 'new ads script here', 'script', 'body', '2016-08-21 11:38:19', '2016-08-25 09:44:25', '2016-08-25 09:44:25'),
(4, 'homepagebanner', 'homepage', 111, '', '', 'image', 'body', '2016-08-25 09:40:23', '2017-02-14 10:30:00', '2017-02-14 10:30:00'),
(5, 'ارسلرصيد', 'homepage', 253, 'ارسل رصيد', 'ارسل رصيد', 'image', 'sidebar', '2016-09-26 20:02:51', '2017-01-26 12:51:45', '2017-01-26 12:51:45'),
(6, 'cards', 'homepage', 403, 'cards', 'cards', 'image', 'sidebar', '2016-09-29 00:12:26', '2016-09-29 00:17:24', '2016-09-29 00:17:24'),
(7, 'left', 'homepage', 42, '#', '', 'image', 'left', '2017-02-14 10:27:42', '2017-02-14 10:27:42', NULL),
(8, 'header', 'homepage', 43, '', '', 'image', 'header', '2017-02-14 10:40:05', '2017-02-14 10:40:37', NULL),
(9, 'right', 'homepage', 44, '', '', 'image', 'right', '2017-02-14 10:41:04', '2017-02-14 10:41:04', NULL),
(10, 'sidebar1', 'homepage', 45, '', '', 'image', 'sidebar1', '2017-02-14 10:41:19', '2017-02-14 10:42:19', NULL),
(11, 'sidebar2', 'homepage', 46, '', '', 'image', 'sidebar2', '2017-02-14 10:41:37', '2017-02-14 10:41:37', NULL),
(12, 'sidebar3', 'homepage', 47, '', '', 'image', 'sidebar3', '2017-02-14 10:41:56', '2017-02-14 10:41:56', NULL),
(13, 'index_body', 'homepage', 48, '', '', 'image', 'index_body', '2017-02-14 10:42:45', '2017-02-14 10:42:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `alt` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1866 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `title`, `alt`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1654, '', '', 'uploads/edit_content_folder/69c2bb1913df6e017b03b069be10331f.png', '2017-03-22 15:01:29', '2017-11-06 20:09:12', NULL),
(1655, '', '', 'uploads/edit_content_folder/323f11295a1037e9d0950e37da68e7d9.png', '2017-03-22 15:01:29', '2017-11-06 20:09:12', NULL),
(1656, '', '', 'uploads/edit_content_folder/c9f0fcbf5810744a623c66a71a4bcb0f.png', '2017-03-22 15:01:29', '2017-11-06 20:09:12', NULL),
(1657, '', '', 'uploads/edit_content_folder/d8419ed01a42202e012b121f54052829.png', '2017-03-22 15:01:29', '2017-11-06 20:09:12', NULL),
(1658, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/546facd4cc2ead96c9d90d0eacce3b0f.jpg', '2017-03-22 15:01:29', '2017-04-17 10:06:58', NULL),
(1659, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/fd94f37677d625f184f5280926084789.jpg', '2017-03-22 15:01:29', '2017-04-17 10:06:58', NULL),
(1660, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/fa5fb3a989ecf1c44b9fc809b54c6045.jpg', '2017-03-22 15:01:29', '2017-04-17 10:06:58', NULL),
(1661, '', '', 'uploads/currencies//1004ae2f6aff873a75dc9a50ace10bec.png', '2017-03-22 15:09:48', '2017-03-22 15:09:48', NULL),
(1662, '', '', 'uploads/currencies//634067958ac8be858c11d951a7611052.gif', '2017-03-22 15:10:23', '2017-03-22 15:10:23', NULL),
(1663, '', '', 'uploads/currencies//130de87a77ca508f8f568fb1bfde3085.gif', '2017-03-22 15:11:28', '2017-03-22 15:11:28', NULL),
(1664, '', '', 'uploads/currencies//be8a7e3674c8a38030e35644b75fe4a7.gif', '2017-03-22 15:11:33', '2017-03-22 15:11:33', NULL),
(1665, 'اللغه العربية', 'اللغه العربية', 'uploads/langs/ar/3b460d7791b6ba1ba3cb7ff291eba905.png', '2017-03-22 15:13:31', '2017-11-05 19:39:06', NULL),
(1666, 'Français', 'Français', 'uploads/langs/fr/c266cbfb8a37bfa402f3fbd8e71a2d81.gif', '2017-03-22 15:13:58', '2017-03-22 15:13:58', NULL),
(1667, 'موقع الكويت', 'موقع الكويت', 'uploads/general_edit_content/6c3f819348cec6d7da3200d43cf5a8f9.png', '2017-03-22 15:18:43', '2017-11-06 20:15:29', NULL),
(1668, '', '', 'uploads/general_edit_content/e956df4e1618fa835e83c8288f2ee677.png', '2017-03-22 15:18:43', '2017-11-06 20:15:29', NULL),
(1669, '', '', 'uploads/general_edit_content/f6b4b457dbc0dfd29af0cd7fca0bbf92.png', '2017-03-22 15:18:43', '2017-11-06 20:09:12', NULL),
(1670, '', '', 'uploads/general_edit_content/0f2f2a13f0aa078738a1c00deca86574.jpg', '2017-03-22 15:18:43', '2017-11-06 20:09:12', NULL),
(1671, '', '', 'uploads/edit_content_folder/ab909569b9afbc5af1d4247c3dd1efee.jpg', '2017-03-22 15:20:55', '2017-04-13 13:36:13', NULL),
(1672, '', '', 'uploads/edit_content_folder/754150e83e5cfe5ba61812b051ac5fd8.jpg', '2017-03-22 15:20:55', '2017-04-13 14:38:38', NULL),
(1673, '', '', 'uploads/edit_content_folder/8ec532f5fb6d122c3a73a7f5e193b616.jpg', '2017-03-22 15:20:55', '2017-04-13 13:36:13', NULL),
(1674, '', '', 'uploads/pages/slider/907a7beb114c66148f344782fe88d0b5.jpg', '2017-03-22 15:28:56', '2017-03-27 12:26:13', NULL),
(1675, '', '', 'uploads/pages/slider/ca543d32e2a82efc625eb0b6f1dbf124.jpg', '2017-03-22 15:28:56', '2017-03-27 12:26:13', NULL),
(1676, '', '', 'uploads/category//34c2f0f996e099c6380e9a953a56b1e9.jpg', '2017-03-22 15:36:22', '2017-05-06 21:30:27', NULL),
(1677, '', '', 'uploads/category//3b660f83984c178ca8136eadeb2053cd.jpg', '2017-03-22 15:36:22', '2017-05-06 21:30:27', NULL),
(1678, '', '', 'uploads/category//cf04be4d50ddd89d4f54a9d19c23b904.jpg', '2017-03-22 15:36:24', '2017-03-22 15:36:24', NULL),
(1679, '', '', 'uploads/category//0f944421a0021431251b9de1dfa0d3e4.jpg', '2017-03-22 15:36:24', '2017-03-22 15:36:24', NULL),
(1680, '', '', 'uploads/category//4d9573e85ec27ba2ef83f311171cf42f.jpg', '2017-03-22 15:37:44', '2017-05-06 21:00:46', NULL),
(1681, '', '', 'uploads/category//536cdb890346ea14c8221ecf5982586f.jpg', '2017-03-22 15:37:44', '2017-05-06 21:00:46', NULL),
(1682, '', '', 'uploads/pages/slider/79438b3619f487e923256bf27276c871.jpg', '2017-03-22 15:37:44', '2017-04-20 13:04:50', NULL),
(1683, '', '', 'uploads/pages/slider/093f8980ae7f4ec4e8478d3c1e22100f.jpg', '2017-03-22 15:37:44', '2017-04-20 13:04:50', NULL),
(1684, '', '', 'uploads/pages/slider/70da02293474c117153a20c2a21bcc01.jpg', '2017-03-22 15:37:44', '2017-04-20 13:04:50', NULL),
(1685, '', '', 'uploads/category//70b7fb245589aef66d7910eacf255c71.jpg', '2017-03-22 15:39:51', '2017-03-22 15:42:55', NULL),
(1686, '', '', 'uploads/category//631cbee6c9e12b71de9cecd51570cfbf.jpg', '2017-03-22 15:39:51', '2017-03-22 15:42:55', NULL),
(1687, '', '', 'uploads/pages/slider/698de8aab6614d50b4483741b0e0839c.jpg', '2017-03-22 15:39:52', '2017-03-22 15:42:55', NULL),
(1688, '', '', 'uploads/pages/slider/d2c555f2eb0a8e699d1bb8427927da61.jpg', '2017-03-22 15:39:52', '2017-03-22 15:42:55', NULL),
(1689, 'Nile Cruises', 'Nile Cruises', 'uploads/category//a3f7af55b4849067e64f883a26d6a698.jpg', '2017-03-22 15:41:16', '2017-03-22 15:42:41', NULL),
(1690, 'Nile Cruises', 'Nile Cruises', 'uploads/category//e0bec88aade3fd155c39a02816b1e44d.jpg', '2017-03-22 15:41:16', '2017-03-22 15:42:42', NULL),
(1691, 'Nile Cruises', 'Nile Cruises', 'uploads/pages/slider/ed5a76b451344a027dbc377b35931cb9.jpg', '2017-03-22 15:41:16', '2017-03-22 15:42:42', NULL),
(1692, '', '', 'uploads/category//953eba1f9b6e61a18e983598d55130de.jpg', '2017-03-22 15:41:44', '2017-04-13 13:03:00', NULL),
(1693, '', '', 'uploads/category//53fcf62af14374fa6f76def2bbe6a238.jpg', '2017-03-22 15:41:44', '2017-04-13 13:03:00', NULL),
(1694, '', '', 'uploads/category//222e4ae17a2e161adf4254c040d5cc09.jpg', '2017-03-22 15:49:24', '2017-03-22 15:49:24', NULL),
(1695, '', '', 'uploads/category//f46d887611fe01f139c961195167433f.jpg', '2017-03-22 15:49:24', '2017-03-22 15:49:24', NULL),
(1696, '', '', 'uploads/pages/slider/97a22c8e41e8da95b70a32579f2d8c71.jpg', '2017-03-22 15:49:24', '2017-03-22 15:49:24', NULL),
(1697, '', '', 'uploads/pages/slider/fe800baf3c523d512bcf83a28d2ea371.jpg', '2017-03-22 15:49:24', '2017-03-22 15:49:24', NULL),
(1698, 'Aswan Day Tours', 'Aswan Day Tours', 'uploads/category//a9bf6f806b4918b4e1a13f924eb944d5.jpg', '2017-03-22 15:51:12', '2017-03-22 15:51:12', NULL),
(1699, 'Aswan Day Tours', 'Aswan Day Tours', 'uploads/category//150699f94538713da5b7cd79c02e56ad.jpg', '2017-03-22 15:51:12', '2017-03-22 15:51:12', NULL),
(1700, 'Aswan Day Tours', 'Aswan Day Tours', 'uploads/pages/slider/305b87f57ee3362f6c9c8f44831e8863.jpg', '2017-03-22 15:51:12', '2017-03-22 15:51:12', NULL),
(1701, 'Luxur Day Tours', 'Luxur Day Tours', 'uploads/category//8912477ff05d7e264aa9926878e94703.jpg', '2017-03-22 15:52:13', '2017-03-22 15:52:13', NULL),
(1702, 'Luxur Day Tours', 'Luxur Day Tours', 'uploads/category//3f93c81400473cc6af705d7c0ecd470b.jpg', '2017-03-22 15:52:13', '2017-03-22 15:52:13', NULL),
(1703, '', '', 'uploads/pages/slider/1521caf8e685c94a235448b58a3202e3.jpg', '2017-03-22 15:52:13', '2017-03-22 15:52:13', NULL),
(1704, 'Lake Nasser Criuses', 'Lake Nasser Criuses', 'uploads/category//e06cc12a485740082638499130bb891c.jpg', '2017-03-22 15:56:38', '2017-03-22 15:56:38', NULL),
(1705, 'Lake Nasser Criuses', 'Lake Nasser Criuses', 'uploads/category//499724533dfac062408195f52f70384d.jpg', '2017-03-22 15:56:38', '2017-03-22 15:56:38', NULL),
(1706, '', '', 'uploads/pages/slider/8e5a78c72fb74873c978c2c71a8f0129.jpg', '2017-03-22 15:56:38', '2017-03-22 15:56:38', NULL),
(1707, '', '', 'uploads/pages/9fe56b5805e6e8873a41b79402cebc2b.jpg', '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(1708, '', '', 'uploads/pages/4cdcf73494d89161c0977f2efbe8d1f1.jpg', '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(1709, '', '', 'uploads/pages/slider/a7e0b316d4a0a221208594aa1f361ae8.jpg', '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(1710, '', '', 'uploads/pages/slider/6cb7f91002d5482ed2a10c26f2b34056.jpg', '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(1711, '', '', 'uploads/pages/slider/917af094a7aa3d2829d239b63c17099c.jpg', '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(1712, 'Nile Cruise Luxur Aswan', 'Nile Cruise Luxur Aswan', 'uploads/category//64a256c7d0bcd02c892cb521b78148f4.jpg', '2017-03-22 15:58:15', '2017-03-22 15:58:15', NULL),
(1713, 'Nile Cruise Luxur Aswan', 'Nile Cruise Luxur Aswan', 'uploads/category//0c984b2a36870630d5a18809223d153d.jpg', '2017-03-22 15:58:15', '2017-03-22 15:58:15', NULL),
(1714, '', '', 'uploads/pages/slider/e96f8b40806192a02b93aba52ee2db3a.jpg', '2017-03-22 15:58:15', '2017-03-22 15:58:15', NULL),
(1715, '', '', 'uploads/pages/b97948c3720e16b98ae67ec3c175bfd9.jpg', '2017-03-22 16:01:40', '2017-04-17 09:50:50', NULL),
(1716, '', '', 'uploads/pages/240837119452067eae4e1fe4aacf6a5f.jpg', '2017-03-22 16:01:40', '2017-04-17 09:50:50', NULL),
(1717, '', '', 'uploads/pages/7189b13a72279a55b8ba0699a126633f.jpg', '2017-03-22 16:03:17', '2017-04-20 13:15:00', NULL),
(1718, '', '', 'uploads/pages/2c7c68ee7929b3fda62b92ff184f3d41.jpg', '2017-03-22 16:03:17', '2017-04-20 13:15:00', NULL),
(1719, '', '', 'uploads/pages/slider/e3a56a8f5a6fc7f67f7d19692b60de99.jpg', '2017-03-22 16:03:17', '2017-03-27 12:26:49', NULL),
(1720, '', '', 'uploads/pages/0f371dba9f04746cfd2f7a0ee5a7cbdb.jpg', '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(1721, '', '', 'uploads/pages/029addbc12429acb3536bfab5e7700fb.jpg', '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(1722, '', '', 'uploads/pages/slider/87ef72680ea1e6fa18ec904dab05ce1c.jpg', '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(1723, '', '', 'uploads/pages/slider/4554a0fd2e7eb94bc46c061a7d8e3636.jpg', '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(1724, '', '', 'uploads/category//9cc4887a6b4f910f7189451acc256eb9.jpg', '2017-03-22 16:05:38', '2017-03-22 16:05:38', NULL),
(1725, '', '', 'uploads/category//d0c563e7eeba190d74a0cc683537ec7d.jpg', '2017-03-22 16:05:38', '2017-03-22 16:05:38', NULL),
(1726, 'Luxury Trip one', 'Luxury Trip one', 'uploads/pages/df0b1a9886269cd0165e790b968c753f.jpg', '2017-03-22 16:05:58', '2017-04-20 14:56:49', NULL),
(1727, 'Luxury Trip one', 'Luxury Trip one', 'uploads/pages/a032ceebe4562de7240a86730f8fcb60.jpg', '2017-03-22 16:05:58', '2017-04-20 14:56:49', NULL),
(1728, 'Luxury Trip one', 'Luxury Trip one', 'uploads/pages/slider/ba85ed098df98013dfd42137ff869bb8.jpg', '2017-03-22 16:05:58', '2017-04-20 14:51:03', NULL),
(1729, '', '', 'uploads/pages/71bd763afd75c79e05a137e77c753b9d.jpg', '2017-03-22 16:08:15', '2017-03-22 16:08:15', NULL),
(1730, '', '', 'uploads/pages/c11774e1fe9f2020e2ad526d54e59776.jpg', '2017-03-22 16:08:15', '2017-03-22 16:08:15', NULL),
(1731, '', '', 'uploads/pages/1babfae9806baf72e2b644d148fd578d.jpg', '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(1732, '', '', 'uploads/pages/38cdc47a77bafa2625011cc375548ad0.jpg', '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(1733, '', '', 'uploads/pages/slider/3164b27589134c79f841a179d8e2e1a1.jpg', '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(1734, '', '', 'uploads/pages/slider/70145eed824806ce48e8971508149df3.jpg', '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(1735, '', '', 'uploads/pages/59c7c15daad6eeaf1cc12c8454fc21cc.jpg', '2017-03-22 16:09:49', '2017-03-22 16:09:49', NULL),
(1736, '', '', 'uploads/pages/1cf40fe827938e152a00b52e269127de.jpg', '2017-03-22 16:09:49', '2017-03-22 16:09:49', NULL),
(1737, '', '', 'uploads/pages/f972e120ef41489273fdabb6663430e7.jpg', '2017-03-22 16:10:52', '2017-04-23 11:31:46', NULL),
(1738, '', '', 'uploads/pages/23b43613ce52687d121c1453a135bce4.jpg', '2017-03-22 16:10:52', '2017-04-23 11:31:46', NULL),
(1739, '', '', 'uploads/pages/4cf7d748a62c1a1b487db72823adfeea.jpg', '2017-03-22 16:12:40', '2017-03-22 16:12:40', NULL),
(1740, '', '', 'uploads/pages/a864075ae76b67d4c03fa9e27e3945e2.jpg', '2017-03-22 16:12:40', '2017-03-22 16:12:40', NULL),
(1741, '', '', 'uploads/pages/2f159b259afb877844d8f3eb24e057cf.jpg', '2017-03-22 16:13:34', '2017-03-22 16:13:34', NULL),
(1742, '', '', 'uploads/pages/17f6c68b27ef9fe43a7e79dda5c4a756.jpg', '2017-03-22 16:13:34', '2017-03-22 16:13:34', NULL),
(1743, 'France', 'France', 'uploads/category//b0b685b77b65d5a393cda4ccee2eed56.jpg', '2017-03-22 16:14:30', '2017-03-22 16:14:30', NULL),
(1744, 'France', 'France', 'uploads/category//9d6d9be3feaa67967eb61c4e0cad8fc6.jpg', '2017-03-22 16:14:30', '2017-03-22 16:14:30', NULL),
(1745, '', '', 'uploads/pages/c70396ad9148b75e9f1d525f41c88e6f.jpg', '2017-03-22 16:15:32', '2017-03-22 16:15:32', NULL),
(1746, '', '', 'uploads/pages/4ceaa37c5127308d736858823ffaf033.jpg', '2017-03-22 16:15:32', '2017-03-22 16:15:32', NULL),
(1747, '', '', 'uploads/pages/2cd4888b049df77949b9a8c9eff1ad48.jpg', '2017-03-22 16:16:22', '2017-03-22 16:16:22', NULL),
(1748, '', '', 'uploads/pages/1fa6c804e84c8056cb4bc6ec871a3557.jpg', '2017-03-22 16:16:22', '2017-03-22 16:16:22', NULL),
(1749, '', '', 'uploads/category//445feacb8a2d2f4ac3701080c3e4c6da.jpg', '2017-03-22 16:16:32', '2017-03-22 16:21:04', NULL),
(1750, '', '', 'uploads/category//69de51f181556ca7214e8a38cda498a0.jpg', '2017-03-22 16:16:32', '2017-03-22 16:21:04', NULL),
(1751, '', '', 'uploads/pages/ba9713cb8cc66b2ff499a68786731d71.jpg', '2017-03-22 16:17:22', '2017-03-22 16:17:22', NULL),
(1752, '', '', 'uploads/pages/ad368ad9b70a03ebe6a3601739894086.jpg', '2017-03-22 16:17:22', '2017-03-22 16:17:22', NULL),
(1753, '', '', 'uploads/pages/1c9fe0e4d0ca0d3614be903af1fb3237.jpg', '2017-03-22 16:18:42', '2017-03-22 16:18:42', NULL),
(1754, '', '', 'uploads/pages/3965bdd355286f440ac95d908f4f4412.jpg', '2017-03-22 16:18:42', '2017-03-22 16:18:42', NULL),
(1755, '', '', 'uploads/pages/slider/1b1736731ffa6d1663a5b3cd2104fddf.jpg', '2017-03-22 16:18:42', '2017-03-22 16:18:42', NULL),
(1756, '', '', 'uploads/pages/9f63cb0aedeb1264f527eb4f3340ce63.jpg', '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(1757, '', '', 'uploads/pages/8470b0ba44914f03d44a9a4b5dc342f3.jpg', '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(1758, '', '', 'uploads/pages/slider/efdbdddca03e4ea69f217605d94eb163.jpg', '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(1759, '', '', 'uploads/pages/slider/5f7b614332505184e307d4a641a81231.jpg', '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(1760, '', '', 'uploads/pages/c2bb93cd482cd6f213ee8f27adbff758.jpg', '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(1761, '', '', 'uploads/pages/c82f10913a742b7959233bf79ad65d22.jpg', '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(1762, '', '', 'uploads/pages/slider/30ce68a4741804025a895b4de7ec1e23.jpg', '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(1763, '', '', 'uploads/pages/slider/6cfea6f278afa2eb7077ec4af6da2234.jpg', '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(1764, '', '', 'uploads/pages/slider/c03be5a108559b45ac16a4f67878a2f3.jpg', '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(1765, '', '', 'uploads/general_edit_content/d7c7ae9506a8682dd4a2ce6f46588e24.jpg', '2017-03-22 16:27:55', '2017-11-06 20:00:36', NULL),
(1766, '', '', 'uploads/general_edit_content/f164d7aadf1c7aa174d12183843598a5.png', '2017-03-26 12:18:58', '2017-11-06 20:07:41', NULL),
(1767, '', '', 'uploads/langs/FR/e82f8a2b6ddfa31f807ab8f7a8e23fc9.png', '2017-04-03 09:17:48', '2017-04-19 13:56:49', NULL),
(1768, '', '', 'T', '2017-04-03 10:02:14', '2017-04-03 10:02:14', NULL),
(1769, '', '', 'uploads/category//11f053879dbdefe98df421e07fd07565.png', '2017-04-03 10:10:47', '2017-04-03 10:10:47', NULL),
(1770, '', '', 'uploads/category//d58be942cd28ff39074e0b72c9cec7b9.png', '2017-04-03 10:10:47', '2017-04-03 10:10:47', NULL),
(1771, '', '', 'uploads/pages/bc9aa9000e863b182c29b95bab4f2476.png', '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(1772, '', '', 'uploads/pages/e28cf0b22ac413f289a33b6743199192.png', '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(1773, '', '', 'uploads/pages/slider/889f1d7e50c3d41be0d66e9517487c10.png', '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(1774, '', '', 'uploads/pages/slider/f61db48c006ae64fa8670a4c30b36288.png', '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(1775, '', '', 'uploads/pages/slider/f61db48c006ae64fa8670a4c30b36288.png', '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(1776, '', '', 'uploads/pages/c71e168037c833a4ae9444393cc6003c.png', '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(1777, '', '', 'uploads/pages/bf87e3854b9248bc7e5dfb15ab82508c.png', '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(1778, '', '', 'uploads/pages/slider/b48cc9fb282c48255c8acc20c611168f.png', '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(1779, '', '', 'uploads/pages/slider/735e7ef4f327ad67d4b96c21090f8dd7.png', '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(1780, '', '', 'uploads/pages/slider/c9a44226c0320d8fe863afad158222d4.png', '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(1781, 'Spain ', 'Spain', 'uploads/langs/SP/39840b7122a51a30c5a8b1907cc1e54c.png', '2017-04-13 12:50:28', '2017-04-19 13:57:15', NULL),
(1782, '', '', 'T', '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(1783, '', '', 'uploads/pages/9f7e3d9157b18afb2050f8b6d4c13ae3.jpg', '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(1784, 'Pyramids', 'Pyramids', 'uploads/edit_content_folder/a2125faaed216c240dec952046bac694.jpg', '2017-04-13 13:36:13', '2017-04-13 14:38:38', NULL),
(1785, 'Aswan', 'Aswan', 'uploads/edit_content_folder/54d8241c69321b74a703d0a461588271.jpg', '2017-04-13 14:34:09', '2017-04-13 14:38:38', NULL),
(1786, '', '', 'uploads/edit_content_folder/a1daaee246222031a813a612eb3bd24c.jpg', '2017-04-13 14:38:38', '2017-04-13 14:38:38', NULL),
(1787, '', '', 'uploads/category//d39e09e569dcaee09721847a205271cd.png', '2017-04-13 14:43:54', '2017-04-17 10:38:01', NULL),
(1788, '', '', 'uploads/category//d2e70ecb6e363bfc4fef50197de8dc27.jpg', '2017-04-13 14:43:54', '2017-04-17 10:38:01', NULL),
(1789, '', '', 'uploads/category//540aaf0e35379ee874b52143983724d9.jpg', '2017-04-13 15:12:42', '2017-04-13 15:12:42', NULL),
(1790, '', '', 'uploads/category//7a76b5cfdc4b8f08d7fdd26dd73d6fc9.jpg', '2017-04-13 15:12:42', '2017-04-13 15:12:42', NULL),
(1791, '', '', 'uploads/pages/slider/91f62faf2ae96cf964506e0a78c45b8d.jpg', '2017-04-13 15:12:42', '2017-04-13 15:12:42', NULL),
(1792, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/0788e0feb2b8ec3d25d58bf33ca65873.jpg', '2017-04-13 15:20:42', '2017-04-13 15:32:22', NULL),
(1793, '', '', 'uploads/category//5bf79947c631364a77c1603f7b432c68.jpg', '2017-04-13 15:36:40', '2017-04-13 15:36:40', NULL),
(1794, '', '', 'uploads/category//e42b3b780a6fe5aebdf8a7218cac2391.jpg', '2017-04-13 15:36:40', '2017-04-13 15:36:40', NULL),
(1795, '', '', 'uploads/pages/slider/2c187e046b09ed71605e50298512bf46.jpg', '2017-04-13 15:36:40', '2017-04-13 15:36:40', NULL),
(1796, '', '', 'T', '2017-04-13 16:31:02', '2017-04-13 16:31:02', NULL),
(1797, '', '', 'uploads/pages/slider/15f4006380ec64f4f4c6e473bf8bfc82.jpg', '2017-04-15 11:32:16', '2017-04-17 09:50:50', NULL),
(1798, '', '', 'uploads/pages/slider/7bd79d653affe03bbbc2685268a044cd.jpg', '2017-04-15 11:32:16', '2017-04-17 09:50:50', NULL),
(1799, '', '', 'uploads/category//f62e35269ddd149d4e5ea9058a35d5f5.jpg', '2017-04-15 12:38:13', '2017-04-15 12:38:13', NULL),
(1800, '', '', 'uploads/category//ce14d6f984f16ef963cffaf5e8088b73.jpg', '2017-04-15 12:38:13', '2017-04-15 12:38:13', NULL),
(1801, '', '', 'uploads/category//3312e5e6a984654c846bb421dd3068aa.jpg', '2017-04-17 09:42:40', '2017-04-17 09:44:09', NULL),
(1802, '', '', 'uploads/category//8410a4b39b186c082b169275793554db.jpg', '2017-04-17 09:42:40', '2017-04-17 09:44:09', NULL),
(1803, 'bruges', 'bruges', 'uploads/pages/slider/7c974dd79f6719427a2cd0f1dbbe7d71.jpg', '2017-04-17 09:42:40', '2017-04-17 09:44:09', NULL),
(1804, 'bruges', 'bruges', 'uploads/pages/slider/ec2184c989caac4e873d6e8a3133443a.jpg', '2017-04-17 09:42:40', '2017-04-17 09:44:09', NULL),
(1805, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/51fbdebd1ce76bb3c9b09f41b9e74176.jpg', '2017-04-17 10:01:10', '2017-11-06 20:09:12', NULL),
(1806, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/6fbfc0c72b0d709ca0f950e7098b476f.png', '2017-04-17 10:11:13', '2017-11-06 20:09:12', NULL),
(1807, '', '', 'uploads/edit_content_folder/6fbfc0c72b0d709ca0f950e7098b476f.png', '2017-04-17 10:11:13', '2017-04-17 10:11:13', NULL),
(1808, '', '', 'uploads/category//7f78557f6b88e4e3d22d381a56629ceb.jpeg', '2017-04-17 10:43:58', '2017-04-18 11:57:24', NULL),
(1809, '', '', 'uploads/category//35af8681a9a8c6aff6406f24024dce0b.jpeg', '2017-04-17 10:43:58', '2017-04-18 11:57:24', NULL),
(1810, '', '', 'uploads/pages/slider/0ae65be0804d5adeb5cb3a15f637e88c.jpg', '2017-04-20 13:13:34', '2017-04-20 13:15:00', NULL),
(1811, '', '', 'uploads/pages/slider/abbbf9be920706d13e1025600a66673f.jpg', '2017-04-20 13:13:34', '2017-04-20 13:15:00', NULL),
(1812, '', '', 'uploads/pages/slider/fd654b90980aceb5cc436e3c8428a1bb.jpg', '2017-04-20 13:13:34', '2017-04-20 13:15:00', NULL),
(1813, '', '', 'uploads/pages/slider/aeae59c2e12bc69f74c875b5c45976f1.jpg', '2017-04-20 14:52:04', '2017-05-06 21:00:46', NULL),
(1814, '', '', 'uploads/pages/slider/f3c0c44f8c6753bc9dd04f58e27135aa.jpg', '2017-04-20 14:52:04', '2017-05-06 21:00:46', NULL),
(1815, '', '', 'uploads/pages/slider/7423c770758d3e0d7dbc09c3bdda481c.jpg', '2017-04-20 14:52:04', '2017-05-06 21:00:46', NULL),
(1816, '', '', 'uploads/pages/slider/17237170bf197ffbe89e7f55f564e9b8.jpg', '2017-04-20 14:52:04', '2017-05-06 21:00:46', NULL),
(1817, '', '', 'uploads/pages/slider/bcba924b93b60377b05bc042d356071a.jpg', '2017-04-20 14:53:59', '2017-04-20 14:56:49', NULL),
(1818, '', '', 'uploads/general_edit_content/adce155d9fa6f569f5fbf9318ab027f4.png', '2017-04-20 15:39:53', '2017-05-06 09:40:04', NULL),
(1819, '', '', 'uploads/general_edit_content/6fa2724251d4a5efda46aaa5bf9786fc.jpg', '2017-04-20 15:39:53', '2017-05-06 09:40:04', NULL),
(1820, 'عن الموقع', 'عن الموقع', 'uploads/pages/fd282122412085d343ed592c4dc6835c.jpg', '2017-04-23 10:48:25', '2017-11-06 20:25:08', NULL),
(1821, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/d2645810c275b0f163d983ccf4889037.jpg', '2017-04-23 11:21:48', '2017-11-06 20:09:12', NULL),
(1822, 'Travel Corner', '', 'uploads/edit_content_folder/d2645810c275b0f163d983ccf4889037.jpg', '2017-04-23 11:21:48', '2017-11-06 20:09:12', NULL),
(1823, '', '', 'uploads/general_edit_content/f688fb717d6142e6886d6f82d5a20bca.png', '2017-05-06 09:40:04', '2017-05-06 09:40:04', NULL),
(1824, '', '', 'uploads/general_edit_content/baf5acead51c606c7bdf407a2eaae091.png', '2017-05-06 09:40:04', '2017-05-06 09:40:04', NULL),
(1825, '', '', 'uploads/edit_content_folder/5642c8dd00d63d48953ee6496e64b12a.png', '2017-05-06 09:40:05', '2017-05-06 09:40:05', NULL),
(1826, '', '', 'uploads/edit_content_folder/07270209e4f8fd8ebc2549198ff0cad7.png', '2017-05-06 09:40:05', '2017-05-06 09:40:05', NULL),
(1827, '', '', 'uploads/edit_content_folder/5a1fc42648a2895fb66cf193ed303012.png', '2017-05-06 09:40:05', '2017-05-06 09:40:05', NULL),
(1828, '', '', 'uploads/edit_content_folder/65767e220942b9a7bdb7ecfeac3f7332.png', '2017-05-06 09:40:05', '2017-05-06 09:40:05', NULL),
(1829, 'Travel Corner', 'Travel Corner', 'uploads/edit_content_folder/7ab058a625f19ee0377baa8c91a1910f.jpg', '2017-05-06 09:40:05', '2017-05-06 09:40:05', NULL),
(1830, '', '', 'T', '2017-05-06 22:40:00', '2017-05-06 22:43:50', NULL),
(1831, '', '', 'T', '2017-05-06 22:40:00', '2017-05-06 22:43:50', NULL),
(1832, '', '', 'uploads/pages/slider/9c964579af2df7601df30ee64b620c26.jpg', '2017-05-06 22:42:04', '2017-05-06 22:43:50', NULL),
(1833, '', '', 'T', '2017-05-06 22:48:23', '2017-05-07 13:25:33', NULL),
(1834, '', '', 'uploads/pages/d60bc6da2376675822243043bcd14ae5.jpg', '2017-05-06 22:48:23', '2017-05-07 13:25:33', NULL),
(1835, 'Portuguese', 'Portuguese', 'uploads/langs/PT/91d8fa76d84270e61b70c8a218d273cc.png', '2017-05-07 14:06:41', '2017-05-07 14:08:00', NULL),
(1836, 'Middel East', 'Middel East', 'uploads/edit_content_folder/9ae305efa1e473127422800bb8125376.jpg', '2017-05-08 15:35:31', '2017-11-06 20:09:12', NULL),
(1837, 'Europe', 'Europe', 'uploads/edit_content_folder/67c119020a9f0778dbe9e5490ee3171f.jpg', '2017-05-08 15:35:31', '2017-11-06 20:09:12', NULL),
(1838, 'Americas', 'Americas', 'uploads/edit_content_folder/89c5616a05e564091565edf5c386a8b5.jpg', '2017-05-08 15:35:31', '2017-11-06 20:09:12', NULL),
(1839, 'LAST MINUTE SALE 30% - 60% off', 'LAST MINUTE SALE 30% - 60% off', 'uploads/edit_content_folder/fdae45a4c18a69771987984d4d3f9b28.jpg', '2017-05-09 08:52:52', '2017-11-06 20:09:12', NULL),
(1840, 'Latest Packages ', 'Latest Packages ', 'uploads/edit_content_folder/520f20071d0e00579e72b6fea2c5ac97.jpg', '2017-05-09 08:52:52', '2017-11-06 20:09:12', NULL),
(1841, '', '', 'T', '2017-05-09 09:56:30', '2017-05-09 09:56:30', NULL),
(1842, '', '', 'uploads/pages/046cd796fb56bbd5f6b336d2a02e782e.jpg', '2017-11-05 19:50:58', '2017-11-06 20:27:55', NULL),
(1843, '', '', 'uploads/pages/46462702668808e026a3765c30d43892.jpg', '2017-11-05 19:50:58', '2017-11-06 20:27:55', NULL),
(1844, 'صباغ', 'صباغ', 'uploads/pages/c8c647f51cd65ca8c4db937a35f7fd74.jpg', '2017-11-06 20:29:36', '2017-11-06 20:29:36', NULL),
(1845, 'صباغ', 'صباغ', 'uploads/pages/e3f5895dba63776bb028fe71eec765b8.jpg', '2017-11-06 20:29:36', '2017-11-06 20:29:36', NULL),
(1846, 'نقل عفش', 'نقل عفش', 'uploads/pages/21d7171b7b7f338a22e07d656cae3493.jpg', '2017-11-06 20:31:31', '2017-11-06 20:46:32', NULL),
(1847, 'نقل عفش', 'نقل عفش', 'uploads/pages/bbbfe4384837f6506abbc405ef679272.jpg', '2017-11-06 20:31:31', '2017-11-06 20:46:32', NULL),
(1848, 'ونش سيارات', 'ونش سيارات', 'uploads/pages/1cca1b2c9e3e3488fdee993cc5b16871.jpg', '2017-11-06 20:35:01', '2017-11-06 20:35:01', NULL),
(1849, 'ونش سيارات', 'ونش سيارات', 'uploads/pages/e4850c5fd2ef391de599f0b70bdbefdb.jpg', '2017-11-06 20:35:01', '2017-11-06 20:35:01', NULL),
(1850, 'مكافحة حشرات', 'مكافحة حشرات', 'uploads/pages/2e6ceb7b5cb4b2ccc1eafd029a81ab3c.jpg', '2017-11-06 20:36:34', '2017-11-06 20:46:44', NULL),
(1851, 'مكافحة حشرات', 'مكافحة حشرات', 'uploads/pages/10e571be145eff9f51a6aaa3e657164e.jpg', '2017-11-06 20:36:34', '2017-11-06 20:46:44', NULL),
(1852, 'مدرسين', 'مدرسين', 'uploads/pages/fae2f8000604f5125f6205f716cab8d9.jpg', '2017-11-06 20:37:11', '2017-11-06 20:46:50', NULL),
(1853, 'مدرسين', 'مدرسين', 'uploads/pages/108fdb42f663add17fdf7ed2a90f4dee.jpg', '2017-11-06 20:37:11', '2017-11-06 20:46:50', NULL),
(1854, 'طيران وحجوزات', 'طيران وحجوزات', 'uploads/pages/816b85307e6479f137d70aeb4442159a.jpg', '2017-11-06 20:38:12', '2017-11-06 20:46:57', NULL),
(1855, 'طيران وحجوزات', 'طيران وحجوزات', 'uploads/pages/6e3371beab659680268418e8ab2cde7e.jpg', '2017-11-06 20:38:12', '2017-11-06 20:46:57', NULL),
(1856, 'فنى ستلايت', 'فنى ستلايت', 'uploads/pages/6dbd35b3c342280fb3061cf1a5ac07a1.jpg', '2017-11-06 20:39:10', '2017-11-06 20:47:04', NULL),
(1857, 'فنى ستلايت', 'فنى ستلايت', 'uploads/pages/f4bffb8ab53f8e7d41e8f6318645d0d2.jpg', '2017-11-06 20:39:10', '2017-11-06 20:47:04', NULL),
(1858, 'مكتب عقاري', 'مكتب عقاري', 'uploads/pages/c5b95ac79b6f3fe18e90b0623a2ff1d7.jpg', '2017-11-06 20:40:42', '2017-11-06 20:47:09', NULL),
(1859, 'مكتب عقاري', 'مكتب عقاري', 'uploads/pages/190c7ada192bd24aaea52f08f0519217.jpg', '2017-11-06 20:40:42', '2017-11-06 20:47:09', NULL),
(1860, 'تأجير سيارات', 'تأجير سيارات', 'uploads/pages/7f155d65510087c931eaab1f767a7661.jpg', '2017-11-06 20:42:37', '2017-11-06 20:42:37', NULL),
(1861, 'تأجير سيارات', 'تأجير سيارات', 'uploads/pages/f2f4f854d8ae990f9a45f5826301ea64.jpg', '2017-11-06 20:42:37', '2017-11-06 20:42:37', NULL),
(1862, 'تعليم قيادة ', 'تعليم قيادة ', 'uploads/pages/1f93c4847a079b869ba6f4b43562ea7b.jpg', '2017-11-06 20:44:01', '2017-11-06 20:44:01', NULL),
(1863, 'تعليم قيادة ', 'تعليم قيادة ', 'uploads/pages/198ad71ae0337e31caafb06867b1c0c9.jpg', '2017-11-06 20:44:01', '2017-11-06 20:44:01', NULL),
(1864, 'كهربائي', 'كهربائي', 'uploads/pages/9e655f78e7017ceb9eb3b83d8e4120bb.jpg', '2017-11-06 20:45:15', '2017-11-06 20:45:15', NULL),
(1865, 'كهربائي', 'كهربائي', 'uploads/pages/d49240ebbf3364f664f10fae49060f3a.jpg', '2017-11-06 20:45:15', '2017-11-06 20:45:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cache`
--

INSERT INTO `cache` (`key`, `value`, `expiration`) VALUES
('laravel_general_static_keywords_1', 'eyJpdiI6IlNrbFZnSjJNQlM1Q2VTYnlGbU9FNUE9PSIsInZhbHVlIjoib0lndDdXU2c5MGVUMW9VZ1cxTGdTZVJpZEpQSDl3MnhDYklXb3BcLzNRY2VSYXNsMkVNeHp3clRSSWIxQ2oyUmZhN2VCaXRvY2hoNHRvM1I3MDVPOFloK1NoaTV4emhnWnM4NE5QalU4S2tiUHpndVwvNk1sY2ZUTDFqYWJRdHFnbFoxQ0xYZU9xcTcycjhoalJ6OEVEMERjeXRyUFVUaHRzT0h0ZlY5cmMzdWV1SzZuTzV3RjUzNXdLTDdcL0xXZUw4OFN0QWwzN2xzN1pPUk9UN1EwY0hkb29naGVEWk5HNFBmS2ZzbE1xRElzazlNN1JMQ3pMSEVXQ0w1aElxRGFUcDVsaHVHNVB0cVRpVks2UFp4ZFVPVGNzODBGS2h5TFwvM1BDblFyV2pOTUhpeXdWV0dnTlQwWW96b1h5Qkt6NWVmSXdVejNERGsxWjE5aGlIRmUrQmZKQUxwZXBINndtRVRWT3NEcWpIT2tvY2RncVU1TFUzK2JwRWtnQ1dLc1wveHNJS2NnSjQyZTBBdVMxbXRrMGF4RUo0OEt6OStWUzNhclpFV24yZTBSdWhlZ05UZmxmazFJTEJ2V0s4VHN3cUNya2RZbWczd2dFbmRoNTA3VW15Q0hvaDlwV2liR1E2a1BrdXlORWZaSUxrTnpVUTRiQ2I1MjlZTmF2TmRZaSt5VVZFbFQrZHU4ZFd6RFBQa0xYN2FzZ3EzWHd1eEtKSlJFWWVcLzUyVDFBOFg0K2gwbmZhQTEzcHNKZmhsSWVKRFRHYTViRHhXMFZIclwvdmdsdjFaQnpWbzZKbVQ4dkZQekxqYmJ0YmRcL1dxcjFROU95c2Q5ZElPT0xmakFIWUNSQnA1ZGxWTmtKeVJReGtmMFBRUEZsWkwzalJaUnpoNm1OZHpwd3U4K1N0N1wvZm1sV2RGXC9jcHR2SFNhSkp6RCtRTVcrTTdiZis2bzBRYnZBOXJvcGdhOTdveWRHMTZCVUN5eUpiR1Q5OWM2QnQ2bWxzVndlcXVlOStvZm9YQVBDNmxxK1pXMU5GUzFuQUxmK1NzV2kzR2NTWEpTS1VEemJyTk1qV2ZXd2VYMzA5b3M9IiwibWFjIjoiOGMyYTYxNTg2MzAzY2RiZWUxMWMzNTEyMGYxMTRmYzJjZjJjNzQ0OWM2NDE5ZjgxZTdiZDU3ZmYwZTk4YzkwOCJ9', 1516385432),
('laravel_general_static_keywords_2', 'eyJpdiI6IjhwdExIcHgraFgrcVJPSDkwY1ZTMHc9PSIsInZhbHVlIjoiRG1md0lVV1dyaXhVMnh0WlVTaWNuQXdCb1E4c1VmNit2cTVVdVk1VTc0WFBCdGhqWHhXenlMb1JYQmhSa2tpRE5FbVVkRU53emRNdHNsZ3RRMzZzUEs0YkgwU1Zyb285V1hoanRFOXJqbExoXC8rZHA1Z0I2UUlSS3dIZTRGdVU0dkNKSWlFbTFQMGxDQnVzTGtyaDR4THNVZmNDQkVaNmFFZk5rOXMxTFU0NWU1XC82emtJRmJsNGNjcXQ2NEVpZlA1S09Od0FXN253TVd2YW16TkQ4OHpXcnN5YWt5dTFuMTRkOEU5bWNyZXhaYUt1SzU1T3h1QmxZSWpNMTh3S3dTZ0VtVUZBZEdManhKUWlwRWNpXC93UEVCSERQdTBwKytRYXhIelwvcGNFeWo2SUhhQk1WVXRLeWxRZVA5Q05tUTU4RDNLZjF3SjY4XC9xeVRJdWVjbTI1Qzg4YXBRelB5ZDRCc0NZT28yY2ZcL1wvVTVaalhnK1o1ZU1pSWNqRFp3QytlZ3hSc1FcLzlUQjlQdWNWc1RIYmJGSm0rQ1lkVVRld3VmNUI2eWVjemhjWHhoVWVXMUJyU0ppWFBiUG00c1c1bEM2VHhWeHNSUG5FUXJ6TTR0Q3lubUNCQjQ2OTR3Z0JlbmoycVAyOVwvdjFDVWl4ZGI1dGZOV2hDeVNYMlJxWlVxMmUwbEJYUG9QK2I0U1NrUkE4K1ZGamVNU05mS0dSbWliSitPZVYwSVpaSlFGT2VQMmI2TXg4RmdwZlMxc2JydVNUSDNoWWVrODBIbXZEWkk1Q0JBbk9KNFE1TUJXK0d4RHpjems3eVJRdnVodVVTd0loYXdXXC9YT3ZDRjNYOVRYMEN0QkFvNWdiRE4wZ05malBXSm1IcW5hVjllZkFURTJNMEw4dlpWOTZFMWtxUzhhbVdoR1pUNmtuK2pxM0drTjcySWg3K3RmSVJXKzJnUWFXc2xjb1VkdXFCOXY0XC8xRjFRZWYwTExFRHlJVzY3emF3NDFwOCtteUZnS1lGb3FFOEZMYlJkcTFlM3R3dThvYzdtbmptRElJZmViNFNFaUhhSmtkaEc0ZkYzMTBvPSIsIm1hYyI6Ijk5ZGI0NDE4ZTZhZjMxM2I5ZmFmYzRiM2ZiMTFmM2IwNmVmZjJhN2M1NjExMDgxY2I4ZDFhNWYyZTk0ZDkzYWQifQ==', 1499414752),
('laravel_support_3', 'eyJpdiI6Im5ESzVBVThjYmZRaUFYTkVONXFpZVE9PSIsInZhbHVlIjoiZElLcHhLRmI1R1h6aWFJR0xNVVMyWVk2NWdxRnFqZUxGckdaWnREWTZBQ1ZCQkFjcWZ4eHlCRW9MTmNnSVJsajRFZ1B6Z2ozUFFSUHptNWsxV0RcL0VPQUlZajlpVnRqYUVXelpiOEh5Z0x4XC9YMGhBWjVsMDRGVnFMQ2tzdEVBZ1RDQ2xhV2FQRE85Mk1XdVJCenMzWTZmU1wvNk5GTWF3em5VbVk4VGpiXC84WUNiM1wvdkg2OWtLS0pPTmVWU3NETFQ2Sitnc1NHdnNCY213bEI0SHlWZklYMDJ0YjdJUjZKc2VyWkhRUEZCeHo3XC9KWEJ6YTArZU50dEJqRm5yV09nSkNCYlAwVXZHSGhDaTRYQTllbnZQNWcxV2FnekF1WG5EdGtzYzBmM3VXQ3dBalwvbVlyN2RRaFN5T2NGMmFCcUVqdjJsRzV3YWd5QkZqWWVDazdYYVVUQnRjcUU1M0JLcVlXNm92TEJRbDd2Y0RxQWFHSXZlTHJrM0FoRmdpc1N1ZFl5d1JPKzQxSTNIaVVQMmJxU1JiWUNvVVNWVEFCYUg2dzlaN3FQd3d5RnRmVHd6WmVtcWp5UnFYYjFUMXcwQ2NXWks1SjdHMzcrMk5ydG9pY0FBTnlQVmVwNDBpeWRhN2VFb3NnRGJGRjM0Smo2d0NHWmFyOVBLZUpqa2picit6VWNmdW1rNUVwSXpDSzhjajFLU3c3Tm81c2g1eTdyUFNZaFR6ZHIyaGdic01PUEFlVDg2OGJXZGhpdUJcL0lsWEduSUVpK2psaWhLXC92bWhDenRaa0lmbXlQT1pFWllPY0RqdkcrNzVZN2tQUGRobjgrQ0F6V3g5M1FUZ2FjMHF5XC9YWTNYcWhmRWdnYnlhQzh4VHRuZXpLd1lDdWRoeUtDUW1BV21mY2lSYkU0eXExRHZUVU9mZllYUlZnMnd4cURxQURTUGZzd01OS1BpMFgzSG5VME1Xck5HVG1ZakpNR2hGck9MQ2xpRk5NOHY1am1uUWxxTHF4dlwvVFdibTdzSGtKUEs5YUhhc0lPemJNcVRzYitcLzdEU3A0dTBOXC9JYW8zcEU5K2d6aUxwOE1QblZvVVYrckFsN0ZoV25GRTB5WVNWZGRzTDZiSmhTZjVHdWkrcWt5ME9VS3hRbENjdHZKRUNCWEZFZlJLZU9sdkxyQXptZnlBQnpXYTY4OEswNkxKSmgrRG0yQVwvYVJaVTVReWVEYTRVK29TMFZCVjNxMVFiTEtySWVUREJkRGNPWVZtSDhoWE9DbmlFR1NMN041QWFkemV4cGo1WTQ1eTNJR0oyQTdEaElUTE9SdkQxTFNieEV2VmtsTEs0aWFtWnRZZGRqRGZMUHFHWnllOW01dElVWjdoUlY4RklNMHphZnNZM2prcCs1QVFCSDBuWTk0NmRKNm5LVUpESG14bTRGQ2dSTnh0MGl4TU53M0k5ZlJNUHlJVjN5Y1RQcDllWGNvQitYM1RFdlY3dXZGTG9nK1R5NFhiQmVxVDNkZjQ9IiwibWFjIjoiNjcyNjQ5NjQzNmVkMjQ2MTM3YTU1ZmY2YTQ3MzNlOTIxYjFiYjRhN2QzOGFkYjc0YzcxMTYyZTIxMGI3MmM2MSJ9', 1500389154),
('laravel_all_cats_page_1', 'eyJpdiI6IlVcL2xtcDRUWjNJVDJOQlF0VndHVHR3PT0iLCJ2YWx1ZSI6IndxYU5WMUZSb0pcL3N3VXo1XC93TTVxUWs3T28rbml1UnNiSXhtZkJjeVpYNmhZeGM4SGhZdEQ4TmdBeFRhZ2FiMHFaclwvTXVxT3picm9XZjBIRHZLTkIrUk42aXdTMHlZcmozcE1Zc0ZhelZkR3RMVHczTDIwMHl1N2V0U0hcL1B3bFZFMTVNcTRUYzNuSXdvd2JOSEVWT3haVEhsYjZuNytRTHRtQlFxcDZFM1RWWXBPN3E1OXlXZHBicVB0SktwTE91NzdOZTRoWXJVYUh6NGpQTitwaHBrTlB6UGp5UlcxdzJsNloxN0pqMm9EZ1RLWlU5T0grditXeTJ4SXVNOHpjTVNYZUpBRmdxdFwvS3dcL09BU2lBVUZ0Yk9sVWtVY0pEU3djeVErQUFwTlc1QmliQ1FERGZINncySnNJS1FXeG9PekhVUGhZVXZJXC9YbUpia1VSVzdTY1IyZTZZY0orbnU5bDBBbmQ1WmlvYlNTNmVIWWhcL2Z2VFJSZFFmSTdyR0g4Q09Qbzh5SExva3NqcklYa0xIMEhMdlh5UEtxUVNGMjJpaHRvMUozWUFpZUJvN2JWMGZcL2J2WlVNOUNvZURIK3A0cWJ4cDZcL3U5VmFRV3loY2xEWDhsSHJEMEpxMmlteTZ4TUdad0s0TytlNFhCZFNmQWE3cmlPZjFFUDJteWJNYnVKcmVRNVwvUDFYK3lnTStcL2RCM01cL2RJS0l5eFJTWFdjVFlVc29qcVZNMTRoMkd6SSt5MVBcLzJuMGpsUmIzK21JZVNQQUxnZlwvMmpBKzEwNmxrcHdVbVN2dHhUaUIwWnMyYmNDMUt0QU1McUt4NFlRVGZHVlE4a0QyTVlTeGU5MnNTT1B4czc3NXlROUtEU0ZReUtUV1p2bmhseVwvSVBXRFNyZ2Q4SUhsRHdXVHVvUlBmMkQyXC9ITU5lTFZBUGU5WEwzUnVnWFNLcW80UUlQbTVOXC94QnFpbDZuTW83M3ZYK281ajludDlcL1JPbG1waUtYVXVBTXNES0Jkclg0aXhBejRhRGJ0cEJDWkVSeWtZM2xjSTVOUUNhbE1yRGdkUStYdHZ3NWx1dkNaZzJGc1cwS0J5TzE5UnZBTVg5WDZwQStzXC96VCtkMkJoS2NsK2dxbUxXNlNLMGppSDNTWElHOXA1TUtuYzVCTERMbDZWdGNsOTdyK1V1UnA2bkRhREEzaTNHQVFHZlRPbFpOTUxBZFBoWjBPblRueDUyXC9JVkFJMGkxalFcLzY2ZjNHbkRJdjRPdlVPYzVlVG5RdnB0NHRcL0tkTmRSSWc5T1NLdWMyV3owNUdJSVhqdFYyTmhiWnI4NEZRY1dUenhVMnhzWWplMUtMVmJQamQxR2FpWExETHdHcTZZVkMrRldENlFyZWppVmRtT1pxc0QrTVBFcW9sZWYzREdTMmZpM1wvbVJlc240TG5rUXlBVU9IOFwvNGY1QWhYa05MamVTd1B0WFFrTnBZd2hYVlZlT2ErYlBGdW9sM0UzQU1tQUpEbWZkY3daU3FuNmpsQ29cL2lnU2dTcHgxSW85aWU4R0xka2hMQW5HZlRFQkdXem9Ja3J6RjFEeVdPYkdqdzJEa29WXC9lcnJrRFpFSEtENnlNd09PaEc5dTYxYUJ3VkhzeXlST3gwbE9NaU5kNE5zZ3FwZDJBWFpROEVBaFRZV3cyY1A0aTVaMTVBdHJsSExZVFlaWmpiWForYWlld3hObnNHd1cyOVwvXC9RcmVzUEtnTVVReW9FMzlodHlUYTZUSGxDRU1CZTRCZGs4ZjZ0bVZHY2lrT0ZaVUpYdks3WWo0NFRSTTJyUlFzYjAzTnZCcTFQdz09IiwibWFjIjoiYmM4MDI3YjQwMGE4MTcyZTdiNzUwM2M4ZTcxNjE1YTkyMmM4YzczODYxOWVlNzA1MzNlZDgzYzRkNzA0ZTZiZCJ9', 1498567125),
('laravel_general_static_keywords_3', 'eyJpdiI6IjlcL3VmMTlKYmNZcGtWRTR5Y21vSE1RPT0iLCJ2YWx1ZSI6IkQwT2F6akRsR0ZZbnFVMFpnbGpcL1BGUFZ0bmtPTDM1QVwvbjdEZXZ3Vm9LN205OURzM0xOM25zV2lUbVdEZ0pSSm1pUlExYjIySGRKdE9Kb2VCdHZYbWlDYk5GR0RZMVwvcjZxREJmYUNwM1FYQ1M4TnIza2t2QXR4VEV2VmNndVwvRGdsNTRXZitkTHpDempIOGR0bExnc244cXdzZjBwYTVzV0NxWngzZGl2UjRZaTRQR2tBTFduQVJmcUp3RytQXC9uNzdYSzljSEpnQlRQK3hpU1YwUWZwbEptamRPQ0ZMSThBQ1J6T1lBVjFudjBPSFp6a1V2K0xKQURKb2ljcVQ0RGVKUDVINm5cL2E1Vk9cL1VFNWlLR0lpSnhlbjBoYnZkUHlJcktIbjJGZFFKOHpBXC9TUXhJV3BSaGVSV0ZxcXQ3WVBONlh6NVhmT2k4cUtoYkk2Ym1yQ3dQY1BUSUxod2RHVXZHd2dkRGpIQXpOK1Q1TU1Ia0VqMUt0aHZvMTdzbFUwclp2WlpwaXBqMHM5Skl0Zk1KY1wvRTduOFZKd1Q0bmhxMFlLYnVhSHZVNXNYcThGbU1DMGhyYWpmUlZ1eHloSWR4NXZ4cGw5cVI0TzRVcU5qczFUTTZ1VWpsTzBZd08yRmVQVDRXbXd4QnllelA1NFdjMGN6dk9uamtIWmlpYmx0WVlIUzR1QXFXelBtajBidUIwZjZibXhCdTU1WEphQ3pBdXNkK3g0UjVvbEhOblVLaFdyVm5aQ2lvMzR2VlZuOHFYRUlrK2RyNVJWWXJzQkVFNzJ1R3Z5cXRhU0FyNFpKeG93cE50ZzRGb0hOZ2VrVlFYa1FLcWlUbmtDRndCT2Q3bFZLVzNBK2FoY0ZMeFY2cHBnbVB5ZWxPc2pOMGRBSVJ6K0ttVFBIM2dZdW9qQ0NvUWlBbkN4bEd2cjdLWGtCQnhCSisxR2hRSEdKVDJoOFIxdHhZdkMwZFZIVXVwSGdheUkxUkxUK1J4MFhrVnBrcUEwQXFhb2REeFNhT2tkaWJ2RU5rQWhWa1BIVTdPMW5OcU00TFkyMDhaSkdETFlMckJPZldnZXI4K0NiSCtEXC9IbU1pUStxQXFDNW1EMjNtZ2wrMnlod2VCUHl2c24yUWNXK3lQQUkwVmdxMk11aVRrTlFxelZjYkhxQT0iLCJtYWMiOiI4MDQwNThmZWIwYWVkZTJkOThjZDI0MTRlNzQ2OTgyMjc0NTk1YWU0Y2EyZjkzY2EzNjkxNzhjYWE2M2FjMDg3In0=', 1500389154),
('laravel_edit_index_page_3', 'eyJpdiI6Ikc4b0FXS25jem4rXC9rZk1WTE9oV2lBPT0iLCJ2YWx1ZSI6ImFYSVdtS0k3VXRsVnA0SHF1c3dUcE1oZE9TWlJaeng4enFMaHFDelg4XC9Tc0x4eE1ZUkJGRCtWMVwvazU4dnBKUlwvUWF2S0VPSElPZWxIazM0WlZibHdTREJJMzVNTmFKalMxMDlhcGFVSkN2TkYwODYzdDZRZEpPcHRjM1BHZlFIS2lqM1ZUT1BpdUFGYTlkQlNGQ1dYU2QxUGhNM3NwMUFKR3pvWkVuSHBMVmhxQXZQS2tzblZ2cUhHdmxwTU5BdjdUMHlRYnNxXC9oSXpkNWM0Tlc0eFFaNXNiNjdZSWI3WDhRWUs3UVdYbUpTejNjUmlKMHJzUmJveERlVEg0U3FESjdBaGh6WjdRNE1SWUt1Y3l6TDlzbzJzZm5cL0tET0MzOTRuOFB1aUhmSUFScVV0XC9SZlFyMkVWK3dtXC9uOGJyQUFCVmVMakZmQ0htYXdFaVwvUWFRZGFCK2JNVCtVMnJhbmVXWk5OaG5rREpBcEhta2h4RzNUdmQxNytBXC91eXpnT001S1FJczZsb242UERrczhLeVNnc3hkdjJwaW5qUXpJcndKUDVnU3J3WW5XcW9cL2VPUDhNQThMQUYrdExQNDNLT3M5TUF1em5QWEVkMzJ0WE9hN0Rqb0owMTVFbkw4dUFDblJ3TWhlbnhQTEZIUHMzcGwrQ09HUWJndDdXUUFHSVBxMllkVmRiaG83OExUbDN2cG5USWhVc3pBTWZXa21yU3NwWlwvcVFGRzBidStJNjhSZWt4bDNnQWNHSmZ0K0xscmxoVWpYbkxqb0w0ZmtsXC9oQlRrOWJFbWRSNE5HZDBkZ2UxN1g5QWc4aXNkZXRSTGVBVWJ6akJHbUFrQSsxWVZpQktHN3RIVmhZeGpJOVRWOFhZUVF4cjZsQVZTU3UwbWdpSkN5eTRJZWF4UFZRSnhkTEFKRUc1dDgzdU5qNkhOMU5kU0szb1Z5OEhcL2NlV2wxRm0zQmpranVSN2w1akg5UWxXa1hhVU5ZMDVCeVhkT29ibXI1Q20wZlVlUXcwWVhCeWFuREFSWWF3OUVwNnI4dXpjZktnczdoTVAwV2xNZWZPVjNlNFl3TlJMK1p0bGV5Um10ZW5KUjJDOGJvcDRWNzJ6N3F3NVpmRUxcL3BTRm9KMWF6UGRXcGZHREFINFBMXC9nWEhTUFNMYnRSVnlKcVcyS3FjendDSm05bkIzcXlcL0xxYktHZlNXM3lsSDBWYjk5bTlCY3VqSW8wM0xRMGtPeVNCbFwvdmtFdXhwRDN5dEN0ZjdOakxkRmdzN05sT1VBT3J1eGxmeFRDNGJmcFhGWThPQUV4Q1NYNFwvaThwXC90MkdJY3hHN0swQ0I4dW5nMEtndXhZVlRuckxJSEFtemsyaTJ4eFwvb3NkS2hQS1hPdjNWYmE3UDMzNUY2Wkx3NHBmOXp3byt5dWlKbG9sMWFlMGduS2VoTmdaT3V1bXdhV1JKRWlKUlBhM251MjdMQmhSUm04UHI3WHNMZ1BaXC9BWktPbnQ3ckwwRHJaSFowTEE5OFNXa2FOOUd6ejdOVGRqTzVqK0xPWmJmZ2ppUGFVc0E5MGZBeFZvZ0hhdmhRZHRxdkd1THI5MEVkcWVrNnptKzZmM0ZUaktXamZOakpGMjRjazhHS2Z1OXJuUnl2ZHVVaklsazlxeUdHSVhhQnliNHl1T0d3Q3VCVkdJcmZGZmJMVkdlZTNsU3U3emVwR29VMWdlWDFaS2NxeVB1TlErbHcwTW9nbFF2TkFvRjBRZG40QWtqRkdBZTVadXhlSjVJeUxhTVJGUmpjbmY3dE9MeUdiZnFmWUpDNjByWVViQzhURmJhOTl4WmZQakQ0cVR1a0RTTUVrXC9PWE9qYVpLK1U3bDRFbzE1VUdkQzRzSzRPRjRnXC94M2xKQVFudWtaYlUyYXZmTllQSHUyeTBTRFFHcUY1b3N2ZzYxNDVmQlhSSkJJZGNKT2dsSWRQbnl4N2NPRjM2SkZweTBCWjlXZmFcL0RmZFdaTGV2VENwbThHTWd0RzNJRE00UTVJT3VSY0JzbVdqMFR5M2U1OEtXTkNEVFpJcW5DS1FMS1NOVE43VkYrU0NZTTkzMElpUnRDSmlCU1NHbUg0VlFGMitlUW5BNko2T1ZUTktGWGFBdHNWOUNldmw2MXRpeEZTSGZqdjVJY0FuUkxMSmZ5SnN0clBVK2tXWDE1WUNxRGtLa2owUUhmYlg4SXE3V1wvVzlJWVlSc1A1RWVlaStpeDJNaWRnUDBqXC83ekhNYlwvNWphYjJaczlOZGgzSnZxdzdhc3BEa0hWXC9SRWdLK1JtSVJGRHNobVQ0UW1TRTk3dlJ6d3YweWxlSUxLc1dzSTh0b3ZpUmJBclc4bE1YanZLdW9UdFZjVHlFVXdXSjRyOTgxR1UzWjZ0RzNqMXFBWUh5cktIOUZsaWZaeFwvTDBmbUdKWUxEdnNqWjZ1S2J6WmlSNzZzYWQyakRFQjFGNFJZVXdJN0pPNW9mcm1VNHRhbW5cL3QyTU1HWldoUDNmYm0rb0hmSWxZOW9XclJJbjFGYzNCejA2VUduQlBuMnNKWVhWQ2JCcStGQjI1Z0RBeVYxMWd2MlwvcWZIZUppU0VVanpKQUJPNWppK3NaU3hWTDFEWHNkbTBWbzBTXC9CYytUZFQyZTV1ek1PZEc1d3BFc0pMYU05SEZNNVVOcm5KU2RSMlFreDdpbXIzdUVHenF0d3VraXJCT3JtYzd5QUV1VnhpdWN1aUZXd2xvTUJzNUl2UkVkWHZ1QU1lRjk5S2FvcE9ieWl6bDVJTGZhK2dUWEhZM1lXXC9lUGU5NHpObzJxcWFBZjdOR2Z2WTBjWFVhVG9zSVpiUzdxSStobmE0K2t0XC9NZ25kVjZQQjgxWnRaN1lEQ2M5dDM1UjNqTHM0c1k5SzJMS0JrYTkydU1La3JoWkVjSFFcL0ZXMmZkdlFcL1NnRnQ1Ymk0S3RpWHIrSkY5bmlhRVpqd3Y3S3VzbGlXc0h1VVQrMHJ0eVNQWmtHZVVibHVSSVwvVzZMUWtLOWhXRTBWXC9SUmdwT1pRanZxMmM0SzFGOG5STjNTQytQcytuRCtYdWNQXC8rYlRMSGh5ZDFkeHlnK3habkJtRFpWQ0s3ZWJUVldKelF1RjF5MXByZDdDdWxKd0lEYTF6Mk1oT0VmcXQ1S1hUUkJBYzBzcFFwUnhBMW9CTUxjRWhvamNKQzUrMG1ubmpEVDFqUWpNeVpiR3ZnNDcxanhFa25pYnY2U3NKK2xVKzd5dlFTbmtNcGdYMXpOQjhSWnVWY29wMEdpSUY4bDZuQlhVMTBmU2hEeDJHSUxIUXVjcnZhdkIyRTlQdm4rNHJOVFhqeGFqXC9rQ1wvclRBelduSjZoNVFWb1hHNVU4VExnQWlOcFJ5QnJuMHgzU21vV3Z0MWZaUm5TbWs1QnFvN0N3QVdGNktOTitJS25HckZvZHZtUVwva08zNXQzNnRjZkI4bmF0cW9Jd0VlQUpVR1l3b1M0WHFtYk9jZXY0MU5iUmZzcXU0ZlJIb0FCVWQwc2k3aFhobXZ0RjNLMlVmR2czMGxpelwvRmhNT0J2TUc0aE5jSDJYWmtwVnZSSUpKdWZqQWJ2MWEwdVl3YlZGbEhwdW5ISXdKeWJPZ1VFREpkb29MSjJMZ3NaazRPUmlrZWUyakNXb1lsUUN0RU5qUlN4czI3MFwvbzJNZWF1ZEpnS1hNVEFtTXkyTTViQ3BLNnF4OWdId3NxU1lzY2NtZVF1am1ZaDI0NEk5RXI4RUJ6RU41MnMyM2t1MnVGdWFZSGs0YUE1RE80MnZTZTNaUllwc1JJeHlWc3BXclJlKzZ5UkRHUHd3dVdIQjZRaXQ4YkhmOUFtcStKWFZSdHdWK1Z1S094eEt2Y0NkV01iZE5tRTl1Nm1UemtqejJKQ1p0cmRUbWYwcjBsdDdieHJ5R1wvMGdJcGFJbzRwV2VJUkFyazdJYmx1enkzRXZDWWV2MFpobXZZSjkyUFF6K01mMFpodXhINVlRM2s4d1JpM010Z1VKNzNmbDBjOVwvUGxqUFl5ekU3SzlQUkJmYzczYTdUTjRydHNWRktESmF5R2NJVXJlSE5iZXJ2em1KTlJOdGk4T2NYbERRd2hlY2EzQVA0TFJ1b0tGUUxVUkJQV0JOWUNOYTNVR2RiZ1FSUlA2VGVudG5hR2pERDFsdjhnZVM0S1p6cndidkhtYVdKNDVwd01QaWxUZUNqajVjdUhFelpKdGNiTEV6b1wvSHFEcU9MUnBpMTd2RytnUFY2OFp3TU8xb21Vc1krQkJwNm5pckVcL21LYW1cL0lQXC9yTUVLWXNVQk1iNXl4K0Z5aGNvSXJuQkhDMk5SdElRQkhKM041bmZSd2FPXC9la1I0R0pnNTZGclRcL093YU1BRWo0eGpmU05QSEhoN0w3VGdSMHh3ZUlzempKRVpsZW1DSnJ0d3oydWhzTVJcL05qK0tEcWVwa1Z2dVl1MDFlVXhadVRTNGpQcnpyKzNjRVVUcnlhUXUyZmVGd3hHaWRLXC9cL2FRN1wvSGY2QmxGZlwvYm1rUGZQOUM3blJpNTl3UVZ1Ym5DTlhDM1Y4eFNwRTR3bG5UMDJoTG9ISUJPZW1FR0V4RlZRUW5MUmwxTG5CdWJNanJCTmF5S1RPNnJLVWRiU1NXdVE2dmtnQVVkYUFqankwQjVDcE5qejQreTNKZHQ3aFduVTBUb25JVmZhbjBaeEpCTXd2UFI1V20xU3BsdDlBdUFXb0hncVoxZVFqcnRWWllxRTJCd2FOMGdWM2Y2Nm1BSkloOXhudk1TWm1BRU8zQlljNkRXbFFYN0lPRlIyTU1ZKzJnZGF5ZHpNeDcrV2duZHUzN3o2dXZSamw1K21INXJNbkxyVXR5RmIwNWZRangwOEo1UXNVbkhrU0hFXC9jdU11c3B4eFhYc2lIVGlhdmFPZzdjRXEzQ1pxUkw3YmFJdjhWR3U5bnYzVXA2Y2VpaGMxTldReWNVZ3hCSitrdndzeGxLclZUU3JBVWJQZmFiSHJHbXhSZUxDalZqVGw0bkZmTVluRFd3UnFwSlg4WHZDZ0R2TFNLTEdzYUxWcGU2dXpXaGxmQnVmb2ZXbGxObmNYRnRnaERRbUVuMXJcL2tuakRUKzVHdFhNSDZXSHNZZ0xQM2VQNUs3d3ZzWHRDSFBoSDJvTk5GbWx6NE9jOW1mVDVUeGFMWjBQWE9xNFdaOWtGdWJDOTcrWTRpdGFram03WStnRFkrVFpiS2V0ODNTZ292TnpvbVVTaGFQVUI1TGdyTXcwY2lYbmZmM1duWm9hSHlzcjJNeU1yMFhxM3RLKzhsaVlFM1VPZk0yXC9XNjRQOFwvMEFKMEFramNnRkhjVnF5dDYxRWRMZkJNVmY3QTNRK0tOWGZ6TUdOb1Q1aVRMeEZEVDNcL04xNlFCWTJLRStcLzhyblhJMnNXR2VHMkhwam42aEVqRHg3ZlAzTWlmU0YwUUU2QWloamk3ZzJTeGxGbHluZmg4QkN4ZTFSR0c5aHQwa3pvV0VrWVJCdE43UDhUZVU1azhsVjFZc3hlRjRSZFg5K21taVQ2NkdqbkpjVnhZWHZscEpIMXVocDRJdnVpdXB0T3d2dFpVSDFuU3paWU5JRWxaYmUrSmpHUlc1clpZWTBUcjdTT3AxaGd3TWg0OHVWd1wvVjZ5eXMyZHU0YmgxYmVLWm5mRldVUEdFYm0rME11SWdraGwrY0dCNGNaT2trMUhHOThhZ0NCWVJVTExsSkl0aGpKQUVGa0h0a1wvaVwvaFNaVDV0azlpNjJQcW1qcjR0dis5dWVoNkRIcDJwUGhnWHJQYmJnUEltaU1qMkhTVUlUNDZCK29YQmhrakkxWEFDU3RvQ2phRGxPaHJCUDNsKzNvSmMxRXAweWUyeHE2dkt1N2NrYzU2TUVRc1JNV1VnT2hCUHVZZnJzc3dXZDd0Y05BczJjOXZMTzdSRnRtY2o4SGxKc0N2bTBxKzBON3prb0ZITnFScUVhQkpLQ3hFdFRSZ1p2MFpsQVZFVkgzdE9ackRJOUdxWE9QcE1lNWhoM014TmxTTXFmRkRva05LNE91MEE5d3VUVlwvN2NTcXpOaTFSclwvMVl1S3B0Mkc0clFZb2NEQWFqTjJvOWhHNER5TFd5TDhGR2JKMFN0RExHMmFjOTUwcXhSOStudkdicEVWeDdLSGJ6T25lMmVQVjZJV21kVFZ0d3l5UXhJczRlTVJ6WTJrXC9wazlmS3d5QkNkYmFtTmNcL1JqQUdXOXpyYkpBbnp6dGlwS1BxZ2xKU2tINFVORWhIMXk0RFZSZTFWSHhZRGpuWUh6U0ZRb0tvNW5nZENnMk8zOUNvOVdESDl5NEYwMDVRQWxYT3VHUEVORDVaZlU2M254K29tWXlYS3JhSGVtNTJzak5PdTZLVjQxRTcyYWRXSDlZcklFOUVYa2xRXC95WU84QzU5cjMrUDR1STNSNWxhWHdub0JSSWVQODVXUmFHWjVQTHlpbGM5SERQUlZoclowSjc5cUo5TEhrWCt5V2pNdFcrdmJxVklZM1pQZE5kMkRwbHBWcWZWWWVPMngxQzFub21wWG5YWFc2R0xJQlpGOHdUZVB3Y0haYW5oRVYxR0taR05raCtrcU5FRGRlWGcreUE4ejhyT1ozdWV5VlVsVEFRRDE1c3FNUGd0cXhsYnFEOWxma2xQYXNhNU1aSm5DYUl0RnRtSVwvek9BbkVVcDZHMG5zcElXaUtTck9DMVBsK3BHM2hRbTBURHl6cllNQVwvcTZqaENPaDgyOW5GMTBLQkNHZjl0UlZGSDA0Mlwvc283T1RMclBzdkExKzVBRW1DSCs0K05nNnpJUjFOUEgzWEVqMlVEdWZzMmMwUzdib0JIOGRSVTlZb3FJNU91bEpZR3MzWFFcL3pRYjNwTzBpWEtFNjgrM1d2VU9GYk95Tmd6dGwyOG5JZGFBbThleVp0TzVpbk9HTk9JUlQ1eis4RUhjK2tuXC9jYU1zdzRDZzVwNk9wUkUwcVA2cTduN1lvY2c3emxzUXFSeTU2OFNTRkFWZ1AzWmdjXC9vWlwvTlwvNStyanNCMG5UZDVrK0ltMklwREt6VlRCVERqeUFMNFFQeTlaSm1Yd0lOdGN3T2R6T043eGdlUE5vK0s4c1U5YWhiVWlYbEZOZGFvZnM4dlRcLzNYVVlzc3FYeVhPd1ZudGMwWlJ0bERWdjRCcUp6Sjh2MFo0eDJOM0huSzNpUzMzVFJ2ZVY2N1ZhZW1yd0o0MXBZQlF2aVJqT1ozT3ZRZjNVTUpFSEZ0dXBkZTNYQmdleWVlY2I3OVpkOWJaYjM0XC9iQ3h6dGNqaVJyMEI0aW85SGk5VkFDSFNod1pcLzlNeE03dmRVVWYrTUptamUrUWluVkpGVHBPZURCYlJ4V09kRzRVSWVEMFBQSUJ6NzhET25HaFIzTW56ckhWSk5vaDR2bWd3T1JQcTRqdkFFRkFrV1Y4aTN3Qmo4aWdYeUJSNUZDWHl4UHVNVXZqSWw4MVpxVUdjQnZ4dVZLZGhTZEprckRKRU1yUmFWOGVSM294U2JGb2dvc2FcL1dlMkNBY3JoRHc9PSIsIm1hYyI6IjI2ZmRkNGE0MTEyYmIyNzQ0YjE3NDdlMDkxYjc0OTE3ZmZkZjk0NzMyMmM2MTQ4YzA0NTU4ZDVhMjNiMWY5ZGIifQ==', 1500536414),
('laravel_edit_index_page_2', 'eyJpdiI6IjBLZzNRcnJWM1k1WjBxU0xpZzVYelE9PSIsInZhbHVlIjoiczlidGNNeFwvV3VlQTIyU25OWkFZcGlSSlVpeVkrZHY0c2VZXC9MXC82ZE5pamsrMGtHcENSMnkxRjBmNUFuazJoUnBnSUZQVDVSYlVCMzc1dU13amRWWndQOTRxbVllNmFESGtMSmxLeUlzZ0p2b01cL1RvNzdFaFRnYzNBNEJJSmc1QUZ2aFdsSGlcL2RUTXhxaW91b0VhTWd4QlBDa2x3bGFOa0RRYWZJMUk5d1FZM2NYUTdrSTJIbndnYjNcL1ZldWNETzM1VTltOHV4ZFE2NkNZc1grYmM3RlFMWEZxekNXT3M4aDhYWk1laEJLUG9zWUJwVWlEcjhJY2FUcHN4UVc4dytseFo1ejZzb1R4Qm1UNEV3MTJIWHFPaVhBK0ZwU1wvYlwvOWRNUHMzZEVDYjVhYWYzcnpTNGxpNWtvYTZpV0QyMG81cDZOMkdQYzVJbnQyQjZieThhRTdsU004VkZWd1BhM1FaNU9SN1lqR1NhUFwvUW9rM0h3QlJiZTBuZEZhTzRoQjVSSWlxUlBObWFBeldibUd2ZGwyVlloVTc0NnBDbXBwVFBNalRydjhWM3Z0RWFPWmRXTW9PNmFISFp3QmR1WWxjUlhabG5VVVFITVJUc2ZvSEM0MVMwaG5JS01jYm0wdEM3T2RkZlVQYUZJdVA1RzdDbVRMakZZaTFcL1pWdUNrUndNZVVlcmo3MUdNa2NZYUtoQ1c3QlB1ZHRxOWVVTGtNRG1ZZDdrRTFOekRTKzV1Rit6WkFyRWV5d0RuU1lLVDJCcUtvU1RJbXduQmlEXC9VUDlpeE1IOEZpU3BWY2d3NHNoOHFBZzdwcHh6OVF4Rld1VGt6aG1namJqM1ZTVlMzY2ExbTlRSHBUa0c4cjhYcWplZUdab2ZYaDU4OGFcL0o1RHFRa0IzakhUbCtCK2ZKUXkrczVOYTlwdm02bzZ6Vk5YRUx0MTNaWmY1bUZ2SG9POWFXMXBTS3JTbWRRWk1HbmRVbUdFRmo4NUluZzNQeFY2Q0RvZlhMKzNneGgweklhWXlEWFwvWlZ2NlVmenBGTFRwbEN5SnZwd0VBOHVmT3JRU0lLS1AxdE5STm5JQ2llOXh5SlJcL2pqV0x0MVRXYzRmeFFHMGF2UTY0Y2haKzBrSmRyTDI3YnBRSVVhSXR3OGlcL0NxRCtRSEFCb0V2Z25LSHFDM09xdDlpOHlaNmhGQWJmbDZLaXd3UkVYYXhtZWhxSWtuVzZzTHBjUHF0SG1ycjB1Tlg1aDQrNXJXbVNxcTBzTGxqa200TXQ2bVwvc3hUVEZoZzBwUWNmNitUcGNcL3MyQlN1bVlVUklWR2dLSVVFVzBMN2Z4bFoxdlFIcGYxdkhjNGxKUnN0VHAzVFVhTnptTG5rZXVZcis4Uk5JVHpISDZMdjhyOTcwUTNsdnZTVFZKR01ibk5hdDJYcXpKb3RiYnJPb21lOXR1aHhsaENsREtOR2FaXC8yUHhLWTRydWx5eFJ0XC9CTGRwNlRCMVdtSlJTMnY5K04rUGN1bWpGU211WVBCM1M4QWJsMnhLSVZIYjNEcXg2YnJYU0ZtTUlpWkZUSDRWQklcLzhvVWNNc2dIeFkrck1cL2lObUFqXC9JXC9VcEh5RjIwUWpWb2ZhQ3N0SG1uNnVGQlN0USs4ajYzVHhGazdGT3BGUGZId0VHcnBsVjFMalNqNG9jVFlacWZSXC9zam5EWnlzMm1BVE02OWQrdUZJODJnekdLekE2ZENjcmg0Y2NleXlIdVYyZHZHdVBrXC9VY043cXJqZDk5ZjZJWk05QmZLTWdrcW84MTR0YjBONTFoRmprSTVlWjE1N0NpTGxJZHBZN25vWW9KTEJiNHU3K1drTVNpYjFQMGRoV285Y0pxdklVOFJQXC92VFl5ZGtxTmVmdWlCWDZlQXA3endWZnVXdUhaanp6cUthY0JGZXlRUUdSM2hRdnM0WlBoY0xVeGl1WURnWG9SUU5ITlI1c0lnbURpUlgrUXZZRG1QYnQ2THBQY3U1ZU9hdUtRVUZBcm1JK3RBSjIyRkVcL0hCSnBkNnFIYlVyM3I4NnQ1UnFcL0E4OFBaUm1Fc0UxNTRBRlgzeTVERzRteE5Tb0RYdUhYQkVicjI2VUtSREtcL21tS1wvOEdNVkYyUitGcDV2MU8rVXU5TW11UlFtUlJIV1QwQXNXUnRYUE9PZFppTUtsY3dXQ1hkUjRjeVwvS3lSQjQ3ZFQzUkIwYnU5RzBZVlhnbmhna0NaMTBrZnhXTlFOSzRsRTg1c3RWY2RcL3VNK29wdHdmT1dsTDVoajcwQ3lGeUlcL2tPQmtuTG1uK1RBNXd2WFpYU2xJYSsyVWlvbUZcL0xhUnVFN29rSzk2Y0VCd1BNSGorQ1hEWVkwOW5DNkViSVwvc3hVSzdIdGNOSTg5R0RlSm9JdHZoVlAydFhrclRaYkZ2T2VQU1JUd3Q2Vmp2Ykd1ejdFNHM1aWd2TVNtSXo2YjloNnBCVVM3MVkrVFQ5QTRDbXNpUzJXY1d3VUF4N1JVT21IcStVXC9RWFpjSVZjR0lFK2g2XC8wclNmTU1ac2FCOGpEQ1hiVDgzemVxaHR2b29MZmxaczVPY3NlVHNlaGh6MGFUdzB2WDlnZ2crWnNQclVlTjhGNCtseTM1WkxHS0JhRXdZV0pvM2RCU3lucWpzZmxkbHdCTzFFR3dIRVB3cktsNFR2UmRpNm9jNlZ3Z1B6V3ZQeEhoQ2JZOVpocjdYWjFXOHNBZXVDTllubG1kVGk1OGd1WXljeEtaa09QVm5YXC9mcUx0R1l4YkRmWDNUSWdqTTN4dmQ4XC9wVE51c2RLZ0pzMzlRWitBRjloT2pmTmQxOUN3NkY1THJMQ3gzWnh1WkFaUzZ3cXFwSDAzYzZKZWt3MXpxZE90SkljVDB1cU1kcDdrcDZyemxkazlCQVBJZ1dNWUJYZmhIeHJkVkFxeGhWXC9Ib2N3ZTQ0NzhKUDcyQ2JcLzV2cEZGYjNqK3dHMlE1enloSDk4RytnTG9SWFlcL1UzNDF0NjJNQWxKbGNrTm1jcXlScCt0RTN0WlYyVzVONnN1WHNIMDU2UW5QOXZJRGtJNU1tbUZSWk9xcUZZMDRvMDRYXC81K0VcL1M3cGplZ2czb0JFRjJzWkxBTUlUUFwvbFY2ZlRmQlZwNUtlUGMwV1pBZkdSRDNmUmVBM3JcL2w1a29nZFwvZVRBMkQ2ODFtamFNUWxVaDRWT1FmUTJlRlYyd3dUelJBdFkzYzIrU0I3UHhDT0ljbUVkaU1vWTFlWThsREkzVTB3Nm1EVXhyZ1wvTHR6dHZsNEVBPT0iLCJtYWMiOiJmM2EyMjg3NTQxMjdhMGM0MGFhZDRlZDY3Y2QzYWQxNWMzOTdkZjA1NDY2YmU3MTg3OTQyNGI1MzY1YWM1NDg4In0=', 1499414970),
('laravel_support_1', 'eyJpdiI6IkZYWnNmYkRkbHgzZWhHQ1lvaXRGTVE9PSIsInZhbHVlIjoiQ3FiaFwvQ3B4Tzl4ZG5SWW1PRW5QYUh5UGV6WVlOQmFockxEekJPTm10SzhNNFp0ckNvc3R0RzQyRGtFZ3FwdkRJTVoxd1k0OUF0cldac21SRkZtclwvRENOUVhnSFFJRzVhaW9DVDhYRnZ0NFR1dFg5c0c0WHc5cnM4b1FzS3NNeFVUSlBJYVd4Sis3TzhNS2NxV3loWnpsVGN4RnhWVGF1WXhWWm4zbVRSQ0dBdjRvYlcwK0RGU08yeDVzMjAwS3dJY0I3azJQOW1TNEdKam02N3pHXC9hQVFFRlVSUnNFNzZaQldoSjgxUGNJRndrR0hKRDRcLzV5RjVLRnRmUjMrMWV3amxQeDZSb3grYStKbGZYRWVya015bDZwbXhMMkZURGdVZzEyZWVZbDUyUzBCenR1RndySDBjeFlUWk5cL1RqczF2RWJpU3RvdmtudUdwUU5ZekxQNFdLZmZUb3NteHlIajd4V3UwUmJtTlF5elQ3eVhrVUxSSlVxR2tSWWRMTnRtU05CbkQwM3psQlVXcWpYUVJjS3hKdXNMMTBydGFCVGVSeXFPRjNSSUppXC8xc0hRT2F2bytjQ0xZTm5hdlVMWUlVb0ZIOUc2M2hBWnBzQVhKajZtbmM1cnhVYUNaZVNwQ3N2bmpZZzdEaTNwMUdzM0IwRHVEYThBbGd3SWdLUE9HbHprTW41cGhSVWlxeFRIXC9Ld1wvSEJOeGlqcEVyRzVuS1AxSG5qc1c2Y1VGN2dvWjl3XC90b1BkUUNxWkFEWEZKRDd3eVBPR29MU25KZUE3VTBzME9uMUFEeHZXYnNrS3h3RFMwRDZOWDZTT0FZY1hYWHJ5MXBoS0kxeTREaUY4dm9xVGQ0SjRmbXFFTCt6amxwcktJQktBSkNWOUg4S3doTFwvUGNGVmQ2eFowM2Fkb2VCcmhiYW50bnpvSkZOY1lLU3lcLzhzQnFEaHJZd3lFTmdJUmxzRnpIbndFQTRxZ09pSXBWYVJQMGlyWkpPUUtuSmJnKzR4NlFLM09qd1J2cG9hZit1QVBcL0RWc0RMd3dxeGNJREpLam4ranBBa1Y3TGMxZTFOaHRBTUliR3g1T3g4azlrTkxkK3FkRTJPSU1halNDSStjYVJOajVpQ29jWEp0ck9OUXRjMWlFSExaT1h2SzBPVjdhVE0rY1FVYUJtY2ZsQkk5Z1FDME5Gd2NLNFBjT0RTRyt2M25GcGZ3c1k0cnNxSzlcL1dlRUZMbTQySE54UGt5UDBcL0ViUlhKbzRzcjdPSmx0aEczWk1xMDNHQldhQ0w2MitmS3hTOFZcL0l2MjVYOUhxSWFmdUFYeGx1UEJQQnF5dXFlXC9KbkZzbGtxMHE5QWwrZ25kNHVwVUluek85WXhlTDJnRGhEb0pUK2RXbjB1WjlXY1Fwa2J0amVRYzZBYml3a1BHZ1Ura1BuYjNjQ05aUTh5UFwvY3VTUmJiMlJwRXVRaXMrTldHTFE0enRKREswalppTnY1WUZLRHNzcTBaZ2NuM3daT2Z3NFhGc3poR1RCbGJaXC9RNkxiSzdVb1FjVE5rMit2aWY4Z1BadVI3ZEpMbDhLa3BYVUVkbkFHdGdxNzFrUDRSY3BPT0FDcU8rZW1EaitUVERIemVhb2pTSUFLeUtTXC9TNzVmU2x2VXByOUNzRE4wRUZwMFwvcGNsckVLUTNWY3hCb0RwYW5XOU0wdlN6NkltdGJPVld1V1lqeHlhRnhHNXhER0lJOU5jcHVONXJxdWswRWxCWGk4Z0tGSGs3MG1GSmpZK0NJWkdrS3F3bXpnWHE2R3dVSFQxMmN2MzE1WFArN2hiTHU2dG1COFFcL3EyMkpCV1wvNUd6T1h4ejZBeGRiZFRBUmYxdWNjN3JldWt1T3I1SFl1K1wvMnNsWEREaUE2bXFGMVR5dDNcL3huZWRhelZocFRZenpqUE8wVHNGYVNocVk5NWh3OE5iSjZNZDg5WG9HeVlDTG1keGg5TVIrZ3NVZlJyUDlXYWtRd0EzQUc4NHdxd2kwUE9VOTBLQkZoUWZPODBsUFM2MlVudVhJMVJ1ZnhSUUZNUDI5XC85clFYSzdYUjByZXJGSTNyVzBSRFBReWh2WWNvYjhLejRuUklFbmRLNVN3VlZKVUdRbXVIMkxzUVZManp3RFdNSXl2SHYxN1M0YXhIVGp2STFBUzFTMnA2azRJMkZ5M0VidmJlSzlzbVFHMGtadTZWRERNNldVV3BkNFBibDB2Z3VST25qWkJaQXM3UEFTaHFjNHpWSWhqajZFenBrWHBsRGY0MDRXWjRcL0QwVGpxMEZVUDYxTkhoRVFyMGFIQ2NYSmo3VGtDVU04ZmNaM1JuVXBocGhjMTZCdFo4NThtNEZyMG9rQXNnQUxncnNGUnNDK1Y5MjZld21QSU5ka1RqdzFpQm5rdjBMSk5jOFo3dzBheUZyWHpBK1U3OENEYlFmS0tvSzF4dWpaYlZZZUw3WXU5eEdrNVVkb1dMdTFCd1U1SHBpTDRYRFwvUzdORWlVbnVoTGVJSW14cE02V0Nodzc4MXNRWmJ4c1gxWFhtRFI1R08xakd3SnFUNVFSbWNqbHZcL1hMRVZaTUNkS1hKVXBYaU9Ua0tTVExSeHBYQXFRV1RxaEw2NjNFVFMwTnVPYXN0bktuWkZxdFkzY1dHcXFZUzNlSWlLRytDVFFvQjQ0YmxCcUN0eHphYWpZeitlZ2g1QjAxdTVCK1FTMTVGUGVSWmRGNHVOQVBzdytTTXF4RzlmQ044Y3pHQkR2dTA4SUF0dHZ0VVQrOVh2V1ptczZvWE0wRU1rbzRFbnBHT3UxZGpzMHV1Mkg3Nzd4K3VzdmJoWHdOZVNmSlZ5THVzU0lRVXdsVnVyam5nYVpBMDhBcG9RQmNLd0NPVnhtV2s3QnQ0d00xXC9LNmcrcVdjbkE1bmhuejFSOUEyVm9JV29KXC9OclE1UlJ3a2E3KzRZZGxnMVJoZngxYUwrVHJNSUdLdm1hR2JwUit4OUJMYlhUMW4zWEphM0E2dlcyN1wvQmZEVmxDMllxZmRyazcxemxsSFlSUkliWWhjMU9GMVRnZGxyN0JLQjZ4VWVyVzhDTlkxNTNybiszZVZKRzIxOUhTaHp1Vmh0eVN5U055dGx1bkNKelhcL0RacjhJMERFSUFhNVFuN2NyeTBrZ2FtVTJQbmZwRTEyVmtwOU84VXJSdThHXC81VDRKdDhEVFdxSG9nU2Q3OEltaDJQQ1pDMFpVN0FMSzFWT0tHb1Q0dmFlSk50Qlg4bkRlenZVOEFZVGZYWktIY0FYN0oycXZuSWV4ZEJyS1l5V3lkNEZldW9iR2trdlg0UDBxOW54cVhOOUJcL0RicFlZelNCbkRiZTdZVmpuNXhWVCtuMWNhdUgxdmF2dWxyd2FncTk5SFRiUkpodmFJYzNjRGk4RVpQVmg4c05IOTJpZjg3c0ExNnFcLzhkQzdBMzc1eUh0dGdpMGIxeUc5RkxYbFB6S3hNTTk0SHlpR1hWbVVwQktzc1hlSkJNQmNmYWY2OW45c3pUVFZXQm44NzZjNU5nZ1NvY1h4c3B1UW5EbFB5YWZ6enRRV0RibXVqalhneHY5SjBiVDJlUmZvTTBlWjF3b0NTQWROVnFoMjg5MWxlTm9FMWdOdkNzQ0s0dWVpQ2U4OThXdGU0S2UxMldOSmlLczU3RDNCN0xlZGV4Y1N1a054dTNcL2lVcGg2QlhVYWQxcEF5ckJWYkpWYnVzS1VMXC8zWWgrdVwvMG1MOGNwaUdkb0pOSUl4ejZReFlqMmRrakF3WVViUUtscStMYlFYbmI1V1oxOXdBeTNZWXQ5Vk5FVzlwcExSM1wvUE9xdFVLR29YSDZ4cjBsQXpKK3hsbUJXdlwvTFdLVWd5VHhzT0RtR25FSEx3alY2bE45NjZvNEI5bU4zWDU1NFVhSVlJeFU2aTlGbVlVMGV5eW0rRnhMUTRvYk9KMlB1NnVDVnZQYVA1MERsdEFYaUNaSXlxc3o0OVwvRmswS3JiM1dPcXdoS2xrMWNcL3FkcW50ZWJsZnFcL2prMlN4XC8zXC9TRVVUUHdnTDlTMHU1WGVXdUZvQ3BHM29ya3N2MUFSOHREemJUNThUQkxueVgwUjM0VGFlZkVQdTZpekRGcXJmdmtPMFFZbU1iUzZFcnlUOHlqMVZqUE1oblkzMktCTzAyWjFaNmh6S1hsZFFoczUwSGlRbytQUjliY2FNbUh4Q2RPdXpHXC9yMmgxNU9jRTdJeFpvQkdEWHRRQUZjaDhnUHpFd2hqTWt0bTF3QStvODg0OGNUcHk4WGZidkxPTU0zSDE5eWVXQUo1V0c4TnlsMmFVMjhNQ3VBXC93Q2FRSGZxVUpGZ1p5NWdvN2RmZTdyWjVcL2o5R2tcL2tPb2MyV256emlud0NxaHAxUkpEYzdcL0p4emFqeWZLTmxNNkJrQ045azNpVmtyRmRKaXVrK2xpeXF3UkNVZ2pTSG9OTEJSNUNIY0xhXC9LMFM5aU9XcHZNejJuT2pETk9ORjA2aXY4R040Qlo4RUlKVE9lazVrcG1LcnlwUkJFaWUrVCtHNHhEc2tTUjlTRUFSWlwvRkFlTml1N3NWclA5ZjJUd29qd0FpVWk4OFpRc0tVVTBCd3lVak83TFNjNm5aYnRcL05NU1hFWFFIVktSdld2K3ZWVytza1JyNEpVMmFqTHgrZVRUYjlpVm9CUXZtNUVNN2Ixcm5senprY1pVWkRCSnltN0I2R25aMXVtK3dESzNXTyt2a29ybytFYlpvbjRJSlgxbFgyZFwvSFNcLzY3NndieExWN09UMFJ2OVNzeWoyaGFHR0VvaUtuNnJRdCtsVHJtZFhpcHFia1wvencyb2hydldhMElhT3kyeGxFeENBTDFVNzlcLzhIeWg2eEkyUmhwQzRHeldKY0E5THgrSXFxUklZQmhMQ3pRdjQ1UUpoeWNRV3hDOGRPb2JGZThZMlJ5RjdQUzR5NWJsSytYUjhkc1J2UFlkS1wvTWFFaDZzRncyc0h2cXU4M1JqUUQxVnpSbm1oM0l3aXMrcE5cL0tlUUFEd016RHp4VVBCK0FGRSIsIm1hYyI6ImMwMmY2MzQ5YmIyMmI5MWVmODY4ZWEzYzhhZTE5ZTdlYWM3ZjYyMWRiNTY3ODdhMDIxZTU0ZDUyZDBhNGMzODEifQ==', 1516478438),
('laravel_edit_index_page_1', 'eyJpdiI6InpLOVwvYlZ2eDVHcUdMejNuK2JmSEpBPT0iLCJ2YWx1ZSI6IjRuOUljRno2YUVZTll4NkdJSzRRbnhENDJBVnBLRTdQbUFBVVhDN1Y0Ym9mWUpkQVBZUzlzdW9kbkpuY1Z3eHYreUZZNUV1ZFBHVGFXZ1dOQTRHY0FXNWZUNjZxVks5b1dTd3phRnArMjBTb3paT1IrU3VvUXh4TEVvOVhqV0ZMR0J6VzZxSmpcL1hub2xvS3NNVk9ISjhOT0E2eE1ZQWxxSXNFNHZXUGQ3MHJnY213OHhOZ3hlY1RhK0w1VVpvVGliTElpN1hXdVJneTROK1hwWE1KdGdZbkpONklvaDV6c08xdVhiZjQ1cDI4ZnlFZktVTjNJQ0JtVVk5M2ZcL1dDeG5Zd1wvUk9lUU4wWUtackdCNVFpNFg1MHJTb0xMZ3VObXpcL1pLNzJFZkd3YjlYWFJJZm15NExiU3RHazY3WWZOZGx3UW13OEwxUnE3enM3RCtBNDZqdm9vbWF5WnBJU29vTVwvSTFrbUZPTit0REpJWU1EcFhBNE5wNEZIU1A3N3NSQStJbHFEMlJLdG5PbFdFM1I2OHpMMEFcL1E0clc3VHJnMWJQelBlS3QyZFhPcVptODNRbXVMTStkSnowWm9oaVo5QjBCWk9iaFVZekxzOE5WS21nQldPMVo0XC9OVE5zdXZwMG5JaHBTM2JEdlwvVGRtZ3JVNDJXem91K1hEM0JpeUVBSnNXT1M1bHZGcjhXVDhrTjlVcFRkbURvSG15Q3pDTGJUMThMUDhuMGFYQkJ1ZjV2UndxZnN2UVBjQmQrc3EwNTZNOXE1T3RJZThMM0dObWFPY285SW9uWGQxaWR0S1JMXC9rZE1LZE01N1JsNENZakUzZ1ZBM1RwTjJJekNDN0kyY3dTT0Zxa0hSd1wvaitYaVV6Q29nT0NcL0NialZrNlp2cm5TRjRjVW9aMFVHTkFZaVYycUxlWnAzN01TeEN5bW0yOXphZWRJaXpVdEplQitXUXVsdVNmK0F3YVJpd05LeUk0VTNDU1BkVDNSdmYxM1VnbGtSNnVQQ2tET0FBNjB1aXFTcFFKRnJvUGZmXC9lZWZDSnlhazFLemNsQTBZZ3YrVzhoS3dPTjlrS2dHT08rZHY0bmY4WlkzKzJObXZPTURoaTFEUUhqVk5SYkh4d09yRDdyQ0xzVzdEMjFwUEJcLytROXBuUWk3T1hkSGR2WEVhM0d3eVRlZW13XC9GMktcL25WV1hBSmJCQ1hvZXFhOGhCQ2F5d294aXErcEVQYXpsWVpRNlwvTTR6blAxcGFScHpNYnVLU0tzRE9VK2Q0cVJ1b1o2Nzk0XC9QcHhxT0N4NjRYYVwvMmZveWJYd2xcLzErU3hhRXo1ZVlOUm55REd3MGZ3TVlOVStySkloN2xvZkRRYnhDRVZTTWZtVVZUVlwveVFFYm9kcTBVcnBHNWNCck41UmdXVXVjWlRcL3c4TWNJXC96MEdKMUJ5RTRTaUFvNWJBUWZZOXNCeExrTGhVdmhmeHB0TVQ0dzlCemMrNkl3MFpGcnVmN3RNM0VnWTZpbncxcU1ZNjRmTHNqTWxiY1Z3aUM0Y29LUmNvSUZnck5ZYjIraUtITExrcEFnREszYnBXRTZ0djNDSVVydXRQXC83ZlBGQVhieXFFendvZzh2dVlPMFhMK0Z2MUx2S05MTWdcL2ZsTVFZUU9nR2RqeWxMUHU2a1cxWm42WG0xbjJwcEZ3Zm1yXC90MWhRNXd0SzJTZVwvTThJbDNRNDJQNlJ0MWx5c1VRTlRZbkgwXC9WWHQ4OWFXcGc0dDB6eEJGV1RQRDlwYURHVDJrYnBFMzA3aWNRMFIyeVpnZGVtVHgySlZrajMwTFMrTTkrMTcwbzZtWkowUkxKczQyQVppd0NjRThwNGxlNkVLWVwvXC9Xenk3XC9sUWd3WEw3TDlEZEUwRzBGZEhaYzB5RmpVVTZpNmNmMGhDeXpCa1dMYTlXbUVJOTRya1VCdm55RFNoRXVTMjlTOEw3S1FRMFwvWWVydW15QTRpRFwvbWdZR0VUbWxVSzZEWDhUd1NHXC92TUFCMXJhN0FXTjNDMVRnU3dFamd0UktMODNoYVM5QXhKd0dFRTU1cDUycHh6dDZnbG5ja2VjRnVhOEN3MWdqbFVxeTZUREpsV1JyQ0NGYU52WmJyYm5qMzRLVHkzbHhTR0Y2OG5FK2c0VVZXZWlMcjJjXC8rOG9xa0pnMVlnemNtMTBMQ2lISFowSHc4cWV2VDhMNnNVSjVDdStNaDBwbU5pVmFMcjN0UVhqNW1ISmJwXC9MSzNqTXNmK3NnN0VBWGV1V29cL3ptYUF0d0JGM0ZyXC83TDR5VE5hRnR1ZEVkNFdHOWxacDhXWjlYbk9sM2dsUEtWWXhoVzRNa3VYMTB1c2w4NU1GSlVwS1FCK0JSMk9LZnpBRDRGa0haUDJsT1lMYWJYdUVWbE41OCtNMmF3cTF4WlFoMEgzeVhXRGVSRTZncXRoTldEblU1RFhVZDRvOGhScWJLUEFoazllcWxYa3c5UyIsIm1hYyI6IjE4NzA3ZDk0ZjVmNDdlMzE5ZmEzNmZiZjUxZTE3OTYwYzlkYjdiMTAwYmQ3OGUzZGI1OWNmMjM2ZWQ2NmExYzEifQ==', 1516479437),
('laravel_trip_keywords_1', 'eyJpdiI6IlBvV2RKdG1KTVdxXC9mdk53QWhNT2ZRPT0iLCJ2YWx1ZSI6IkZzZ2tRWVpKRHVhUzRzdGh6RFwvUTdBPT0iLCJtYWMiOiIxY2JmZDQzYThkMDMwMjU3NzdkYjRlNDM2OGRiYzNkZDFmOWNhMDNiY2E4YWQ0NmFiYjc4NTJkZDkwZThlNDBiIn0=', 1516385432);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL,
  `big_img_id` int(11) NOT NULL,
  `small_img_id` int(11) NOT NULL,
  `cat_slider` text NOT NULL,
  `show_cat_map` tinyint(1) NOT NULL,
  `cat_map` text NOT NULL,
  `cat_type` varchar(200) NOT NULL COMMENT 'article||something else',
  `parent_id` int(11) NOT NULL,
  `cat_order` int(11) NOT NULL,
  `hide_cat` tinyint(1) NOT NULL,
  `show_in_menu` tinyint(1) NOT NULL,
  `cat_icon` varchar(200) NOT NULL,
  `cat_lat` varchar(200) NOT NULL,
  `cat_lng` varchar(200) NOT NULL,
  `show_in_map` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `big_img_id`, `small_img_id`, `cat_slider`, `show_cat_map`, `cat_map`, `cat_type`, `parent_id`, `cat_order`, `hide_cat`, `show_in_menu`, `cat_icon`, `cat_lat`, `cat_lng`, `show_in_map`, `created_at`, `updated_at`, `deleted_at`) VALUES
(71, 0, 0, '', 0, '', 'trip', 0, 0, 0, 0, '', '', '', 0, '2017-03-20 11:15:09', '2017-03-20 11:15:09', NULL),
(72, 1676, 1677, '[]', 1, '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', 'trip', 0, 0, 0, 1, 'fa-edit', '', '', 0, '2017-03-20 11:15:09', '2017-05-06 21:30:41', NULL),
(73, 1680, 1681, '[1813,1814,1815,1816]', 0, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-03-20 11:45:12', '2017-05-06 22:32:59', NULL),
(74, 0, 0, '[1625,1626]', 0, '', 'article', 0, 0, 0, 0, '', '', '', 0, '2017-03-21 14:39:24', '2017-03-21 14:40:35', NULL),
(75, 0, 0, '[1629,1630,1631]', 0, '', 'article', 74, 0, 0, 0, '', '', '', 0, '2017-03-21 14:41:27', '2017-03-21 14:41:27', NULL),
(76, 1678, 1679, 'null', 0, '', 'country', 0, 0, 0, 0, '', '', '', 0, '2017-03-22 11:29:42', '2017-03-22 15:36:24', NULL),
(77, 0, 0, 'null', 0, '', 'city', 76, 0, 0, 0, '', '29.9654476', '31.3548556', 0, '2017-03-22 11:31:05', '2017-03-22 11:31:05', NULL),
(78, 1685, 1686, '[1687,1688]', 0, '', 'trip', 0, 0, 0, 1, 'fa-globe', '', '', 0, '2017-03-22 15:39:52', '2017-03-22 15:42:55', NULL),
(79, 1689, 1690, '[1691]', 0, '', 'trip', 0, 0, 0, 1, 'fa-ship', '', '', 0, '2017-03-22 15:41:16', '2017-03-22 15:42:42', NULL),
(80, 1692, 1693, 'null', 0, '', 'city', 76, 0, 0, 0, '', '29.7181745', '31.4514885', 1, '2017-03-22 15:41:44', '2017-03-22 16:29:59', NULL),
(81, 1694, 1695, '[1696,1697]', 0, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-03-22 15:49:24', '2017-05-06 22:33:06', NULL),
(82, 1698, 1699, '[1700]', 0, '', 'trip', 78, 0, 0, 1, '', '', '', 0, '2017-03-22 15:51:12', '2017-03-22 15:57:40', NULL),
(83, 1701, 1702, '[1703]', 0, '', 'trip', 78, 0, 0, 1, '', '', '', 0, '2017-03-22 15:52:13', '2017-03-22 15:57:43', NULL),
(84, 1704, 1705, '[1706]', 0, '', 'trip', 79, 0, 0, 1, '', '', '', 0, '2017-03-22 15:56:38', '2017-04-20 13:36:15', NULL),
(85, 1712, 1713, '[1714]', 0, '', 'trip', 79, 0, 0, 1, '', '', '', 0, '2017-03-22 15:58:15', '2017-03-22 15:58:30', NULL),
(86, 1724, 1725, 'null', 0, '', 'city', 76, 0, 0, 0, '', '29.5181745', '31.2514885', 1, '2017-03-22 16:05:38', '2017-03-22 16:30:01', NULL),
(87, 1743, 1744, 'null', 0, '', 'country', 0, 0, 0, 0, '', '', '', 0, '2017-03-22 16:14:30', '2017-03-22 16:14:30', NULL),
(88, 1749, 1750, 'null', 0, '', 'city', 87, 0, 0, 0, '', '', '', 1, '2017-03-22 16:16:32', '2017-03-22 16:30:06', NULL),
(89, 1769, 1770, '[]', 0, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-04-03 10:10:47', '2017-05-06 22:33:14', '2017-05-06 22:33:14'),
(90, 1787, 1788, 'null', 0, '', 'country', 0, 0, 0, 0, '', '', '', 0, '2017-04-13 14:43:54', '2017-04-13 14:43:54', NULL),
(91, 1789, 1790, '[1791]', 1, '', 'trip', 0, 0, 0, 1, '', '', '', 0, '2017-04-13 15:12:42', '2017-04-13 15:16:05', '2017-04-13 15:16:05'),
(92, 1793, 1794, '[1795]', 0, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-04-13 15:36:40', '2017-04-20 10:29:57', '2017-04-20 10:29:57'),
(93, 1799, 1800, '[]', 0, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-04-15 12:38:13', '2017-04-20 10:30:06', '2017-04-20 10:30:06'),
(94, 1801, 1802, '[1803,1804]', 1, '', 'trip', 72, 0, 0, 0, '', '', '', 0, '2017-04-17 09:42:40', '2017-05-06 22:33:21', '2017-05-06 22:33:21'),
(95, 1808, 1809, 'null', 0, '', 'city', 76, 0, 0, 0, '', '', '', 1, '2017-04-17 10:43:58', '2017-04-18 11:57:24', NULL),
(96, 1830, 1831, '[1832]', 1, '', 'trip', 72, 0, 0, 1, '', '', '', 0, '2017-05-06 22:40:00', '2017-05-07 13:26:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_translate`
--

CREATE TABLE IF NOT EXISTS `category_translate` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(300) NOT NULL,
  `cat_slug` varchar(300) NOT NULL,
  `cat_short_desc` varchar(300) NOT NULL,
  `cat_body` text NOT NULL,
  `cat_meta_title` text NOT NULL,
  `cat_meta_desc` text NOT NULL,
  `cat_meta_keywords` text NOT NULL,
  `lang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_translate`
--

INSERT INTO `category_translate` (`id`, `cat_id`, `cat_name`, `cat_slug`, `cat_short_desc`, `cat_body`, `cat_meta_title`, `cat_meta_desc`, `cat_meta_keywords`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(142, 72, 'Travel Packages', 'Travel-Packages', 'Travel Packages\r\n', '', 'Travel Packages', 'Travel Packages\r\n', 'Travel Packages\r\n', 1, '2017-03-20 11:15:09', '2017-04-15 10:51:48', NULL),
(147, 73, 'Egypt Nile Cruise Travel', 'Egypt-Nile-Cruise-Travel', 'Egypt Nile Cruise Travel', '', '', '', '', 1, '2017-03-20 11:45:12', '2017-04-20 14:52:04', NULL),
(150, 74, 'Parent Article', 'Parent-Article', 'Parent Article', '<p>Parent Article</p>\r\n', '', '', '', 1, '2017-03-21 14:39:24', '2017-03-21 14:39:24', NULL),
(153, 75, 'Child Parent', 'Child-Parent', 'Child Parent', '<p>Child Parent</p>\r\n', '', '', '', 1, '2017-03-21 14:41:27', '2017-03-21 14:41:27', NULL),
(156, 76, 'Egypt', 'Egypt', 'Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle', '<p>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle</p>\r\n\r\n<p>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle</p>\r\n', '', '', '', 1, '2017-03-22 11:29:42', '2017-03-22 15:22:57', NULL),
(158, 78, 'Day Tours', 'Day-Tours', 'Day Tours Day Tours Day Tours Day Tours', '<p>Day Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day ToursDay Tours Day Tours Day Tours Day Tours</p>\r\n', '', '', '', 1, '2017-03-22 15:39:52', '2017-03-22 15:39:52', NULL),
(159, 79, 'Nile Cruises', 'Nile-Cruises', 'Nile CruisesNile CruisesNile CruisesNile CruisesNile Cruises', '<p>Nile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile CruisesNile Cruises</p>\r\n', '', '', '', 1, '2017-03-22 15:41:16', '2017-03-22 15:41:16', NULL),
(160, 80, 'Cairo', 'Cairo', 'Cairo', '<p><span style="color:#FF0000"><strong>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle</strong></span></p>\r\n\r\n<p>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle</p>\r\n\r\n<p>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle</p>\r\n', '', '', '', 1, '2017-03-22 15:41:44', '2017-04-13 13:03:00', NULL),
(161, 81, 'Egypt Luxury Tours', 'Egypt-Luxury-Tours', 'Egypt Luxury ToursEgypt Luxury Tours', '<p>Egypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury ToursEgypt Luxury Tours</p>\r\n', '', '', '', 1, '2017-03-22 15:49:24', '2017-03-22 15:49:24', NULL),
(162, 82, 'Aswan Day Tours', 'Aswan-Day-Tours', 'Aswan Day ToursAswan Day ToursAswan Day Tours', '<p>Aswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day ToursAswan Day Tours</p>\r\n', '', '', '', 1, '2017-03-22 15:51:12', '2017-03-22 15:51:12', NULL),
(163, 83, 'Luxur Day Tours', 'Luxur-Day-Tours', 'Luxur Day ToursLuxur Day Tours', '<p>Luxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day ToursLuxur Day Tours</p>\r\n', '', '', '', 1, '2017-03-22 15:52:13', '2017-03-22 15:52:13', NULL),
(164, 84, 'Lake Nasser Criuses', 'Lake-Nasser-Criuses', 'Lake Nasser CriusesLake Nasser Criuses', '<p>Lake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser CriusesLake Nasser Criuses</p>\r\n', '', '', '', 1, '2017-03-22 15:56:38', '2017-03-22 15:56:38', NULL),
(165, 85, 'Nile Cruise Luxur Aswan', 'Nile-Cruise-Luxur-Aswan', 'Nile Cruise Luxur AswanNile Cruise Luxur Aswan', '<p>Nile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur AswanNile Cruise Luxur Aswan</p>\r\n', '', '', '', 1, '2017-03-22 15:58:15', '2017-03-22 15:58:15', NULL),
(166, 86, 'Luxor', 'Luxor', 'Luxor', '<p><strong>Luxor</strong></p>\r\n', '', '', '', 1, '2017-03-22 16:05:38', '2017-03-22 16:05:38', NULL),
(167, 87, 'France', 'France', 'France', '<p>France</p>\r\n', '', '', '', 1, '2017-03-22 16:14:30', '2017-03-22 16:14:30', NULL),
(168, 88, 'Paris', 'Paris', 'Paris', '<p>Paris</p>\r\n', 'Paris', 'Paris', 'Paris', 1, '2017-03-22 16:16:32', '2017-03-22 16:21:04', NULL),
(169, 89, 'Egypt Classical Tours', 'Egypt-Classical-Tours', 'Egypt Classical Tours Egypt Classical Tours Egypt Classical Tours Egypt Classical Tours', '<p>Egypt Classical Tours&nbsp;Egypt Classical Tours&nbsp;Egypt Classical Tours</p>\r\n', 'Egypt Classical Tours', 'Egypt Classical Tours', 'Egypt Classical Tours', 1, '2017-04-03 10:10:47', '2017-04-03 10:10:47', NULL),
(170, 90, 'Spain', 'Spain', 'Spain SpainSpainSpainSpainSpainSpainSpainSpainSpain', '<p>Spain SpainSpainSpainSpainSpainSpainSpainSpainSpain</p>\r\n', '', '', '', 1, '2017-04-13 14:43:54', '2017-04-13 14:43:54', NULL),
(171, 91, 'test', 'test', 'test', '<p>test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;</p>\r\n', 'test ', 'test ', 'test ', 1, '2017-04-13 15:12:42', '2017-04-13 15:12:42', NULL),
(172, 92, 'karim test', 'Karim-Test', ' Test Test Test Test Test Test ', '<h5>&nbsp;</h5>\r\n\r\n<p><strong><u>Included:</u></strong></p>\r\n\r\n<ul>\r\n	<li><strong>Meet and greet service by our representatives at airports</strong></li>\r\n	<li><strong>Assistance of our guest relations during your stay</strong></li>\r\n	<li><strong>All transfers to/from airport &amp; hotel by a private air-conditioned vehicle.</strong></li>\r\n	<li><strong>Accommodation for 3 nights in Cairo including daily breakfast.</strong></li>\r\n	<li><strong>Accommodation for 6 nights onboard Nile Cruise on full board basis</strong></li>\r\n	<li><strong>Domestic flight Cairo/Luxor &ndash; Luxor/Cairo</strong></li>\r\n	<li><strong>All sightseeing tours in Cairo are private guided tours.</strong></li>\r\n	<li><strong>All sightseeing tours on the cruise sharing cruise group</strong></li>\r\n	<li><strong>Entrance fees to all sites as indicated on the itinerary.</strong></li>\r\n	<li><strong>English-speaking tour guide during your tours</strong></li>\r\n	<li><strong>Meals at hotel, cruise and during tours as mentioned in the itinerary</strong></li>\r\n	<li><strong>Bottled water during your tour.</strong></li>\r\n	<li><strong>All service charges and taxes</strong></li>\r\n</ul>\r\n', 'karim', 'karim', 'karim', 1, '2017-04-13 15:36:40', '2017-04-13 15:36:40', NULL),
(173, 93, 'Madrid', 'Madrid', 'Madrid', '<p>MadridMadridMadridMadridMadridMadridMadridMadrid</p>\r\n', '', '', '', 1, '2017-04-15 12:38:13', '2017-04-15 12:38:13', NULL),
(174, 94, 'Paris – Brugge–Amsterdam', 'Paris-–-Brugge–Amsterdam', 'Paris – Brugge–Amsterdam', '<h1>Paris &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;AmsterdamParis &ndash; Brugge&ndash;Amsterdam</h1>\r\n', '', '', '', 1, '2017-04-17 09:42:40', '2017-04-17 09:42:40', NULL),
(175, 95, 'Barcelona', 'Barcelona', 'Barcelona, the cosmopolitan capital of Spain’s Catalonia region', '<p><span style="color:#B22222"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif"><strong>Barcelona</strong></span></span></span>, the cosmopolitan capital of Spain&rsquo;s Catalonia region, is known for its art and architecture. The fantastical Sagrada Fam&iacute;lia church and other modernist landmarks designed by Antoni Gaud&iacute; dot the city. Museu Picasso and Fundaci&oacute; Joan Mir&oacute; feature modern art by their namesakes. City history museum MUHBA, includes several Roman archaeological sites.</p>\r\n', '', '', '', 1, '2017-04-17 10:43:58', '2017-04-17 10:43:58', NULL),
(176, 72, 'Paquetes de viaje', 'Paquetes-de-viaje', 'Paquetes de viaje\r\n', '', 'Paquetes de viaje', 'Paquetes de viaje\r\n', 'Paquetes de viaje\r\n', 3, '2017-05-06 17:05:56', '2017-05-06 21:30:27', NULL),
(177, 96, 'karim', 'karim', 'karim', '<p>karim</p>\r\n', 'karim', 'karim', 'karim', 1, '2017-05-06 22:40:00', '2017-05-06 22:40:00', NULL),
(178, 96, 'karim', 'mahdy', 'karim', '<p>karim</p>\r\n', 'karim', 'karim', 'karim', 3, '2017-05-06 22:43:50', '2017-05-06 22:43:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency_rates`
--

CREATE TABLE IF NOT EXISTS `currency_rates` (
  `id` int(11) NOT NULL,
  `cur_img` int(11) NOT NULL,
  `cur_to` varchar(10) NOT NULL,
  `cur_rate` decimal(10,7) NOT NULL,
  `last_date` datetime NOT NULL,
  `show_in_homepage` tinyint(1) NOT NULL,
  `cur_sign` varchar(10) NOT NULL,
  `show_in_menu` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency_rates`
--

INSERT INTO `currency_rates` (`id`, `cur_img`, `cur_to`, `cur_rate`, `last_date`, `show_in_homepage`, `cur_sign`, `show_in_menu`) VALUES
(1, 1661, 'EGP', '17.6100010', '2017-11-06 21:30:33', 1, 'LE', 1),
(2, 1662, 'AED', '3.6724970', '2017-11-06 21:30:33', 1, '$', 1),
(3, 1663, 'USD', '1.0000000', '2017-11-06 21:30:33', 1, '$', 1),
(4, 1664, 'EUR', '0.8608020', '2017-11-06 21:30:33', 1, '$', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL,
  `sender_email` varchar(300) NOT NULL,
  `email_subject` varchar(300) NOT NULL,
  `email_body` text NOT NULL,
  `run_send` tinyint(1) NOT NULL,
  `offset` int(11) NOT NULL,
  `limit` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_settings`
--

INSERT INTO `email_settings` (`id`, `sender_email`, `email_subject`, `email_body`, `run_send`, `offset`, `limit`, `created_at`, `updated_at`) VALUES
(1, 'info@dmt.com', 'New DMT Cards', '<p>New DMT CardsNew DMT CardsNew DMT CardsNew DMT CardsNew DMT Cards</p>\r\n', 0, 0, 1, '2016-08-17 22:00:00', '2017-04-03 09:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `generate_site_content_methods`
--

CREATE TABLE IF NOT EXISTS `generate_site_content_methods` (
  `id` int(11) NOT NULL,
  `method_name` varchar(200) NOT NULL,
  `method_title` varchar(200) NOT NULL,
  `method_requirments` text NOT NULL,
  `method_img_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generate_site_content_methods`
--

INSERT INTO `generate_site_content_methods` (`id`, `method_name`, `method_title`, `method_requirments`, `method_img_id`) VALUES
(7, 'edit_index_page', 'الصفحة الرئيسية والكلمات الثابته', '{"input_fields":{"fields":["index_meta_title","index_meta_desc","index_meta_keywords","website_name","website_info","menu_homepage","menu_contact","fb_link","gplus_link","instagram_link","youtube_link","twitter_link"],"customize":{"label_name":{"index_meta_title":"\\u0639\\u0646\\u0648\\u0627\\u0646 \\u0627\\u0644 meta ","index_meta_desc":" \\u062a\\u0641\\u0627\\u0635\\u064a\\u0644 \\u0627\\u0644 meta ","index_meta_keywords":" \\u0643\\u0644\\u0645\\u0627\\u062a \\u0627\\u0644 meta ","website_name":" \\u0627\\u0633\\u0645 \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 ","website_info":" \\u0646\\u0628\\u0630\\u0629 \\u0645\\u062e\\u062a\\u0635\\u0631\\u0629 \\u0639\\u0646 \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 ","menu_homepage":" \\u0643\\u0644\\u0645\\u0629 \\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629 ","menu_contact":" \\u0643\\u0644\\u0645\\u0629 \\u0635\\u0641\\u062d\\u0647 \\u0627\\u0644\\u062f\\u0639\\u0645 ","fb_link":" \\u0644\\u0644\\u064a\\u0646\\u0643 \\u0627\\u0644\\u0641\\u064a\\u0633\\u0628\\u0648\\u0643 \\u0627\\u0646 \\u0648\\u062c\\u062f ","gplus_link":" \\u0644\\u0644\\u064a\\u0646\\u0643 \\u062c\\u0648\\u062c\\u0644 \\u0628\\u0644\\u0633 \\u0627\\u0646 \\u0648\\u062c\\u062f ","instagram_link":" \\u0644\\u0644\\u064a\\u0646\\u0643 \\u0627\\u0646\\u0633\\u062a\\u063a\\u0631\\u0627\\u0645 \\u0627\\u0646 \\u0648\\u062c\\u062f ","youtube_link":" \\u0644\\u0644\\u064a\\u0646\\u0643 \\u0627\\u0644\\u064a\\u0648\\u062a\\u064a\\u0648\\u0628 \\u0627\\u0646 \\u0648\\u062c\\u062f","twitter_link":" \\u0644\\u0644\\u064a\\u0646\\u0643 \\u062a\\u0648\\u064a\\u062a\\u0631 \\u0627\\u0646 \\u0648\\u062c\\u062f  "}}},"imgs_fields":{"fields":["logo","icon"],"customize":{"need_alt_title":{"logo":"yes","icon":"no"}}}}', 114),
(16, 'support', 'صفحه الدعم الفني', '{"input_fields":{"fields":["meta_title","meta_desc","meta_keywords","header","form_header","form_name","form_email","form_msg","form_btn","form_success_msg","form_title","form_phone","map","sidebar_header1","sidebar_header2"],"customize":{}},"arr_fields":{"fields":{"contact_data":["contact_data_label","contact_data_value","contact_data_url"],"sidebar_section2_data":["sidebar_section2_data_label","sidebar_section2_data_value","sidebar_section2_data_url"]},"customize":{"contact_data":{"field_type":{"contact_data_label":"textarea","contact_data_value":"textarea","contact_data_url":"textarea"}},"sidebar_section2_data":{"field_type":{"sidebar_section2_data_label":"textarea","sidebar_section2_data_value":"textarea","sidebar_section2_data_url":"textarea"}}}}}', 0),
(17, 'email_page', 'صفحة الايميل', '{"input_fields":{"fields":["copyright"],"customize":{"label_name":{"copyright":"copyright"},"required":{"copyright":""},"type":{"copyright":"textarea"},"class":{"copyright":""}}},"imgs_fields":{"fields":["logo_img"],"customize":{"required":{"logo_img":""},"need_alt_title":{"logo_img":""},"width":{"logo_img":""},"height":{"logo_img":""}}},"arr_fields":{"fields":{"social_imgs":["social_imgs"],"social_links":["social_links"]},"customize":{"social_imgs":{"label_name":{"social_imgs":"social_imgs"},"field_type":{"social_imgs":""},"field_class":{"social_imgs":"form-control"},"add_tiny_mce":{"social_imgs":""}},"social_links":{"label_name":{"social_links":"social_links"},"field_type":{"social_links":""},"field_class":{"social_links":"form-control"},"add_tiny_mce":{"social_links":""}}}}}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE IF NOT EXISTS `langs` (
  `lang_id` int(11) NOT NULL,
  `lang_title` varchar(200) NOT NULL,
  `lang_text` varchar(200) NOT NULL,
  `lang_img_id` int(11) NOT NULL,
  `lang_direction` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`lang_id`, `lang_title`, `lang_text`, `lang_img_id`, `lang_direction`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ar', 'اللغه العربية', 1665, 'rtl', '2016-08-16 22:00:00', '2017-11-05 19:39:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `m_id` int(11) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `not_id` int(11) NOT NULL,
  `not_title` text NOT NULL,
  `not_type` varchar(200) NOT NULL,
  `not_to_userid` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`not_id`, `not_title`, `not_type`, `not_to_userid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'abanoub metyas make a contact request...', '', 0, '2017-03-21 13:12:47', '2017-03-21 13:12:47', NULL),
(2, 'abanoub metyas make a contact request...', '', 0, '2017-03-21 13:18:17', '2017-03-21 13:18:17', NULL),
(3, 'abanoub metyas make a contact request...', '', 0, '2017-03-21 13:21:43', '2017-03-21 13:21:43', NULL),
(4, 'abanoub metyas make a contact request...', '', 0, '2017-03-21 13:22:32', '2017-03-21 13:22:32', NULL),
(5, 'Hossam testttttttttttttttttttttttttt make a contact request...', '', 0, '2017-04-03 09:36:45', '2017-04-03 09:36:45', NULL),
(6, 'asdaa make a contact request...', '', 0, '2017-05-09 08:22:55', '2017-05-09 08:22:55', NULL),
(7, 'aaasdsad make a contact request...', '', 0, '2017-11-06 19:52:21', '2017-11-06 19:52:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `page_price` decimal(10,2) NOT NULL,
  `page_map` text NOT NULL,
  `trip_price_table` text NOT NULL,
  `small_img_id` int(11) NOT NULL,
  `big_img_id` int(11) NOT NULL,
  `page_slider` text NOT NULL,
  `page_type` varchar(200) NOT NULL,
  `related_pages` text NOT NULL,
  `show_in_homepage` tinyint(1) NOT NULL,
  `show_in_menu` tinyint(1) NOT NULL,
  `hide_page` tinyint(1) NOT NULL,
  `page_views` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `cat_id`, `page_price`, `page_map`, `trip_price_table`, `small_img_id`, `big_img_id`, `page_slider`, `page_type`, `related_pages`, `show_in_homepage`, `show_in_menu`, `hide_page`, `page_views`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, '0.00', '', '', 0, 0, 'null', 'default', '', 0, 0, 0, 25, '2017-01-30 07:39:21', '2017-03-22 15:36:49', '2017-03-22 15:36:49'),
(2, NULL, '0.00', '', '', 0, 0, '[5,6,7]', 'photo_gallery', '', 0, 0, 0, 1, '2017-01-30 07:49:39', '2017-02-22 09:19:40', NULL),
(3, NULL, '0.00', '', '', 0, 0, 'null', 'video', '', 0, 0, 0, 6, '2017-01-30 08:34:20', '2017-03-11 02:23:44', NULL),
(4, NULL, '0.00', '', '', 0, 0, 'null', 'article', '', 0, 0, 0, 4, '2017-02-08 15:12:58', '2017-05-06 20:34:47', '2017-05-06 20:34:47'),
(5, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 22, '2017-02-08 15:14:12', '2017-03-10 23:46:53', NULL),
(6, NULL, '0.00', '', '', 0, 0, '[37,38,39]', 'photo_gallery', '', 0, 0, 0, 5, '2017-02-08 15:27:24', '2017-02-22 09:18:38', NULL),
(7, NULL, '0.00', '', '', 0, 0, 'null', 'video', '', 0, 0, 0, 6, '2017-02-08 15:31:18', '2017-02-16 12:25:46', NULL),
(8, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 3, '2017-02-14 15:09:14', '2017-03-03 10:55:26', NULL),
(9, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 4, '2017-02-14 15:10:20', '2017-03-02 20:44:45', NULL),
(10, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 3, '2017-02-14 15:10:59', '2017-03-04 22:36:50', NULL),
(11, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 5, '2017-02-14 15:12:27', '2017-03-10 02:50:30', NULL),
(12, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 7, '2017-02-14 15:12:33', '2017-03-06 06:02:21', NULL),
(13, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 17, '2017-02-15 05:49:35', '2017-03-11 02:16:21', NULL),
(14, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 7, '2017-02-15 09:02:20', '2017-03-03 23:14:00', NULL),
(15, NULL, '0.00', '', '', 0, 0, 'null', 'video', '', 0, 0, 0, 3, '2017-02-15 09:06:09', '2017-02-23 01:33:17', NULL),
(16, NULL, '0.00', '', '', 0, 0, 'null', 'video', '', 0, 0, 0, 6, '2017-02-15 09:08:28', '2017-03-10 02:51:11', NULL),
(17, NULL, '0.00', '', '', 0, 0, '[118,119]', 'photo_gallery', '', 0, 0, 0, 3, '2017-02-15 09:10:30', '2017-03-07 13:08:48', NULL),
(18, NULL, '0.00', '', '', 0, 0, '[121,122]', 'photo_gallery', '', 0, 0, 0, 0, '2017-02-15 09:13:20', '2017-02-15 09:13:20', NULL),
(19, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 9, '2017-02-15 09:19:32', '2017-03-05 19:23:10', NULL),
(20, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 20, '2017-02-15 10:43:44', '2017-03-07 14:13:05', NULL),
(21, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 0, '2017-02-27 18:37:18', '2017-02-27 18:37:41', '2017-02-27 18:37:41'),
(22, NULL, '0.00', '', '', 0, 0, 'null', 'news', '', 0, 0, 0, 12, '2017-03-02 09:55:16', '2017-03-10 07:50:08', NULL),
(23, NULL, '0.00', '', '', 0, 0, 'null', 'default', '', 0, 0, 0, 0, '2017-03-20 12:34:56', '2017-04-23 18:49:35', '2017-04-23 18:49:35'),
(24, 94, '1000.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '{"type":"type1","table":{"single":["1","2","3"],"double":["4","5","6"],"triple":["7","8","9"]}}', 1716, 1715, '[1797,1798]', 'trip', '', 1, 1, 0, 0, '2017-03-20 13:10:10', '2017-04-20 10:55:58', '2017-04-20 10:55:58'),
(25, 73, '500.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '{"type":"type2","table":{"tbl_price":["1","2","3","4"]}}', 1718, 1717, '[1810,1811,1812]', 'trip', '', 1, 1, 0, 0, '2017-03-21 11:13:51', '2017-04-20 13:19:36', '2017-04-20 13:19:36'),
(26, 75, '0.00', '', '', 0, 0, '[1634,1635,1636]', 'article', '', 0, 0, 0, 0, '2017-03-21 14:46:26', '2017-03-21 14:46:26', NULL),
(27, 75, '0.00', '', '', 0, 0, '[1639,1640]', 'article', '["24","25"]', 0, 0, 0, 0, '2017-03-22 10:33:35', '2017-03-22 10:39:48', NULL),
(28, 77, '0.00', '', '', 0, 0, '[1647,1648,1649]', 'article', '["24","25"]', 0, 0, 0, 0, '2017-03-22 11:48:19', '2017-03-22 11:48:19', NULL),
(29, 80, '0.00', '', '', 1708, 1707, '[1709,1710,1711]', 'article', '["24","25"]', 0, 0, 0, 0, '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(30, 80, '0.00', '', '', 1721, 1720, '[1722,1723]', 'article', '["24","25"]', 0, 0, 0, 0, '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(31, 73, '600.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '{"type":"type3","table":{"table_price_3_header_1":"1","table_price_3_prices_1":["2","3","4","5"],"table_price_3_header_2":"6","table_price_3_prices_2":["7","8","9","10"]}}', 1727, 1726, '[1817]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:05:58', '2017-05-06 20:33:47', '2017-05-06 20:33:47'),
(32, 81, '700.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1730, 1729, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:08:15', '2017-03-22 16:14:29', NULL),
(33, 86, '0.00', '', '', 1732, 1731, '[1733,1734]', 'article', '["24","25","31"]', 0, 0, 0, 0, '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(34, 82, '1000.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1736, 1735, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:09:49', '2017-03-22 16:11:26', NULL),
(35, 82, '700.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '{"type":"type1","table":{"single":["0","0","0"],"double":["0","0","0"],"triple":["0","0","0"]}}', 1738, 1737, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:10:52', '2017-04-23 11:31:46', NULL),
(36, 83, '850.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1740, 1739, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:12:40', '2017-03-22 16:13:51', NULL),
(37, 83, '1230.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1742, 1741, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:13:34', '2017-03-22 16:13:53', NULL),
(38, 84, '560.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1746, 1745, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:15:32', '2017-03-22 16:18:30', NULL),
(39, 84, '570.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1748, 1747, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:16:22', '2017-03-22 16:18:32', NULL),
(40, 85, '860.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1752, 1751, '[]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:17:22', '2017-03-22 16:18:44', NULL),
(41, 85, '650.00', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.0540557524178!2d31.225419315115627!3d30.06398498187516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840e582b61f9d%3A0xad7472def3c5066!2sGoogle!5e0!3m2!1sen!2seg!4v1490015159533" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', '', 1754, 1753, '[1755]', 'trip', '', 1, 1, 0, 0, '2017-03-22 16:18:42', '2017-04-20 13:19:58', '2017-04-20 13:19:58'),
(42, 88, '0.00', '', '', 1757, 1756, '[1758,1759]', 'article', '["31","34"]', 0, 0, 0, 0, '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(43, 88, '0.00', '', '', 1761, 1760, '[1762,1763,1764]', 'article', '["40","41"]', 0, 0, 0, 0, '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(44, 89, '1250.00', '', '{"type":"type3","table":{"table_price_3_header_1":"Option 1\\r\\nMarijg  fdbsdf","table_price_3_prices_1":["0","0","0","0"],"table_price_3_header_2":"Op 2\\r\\nGran ","table_price_3_prices_2":["0","0","0","0"]}}', 1772, 1771, '[1773,1774,1775]', 'trip', '', 0, 0, 0, 0, '2017-04-03 10:18:08', '2017-04-20 13:19:57', '2017-04-20 13:19:57'),
(45, 80, '0.00', '', '', 1777, 1776, '[1778,1779,1780]', 'article', '["24","25","38","39"]', 0, 0, 0, 0, '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(46, 80, '0.00', '', '', 1783, 1782, '[]', 'article', '', 0, 0, 0, 0, '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(47, NULL, '0.00', '', '', 0, 1820, 'null', 'default', '', 0, 1, 0, 14, '2017-04-23 10:48:25', '2017-11-06 19:47:43', NULL),
(48, 96, '600.00', '', '{"type":"type1","table":{"single":["1000","2000","4000"],"double":["320","20000","5000"],"triple":["6555","70000","5400"]}}', 1834, 1833, '[]', 'trip', '', 0, 0, 0, 0, '2017-05-06 22:48:23', '2017-05-06 22:48:23', NULL),
(49, NULL, '0.00', '', '', 1843, 1842, 'null', 'default', '', 1, 1, 0, 11, '2017-11-05 19:50:58', '2017-11-06 20:29:46', NULL),
(50, NULL, '0.00', '', '', 1845, 1844, 'null', 'default', '', 1, 1, 0, 1, '2017-11-06 20:29:36', '2017-11-06 20:33:08', NULL),
(51, NULL, '0.00', '', '', 1847, 1846, 'null', 'default', '', 1, 1, 0, 1, '2017-11-06 20:31:31', '2017-11-06 20:33:01', NULL),
(52, NULL, '0.00', '', '', 1849, 1848, 'null', 'default', '', 1, 0, 0, 0, '2017-11-06 20:35:01', '2017-11-06 20:45:27', NULL),
(53, NULL, '0.00', '', '', 1851, 1850, 'null', 'default', '', 1, 0, 0, 0, '2017-11-06 20:36:34', '2017-11-06 20:45:29', NULL),
(54, NULL, '0.00', '', '', 1853, 1852, 'null', 'default', '', 1, 0, 0, 2, '2017-11-06 20:37:11', '2017-11-06 20:57:17', NULL),
(55, NULL, '0.00', '', '', 1855, 1854, 'null', 'default', '', 1, 0, 0, 0, '2017-11-06 20:38:12', '2017-11-06 20:45:51', NULL),
(56, NULL, '0.00', '', '', 1857, 1856, 'null', 'default', '', 1, 0, 0, 0, '2017-11-06 20:39:10', '2017-11-06 20:45:53', NULL),
(57, NULL, '0.00', '', '', 1859, 1858, 'null', 'default', '', 1, 0, 0, 1, '2017-11-06 20:40:42', '2017-11-06 20:47:19', NULL),
(58, NULL, '0.00', '', '', 1861, 1860, 'null', 'default', '', 0, 0, 0, 0, '2017-11-06 20:42:37', '2017-11-06 20:42:37', NULL),
(59, NULL, '0.00', '', '', 1863, 1862, 'null', 'default', '', 0, 0, 0, 0, '2017-11-06 20:44:01', '2017-11-06 20:44:01', NULL),
(60, NULL, '0.00', '', '', 1865, 1864, 'null', 'default', '', 0, 0, 0, 0, '2017-11-06 20:45:15', '2017-11-06 20:45:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages_translate`
--

CREATE TABLE IF NOT EXISTS `pages_translate` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_title` varchar(300) NOT NULL,
  `page_city` varchar(200) NOT NULL,
  `page_period` varchar(200) NOT NULL,
  `page_short_desc` text NOT NULL,
  `page_body` text NOT NULL,
  `page_header_arr` text NOT NULL,
  `page_body_arr` text NOT NULL,
  `page_meta_title` varchar(300) NOT NULL,
  `page_meta_desc` text NOT NULL,
  `page_meta_keywords` text NOT NULL,
  `page_slug` varchar(300) NOT NULL,
  `included_services_and_amenities` text NOT NULL,
  `excluded_services_and_amenities` text NOT NULL,
  `lang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_translate`
--

INSERT INTO `pages_translate` (`id`, `page_id`, `page_title`, `page_city`, `page_period`, `page_short_desc`, `page_body`, `page_header_arr`, `page_body_arr`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`, `page_slug`, `included_services_and_amenities`, `excluded_services_and_amenities`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'جائزة ديربي للكرة الذهبية ', '', '', 'جائزة ديربي للكرة الذهبية ', '<h1><span style="color:#800080;"><span style="font-family:Times New Roman,Times,serif;">جائزة ديربي للكرة الذهبية&nbsp;</span></span></h1>\r\n\r\n<p><img alt="لا يتوفر نص بديل تلقائي." src="https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/16730420_628422877345578_6073178317849455002_n.jpg?oh=3ae3cfa7397eca972874cba13b521c64&amp;oe=594533D1" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="ربما تحتوي الصورة على: ‏‏نص‏‏" src="https://scontent.xx.fbcdn.net/v/t1.0-9/16807798_1316431128403835_4334300764927805956_n.jpg?oh=fafc502ca32a35da2f2390909b4e5c61&amp;oe=5937BD7E" /></p>\r\n\r\n<h1>&nbsp;</h1>\r\n\r\n<h1>&nbsp;</h1>\r\n', '', '', 'جائزة ديربي للكرة الذهبية ', 'صفحة جديدة', 'صفحة جديدة', 'صفحةجديدة', '', '', 1, '2017-01-30 07:39:21', '2017-02-18 08:45:32', NULL),
(2, 1, 'new_page', '', '', 'new_page', '<p>new_page</p>\r\n', '', '', 'new_page', 'new_page', 'new_page', 'new_page', '', '', 2, '2017-01-30 07:39:21', '2017-02-18 08:45:32', NULL),
(3, 2, 'كهرباء', '', '', 'كهرباء', '<p>كهرباء</p>\r\n', '', '', 'كهرباء', 'كهرباء', 'كهرباء', 'كهرباء', '', '', 1, '2017-01-30 07:49:39', '2017-02-14 15:06:00', NULL),
(4, 2, 'kahraba', '', '', 'kahraba', '<p>kahraba</p>\r\n', '', '', 'kahraba', 'kahraba', 'kahraba', 'kahraba', '', '', 2, '2017-01-30 07:49:40', '2017-02-14 15:06:00', NULL),
(5, 3, 'فوز مصر', '', '', 'فوز مصر', '', '', '', 'فوز مصر', 'فوز مصر', 'فوز مصر', 'فوزمصر', '', '', 1, '2017-01-30 08:34:20', '2017-02-14 15:06:45', NULL),
(6, 3, 'Egypt Won', '', '', 'Egypt Won', '', '', '', 'Egypt Won', 'Egypt Won', 'Egypt Won', 'EgyptWon', '', '', 2, '2017-01-30 08:34:20', '2017-02-14 15:06:45', NULL),
(7, 4, 'صفحه مقال للدوري المصري', '', '', 'صفحه مقال للدوري المصري', '<p>صفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصريصفحه مقال للدوري المصري</p>\r\n', '', '', 'صفحه مقال للدوري المصري', 'صفحه مقال للدوري المصري', 'صفحه مقال للدوري المصري', 'صفحه-مقال-للدوري-المصري', '', '', 1, '2017-02-08 15:12:59', '2017-02-14 15:04:47', NULL),
(8, 4, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-08 15:12:59', '2017-02-14 15:04:47', NULL),
(9, 5, 'صفحه خبرللدوري المصري', '', '', 'صفحه خبرللدوري المصري', '<p>صفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصريصفحه خبرللدوري المصري</p>\r\n', '', '', 'صفحه خبرللدوري المصري', 'صفحه خبرللدوري المصري', 'صفحه خبرللدوري المصري', 'صفحه-خبرللدوري-المصري', '', '', 1, '2017-02-08 15:14:13', '2017-02-15 09:42:14', NULL),
(10, 5, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-08 15:14:13', '2017-02-15 09:42:14', NULL),
(11, 6, 'صفحه صور للدوري المصري', '', '', 'صفحه صور للدوري المصري', '', '', '', 'صفحه صور للدوري المصري', 'صفحه صور للدوري المصري', 'صفحه صور للدوري المصري', 'صفحه-صور-للدوري-المصري', '', '', 1, '2017-02-08 15:27:24', '2017-02-13 15:57:56', NULL),
(12, 6, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-08 15:27:24', '2017-02-13 15:57:56', NULL),
(13, 7, 'صفحة فيديو للدوري المصري', '', '', 'صفحة فيديو للدوري المصري', '', '', '', 'صفحة فيديو للدوري المصري', 'صفحة فيديو للدوري المصري', 'صفحة فيديو للدوري المصري', 'صفحة-فيديو-للدوري-المصري', '', '', 1, '2017-02-08 15:31:18', '2017-02-14 15:05:06', NULL),
(14, 7, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-08 15:31:18', '2017-02-14 15:05:06', NULL),
(15, 8, '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '', '', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '<h1><a href="http://www.filgoal.com/articles/288909/%D8%A7%D9%84%D9%85%D8%AC%D9%86%D9%88%D9%86-%D8%AC%D9%86%D8%B4-%D9%84%D8%A7-%D8%A8%D8%A3%D8%B3-%D8%A8%D9%85%D9%86%D8%A7%D8%AF%D8%A7%D8%AA%D9%8A-%D9%85%D9%87%D8%B1%D8%AC-%D9%85%D8%A7%D8%AF%D9%85%D8%AA-%D8%A3%D9%86%D8%A7-%D8%A7%D9%84%D8%A8%D8%B7%D9%84">&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل</a></h1>\r\n', '', '', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', 'جنش', '', '', 1, '2017-02-14 15:09:14', '2017-02-14 15:09:14', NULL),
(16, 8, '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '', '', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '<h1><a href="http://www.filgoal.com/articles/288909/%D8%A7%D9%84%D9%85%D8%AC%D9%86%D9%88%D9%86-%D8%AC%D9%86%D8%B4-%D9%84%D8%A7-%D8%A8%D8%A3%D8%B3-%D8%A8%D9%85%D9%86%D8%A7%D8%AF%D8%A7%D8%AA%D9%8A-%D9%85%D9%87%D8%B1%D8%AC-%D9%85%D8%A7%D8%AF%D9%85%D8%AA-%D8%A3%D9%86%D8%A7-%D8%A7%D9%84%D8%A8%D8%B7%D9%84">&quot;المجنون&quot; جنش.. لا بأس بمناداتي &quot;مهرج&quot; مادمت أنا البطل</a></h1>\r\n', '', '', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', '"المجنون" جنش.. لا بأس بمناداتي "مهرج" مادمت أنا البطل', 'جنش', '', '', 2, '2017-02-14 15:09:14', '2017-02-14 15:09:14', NULL),
(17, 9, 'مواجهات ساخنة في قرعة التأهيلي للممتاز.." الأمير في مطب النسور "و"النهضة تعيد المحاوله من السوكي" ..!', '', '', 'مواجهات ساخنة في قرعة التأهيلي للممتاز.." الأمير في مطب النسور "و"النهضة تعيد المحاوله من السوكي" ..!', '<p>ديربي سبورت :</p>\r\n\r\n<p>أجريت اليوم بمكاتب الاتحاد العام لكرة القدم قرعة مباريات الدور الأول من الدور التأهيلي المؤهل للدوري الممتاز لعام 2017 بحضور عدد كبير من ممثلي الأندية المشاركة وتمت برمجة مباريات الذهاب &quot;10/3/2017&quot; وجولة الإياب بتاريخ &quot; 17/3/2017&quot; .</p>\r\n\r\n<p>يذكر أن عدد الفرق المشاركة في هذه المرحة &quot;14&quot; فريق من مدن السودان المختلفة وتقام المرحلة الأولى بنظام خروج المهزوم .</p>\r\n\r\n<p>ويتوقع أن تكون منافسة هذا الموسم قوية ومميزة من واقع الإهتمام المتزايد من مجالس إدارات الأندية المشاركة مع وجود دعم من بعض ولاة الولايات المختلفة ؛ واقام عدد كبير من هذه الأندية معسكرات مقفولة أستعدادا لهذه المنافسة .</p>\r\n\r\n<p>ويعد طريق &quot; الأمير البحراوي&quot; الذي يسعى للعودة مجددا للممتاز شائك وصعب وذلك بمواجهته&quot; للنسور جبل أولياء&quot; الساعي لحجز مقعد بالدرجة الممتازة بجوار نده تريعة البجة .</p>\r\n\r\n<p>واوقعت القرعة الجاريين &quot; اشبال الدويم ووادي النيل ربك &quot; في مواجهة لاتحتمل القسمة على أثنين .</p>\r\n\r\n<p>وتضم منافسة هذا الموسم أندية مشهود لها بالأداء الراقي والمتطور منها : &quot; النهضة ربك الذي كان قريبا من التواجد بالممتاز في الموسم الماضي بالأضافة لفريقي الميرغني كسلا &quot; الأنيق &quot; وهلال الفاشر وعدد من الأندية الكبيرة .</p>\r\n\r\n<p>&quot;ديربي سبورت &quot; ترصد جدول مباريات الدور التأهيلي ادناه .</p>\r\n\r\n<p><img alt="c064dadc-01f4-45b1-9520-8390952b4a05" height="1280" src="http://www.derby1sport.com/wp-content/uploads/2017/02/c064dadc-01f4-45b1-9520-8390952b4a05.jpg" width="720" /></p>\r\n', '', '', 'مواجهات ساخنة في قرعة التأهيلي للممتاز.." الأمير في مطب النسور "و"النهضة تعيد المحاوله من السوكي" ..!', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', 'حلمي', '', '', 1, '2017-02-14 15:10:20', '2017-02-20 18:25:48', NULL),
(18, 9, 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', '', '', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', '<h1><a href="http://www.filgoal.com/articles/288951/%D8%AD%D9%84%D9%85%D9%8A-%D9%82%D8%AF-%D8%A3%D8%B6%D9%85-%D8%B4%D9%8A%D9%83%D8%A7%D8%A8%D8%A7%D9%84%D8%A7-%D9%84%D9%85%D8%B9%D8%B3%D9%83%D8%B1-%D9%84%D9%82%D8%A7%D8%A1-%D8%A7%D9%84%D8%A3%D9%88%D9%84%D9%8A%D9%85%D8%A8%D9%8A">حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبيحلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي</a></h1>\r\n', '', '', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', 'حلمي: قد أضم شيكابالا لمعسكر لقاء الأوليمبي', 'حلمي', '', '', 2, '2017-02-14 15:10:20', '2017-02-20 18:25:48', NULL),
(19, 10, 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', '', '', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', '<p>اليوم يصادف عيد الحب، وبشكل ما ارتبط ذلك اليوم بكرة القدم، مدربون ولاعبون تحدثوا عن ذلك اليوم المميز في حياتهم، بعضهم كان حاد الطباع وأخر كان هادئا ومتفهما.</p>\r\n\r\n<p>ماذا قال البعض؟ لويس فان جال كان حاد الطباع عند سؤاله خلال العام الماضي عن عيد الحب، في حين أن سام ألاردايس مدرب كريستال بالاس الحالي كان يصطحب زوجته إلى دبي.</p>\r\n\r\n<p>FilGoal.com يصحبكم عبر التقرير التالي إلى بعض أقوال المدربين حول احتفالاتهم بعيد الحب.</p>\r\n', '', '', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', 'قالوافيعيدالحبهوليوودمورينيوومفاجأةماركهيوز..وخسارةبيللجريني', '', '', 1, '2017-02-14 15:10:59', '2017-02-14 15:10:59', NULL),
(20, 10, 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', '', '', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', '<p>اليوم يصادف عيد الحب، وبشكل ما ارتبط ذلك اليوم بكرة القدم، مدربون ولاعبون تحدثوا عن ذلك اليوم المميز في حياتهم، بعضهم كان حاد الطباع وأخر كان هادئا ومتفهما.</p>\r\n\r\n<p>ماذا قال البعض؟ لويس فان جال كان حاد الطباع عند سؤاله خلال العام الماضي عن عيد الحب، في حين أن سام ألاردايس مدرب كريستال بالاس الحالي كان يصطحب زوجته إلى دبي.</p>\r\n\r\n<p>FilGoal.com يصحبكم عبر التقرير التالي إلى بعض أقوال المدربين حول احتفالاتهم بعيد الحب.</p>\r\n', '', '', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', 'قالوا في عيد الحب: هوليوود مورينيو ومفاجأة مارك هيوز.. وخسارة بيللجريني\r\n', 'قالوافيعيدالحبهوليوودمورينيوومفاجأةماركهيوز..وخسارةبيللجريني', '', '', 2, '2017-02-14 15:10:59', '2017-02-14 15:10:59', NULL),
(21, 11, 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', '', '', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', '<h1><a href="http://www.filgoal.com/articles/288953/%D9%85%D8%A4%D9%85%D9%86-%D8%B3%D9%84%D9%8A%D9%85%D8%A7%D9%86-%D9%84%D8%AF%D9%8A%D9%86%D8%A7-%D9%81%D8%B1%D9%8A%D9%82-%D9%83%D8%A8%D9%8A%D8%B1-%D9%85%D9%88%D8%A7%D8%AC%D9%87%D8%A9-%D8%A7%D9%84%D9%85%D9%82%D8%A7%D8%B5%D8%A9-%D9%84%D9%8A%D9%81%D8%B2-%D8%A7%D9%84%D8%A3%D9%81%D8%B6%D9%84-%D9%84%D8%A7-%D9%85%D8%B4%D9%83%D9%84%D8%A9-%D9%84%D8%AF%D9%8A">مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي</a></h1>\r\n', '', '', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن-سليمان', '', '', 1, '2017-02-14 15:12:27', '2017-02-14 15:13:41', NULL),
(22, 11, 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', '', '', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', '<h1><a href="http://www.filgoal.com/articles/288953/%D9%85%D8%A4%D9%85%D9%86-%D8%B3%D9%84%D9%8A%D9%85%D8%A7%D9%86-%D9%84%D8%AF%D9%8A%D9%86%D8%A7-%D9%81%D8%B1%D9%8A%D9%82-%D9%83%D8%A8%D9%8A%D8%B1-%D9%85%D9%88%D8%A7%D8%AC%D9%87%D8%A9-%D8%A7%D9%84%D9%85%D9%82%D8%A7%D8%B5%D8%A9-%D9%84%D9%8A%D9%81%D8%B2-%D8%A7%D9%84%D8%A3%D9%81%D8%B6%D9%84-%D9%84%D8%A7-%D9%85%D8%B4%D9%83%D9%84%D8%A9-%D9%84%D8%AF%D9%8A">مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لديمؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي</a></h1>\r\n', '', '', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن سليمان: لدينا فريق كبير.. مواجهة المقاصة؟ ليفز الأفضل لا مشكلة لدي', 'مؤمن', '', '', 2, '2017-02-14 15:12:27', '2017-02-14 15:13:41', NULL),
(23, 12, 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', '', '', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', '<h1>خاص&nbsp;خبر في الجول - شيكابالا يجدد &quot;على بياض&quot; للزمالك</h1>\r\n', '', '', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', 'خاصخبرفيالجول-شيكابالايجددعلىبياضللزمالك', '', '', 1, '2017-02-14 15:12:33', '2017-02-14 15:12:33', NULL),
(24, 12, 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', '', '', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', '<h1>خاص&nbsp;خبر في الجول - شيكابالا يجدد &quot;على بياض&quot; للزمالك</h1>\r\n', '', '', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', 'خاص  خبر في الجول - شيكابالا يجدد "على بياض" للزمالك\r\n', 'خاصخبرفيالجول-شيكابالايجددعلىبياضللزمالك', '', '', 2, '2017-02-14 15:12:33', '2017-02-14 15:12:33', NULL),
(25, 13, 'إحصائيات #ليلة_الأبطال - أين برشلونة؟', '', '', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟', '<p>إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟إحصائيات #ليلة_الأبطال - أين برشلونة؟</p>\r\n', '', '', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟', 'برشلونة-إحصائيات', '', '', 1, '2017-02-15 05:49:35', '2017-02-15 05:49:35', NULL),
(26, 13, 'إحصائيات #ليلة_الأبطال - أين برشلونة؟ en', '', '', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟ en', '<p>إحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ enإحصائيات #ليلة_الأبطال - أين برشلونة؟ en</p>\r\n', '', '', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟ en', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟ en', 'إحصائيات #ليلة_الأبطال - أين برشلونة؟ en', 'إحصائيات-#ليلة_الأبطال-أين-برشلونة؟-en', '', '', 2, '2017-02-15 05:49:35', '2017-02-15 05:49:35', NULL),
(27, 14, 'الزمالك يسعى لاستغلال نشوة السوبر في انطلاقته بمرحلة الدوري الثانية أمام الانتاج', '', '', 'يسعى فريق الزمالك لاستغلال نشوة التتويج بالسوبر المصري في انطلاقته الثانية بالدوري المصري عندما يبدأ مشوار الدور الثاني أمام الانتاج الحربي. ', '<p>يسعى فريق الزمالك لاستغلال نشوة التتويج بالسوبر المصري في انطلاقته الثانية بالدوري المصري عندما يبدأ مشوار الدور الثاني أمام الانتاج الحربي.&nbsp;<br />\r\n<br />\r\nويستضيف ملعب بتروسبورت مباراة الزمالك والانتاج الحربي في الساعة 8 مساء الأربعاء ضمن مباريات الجولة الـ18 لمسابقة الدوري الممتاز.&nbsp;<br />\r\n<br />\r\nويحتل نادي الزمالك المركز الثالث في ترتيب جدول المسابقة برصيد 34 نقطة من 15 مباراة حيث يتبقى له مباراتين أمام مصر المقاصة وطلائع الجيش.&nbsp;<br />\r\n<br />\r\nويأتي الانتاج الحربي تحت قيادة مدربه شوقي غريب في المركز الثالث عشر في ترتيب الجدول برصيد 17 نقطة من 17 مباراة.&nbsp;<br />\r\n<br />\r\nوكان لقاء الدور الأول الذي اقيم يوم 21 ديسمبر الماضي بعد تأجيله من شهر اغسطس بسبب مشاركة الزمالك بدوري ابطال افريقيا بفوز الأبيض بهدف نظيف لباسم مرسي.&nbsp;<br />\r\n<br />\r\nوقامت لجنة الحكام بالاتحاد المصري لكرة القدم بتعيين سمير عثمان حكما للمباراة، ويعاونه تحسين أبو السادات وهاني خيري، وإبراهيم محجوب حكما رابعا.&nbsp;<br />\r\n<br />\r\nوتوج الزمالك مؤخرا وتحديدا يوم 10 فبراير الجاري بكأس السوبر المصري على حساب الأهلي في مباراة انتهى وقتها الأصلي بالتعادل السلبي ليفوز الأبيض على الأحمر بركلات الجزاء.&nbsp;<br />\r\n<br />\r\nوتعيش القلعة البيضاء أجواء احتفالية على مدار الأيام السابقة بجانب انتهاء خلاف قائد الفريق محمود عبدالرازق &quot;شيكابالا&quot; مع مرتضى منصور رئيس النادي وتعديل عقده.&nbsp;<br />\r\n<br />\r\nوحرص محمد حلمي المدير الفني لنادي الزمالك على عقد جلسات مع اللاعبين والتأكيد على ضرورة غلق صفحة السوبر والتركيز في مواجهة الانتاج الحربي.&nbsp;<br />\r\n<br />\r\nويرغب حلمي في عدم فقدان الفريق لأي نقطة في بداية مشوار الدور الثاني من المسابقة من أجل الاستمرار في صراع التنافس على اللقب والضغط على متصدر المسابقة النادي الأهلي.&nbsp;<br />\r\n<br />\r\nويستهدف الزمالك أيضا سقوط مصر المقاصة الوصيف في فخ التعادل أمام فريق طنطا وفقدان نقطتين من أجل العودة للمركز الثاني والانطلاق للمنافسة على المركز الأول.&nbsp;<br />\r\n<br />\r\nويغيب عن القلعة البيضاء علي جبر وطارق حامد للحصول على راحة سلبية بعد المشاركة الدولية مع المنتخب بجانب باسم مرسي للإيقاف، وشيكابالا وعلي فتحي لعدم الجاهزية ومحمد ناصف للإصابة.&nbsp;<br />\r\n<br />\r\nومن المنتظر أن تشهد المباراة الظهور الأول للشاب أحمد فتوح الذي تم قيده افريقيا مؤخرا في الجبهة اليسرى سبب غياب الثنائي علي فتحي ومحمد ناصف.&nbsp;<br />\r\n<br />\r\nويتجه الجهاز الفني لنادي الزمالك في مواصلة الاعتماد على محمود عبدالرحيم &quot;جنش&quot; في حراسة المرمى بدلا من أحمد الشناوي نظرا لما قدمه من أداء في السوبر المصري الأخير.&nbsp;<br />\r\n<br />\r\nويحلم أبناء شوقي غريب في تحقيق انتصار &quot;استثنائي&quot; كالذي حدث الموسم الماضي عندما فاز الفريق على الزمالك بنتيجة 3-1 في مباراة الدور الثاني.&nbsp;<br />\r\n<br />\r\nولم يشهد تاريخ مواجهات الزمالك والانتاج الحربي أي انتصار للأخير سوى الذي تحقق الموسم الماضي، ولم يفز الفريق هذا الموسم في اخر 5 مباريات له بالمسابقة.</p>\r\n', '', '', 'الزمالك يسعى لاستغلال نشوة السوبر في انطلاقته بمرحلة الدوري الثانية أمام الانتاج', 'الزمالك يسعى لاستغلال نشوة السوبر في انطلاقته بمرحلة الدوري الثانية أمام الانتاج\r\n\r\n', 'الزمالك يسعى لاستغلال نشوة السوبر في انطلاقته بمرحلة الدوري الثانية أمام الانتاج\r\n\r\n', 'لزمالك-اخبار125', '', '', 1, '2017-02-15 09:02:20', '2017-02-15 09:02:20', NULL),
(28, 14, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:02:20', '2017-02-15 09:02:20', NULL),
(29, 15, 'مهارات ايمن حفني - فيديو', '', '', 'مهارات الساحر ايمن حفني لاعب خط وسط فريق نادي الزمالك', '', '', '', 'مهارات الساحر ايمن حفني لاعب خط وسط فريق نادي الزمالك', 'مهارات الساحر ايمن حفني لاعب خط وسط فريق نادي الزمالك', 'مهارات الساحر ايمن حفني لاعب خط وسط فريق نادي الزمالك', 'مهارات-ايمن-حفني-فيديو', '', '', 1, '2017-02-15 09:06:09', '2017-02-15 09:06:09', NULL),
(30, 15, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:06:09', '2017-02-15 09:06:09', NULL),
(31, 16, 'مهارات ميسي في دوري ابطال اوروبا', '', '', 'مهارات ميسي في دوري ابطال اوروبا', '', '', '', 'مهارات ميسي في دوري ابطال اوروبا', 'مهارات ميسي في دوري ابطال اوروبا', 'مهارات ميسي في دوري ابطال اوروبا', 'مهارات-ميسي-في', '', '', 1, '2017-02-15 09:08:28', '2017-02-22 18:44:25', NULL),
(32, 16, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:08:28', '2017-02-22 18:44:25', NULL),
(33, 17, 'صور ميسي في دوري الابطال', '', '', 'صور ميسي في دوري الابطال', '', '', '', 'صور ميسي في دوري الابطال', 'صور ميسي في دوري الابطال', 'صور ميسي في دوري الابطال', 'صور-ميسي-في-د', '', '', 1, '2017-02-15 09:10:30', '2017-02-15 09:10:30', NULL),
(34, 17, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:10:30', '2017-02-15 09:10:30', NULL),
(35, 18, 'صور رونالدو فى دوري ابطال اوروبا', '', '', 'صور رونالدو فى دوري ابطال اوروبا', '', '', '', 'صور رونالدو فى دوري ابطال اوروبا', 'صور رونالدو فى دوري ابطال اوروبا', 'صور رونالدو فى دوري ابطال اوروبا', 'صور-رونالدو-في', '', '', 1, '2017-02-15 09:13:20', '2017-02-22 18:45:26', NULL),
(36, 18, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:13:20', '2017-02-22 18:45:26', NULL),
(37, 19, 'مجلس الهلال يعقد أﺟﺘﻤﺎﻋﺎً لمناقشة التدريب ورحلة نيالا .!\r\n', '', '', 'ديربي سبورت  :  الخرطوم \r\n\r\nسوف يعقد مجلس إدارة نادي الهلال في غضون الساعات مقبلة  ﺟﺘﻤﺎﻋﺎً مهماً  لحسم العديد من الملفات ويأتي في مقدمة هذه الملفات تسمية مدير فني جديد للفريق  الأول لكرة القدم  خلفا للفرنسي " دينيس لافاني " والذي تمت إقالته  امس بجانب مساعده الجزائري عمارة مرواني  بسبب سوء نتائج   الفريق في الدوري الممتاز .\r\n\r\nويناقش الإجتماع كذلك أمر رئاسة البعثة الهلالية المتجهة الي مدينة نيالا  لمواجهة فريقي "حي الوادي ومريخ نيالا " ضمن مباريات الجولة السابعة والثامنة من الدوري الممتاز  .', '<p>ديربي سبورت &nbsp;: &nbsp;الخرطوم&nbsp;</p>\r\n\r\n<p>سوف يعقد مجلس إدارة نادي الهلال في غضون الساعات مقبلة &nbsp;ﺟﺘﻤﺎﻋﺎً مهماً &nbsp;لحسم العديد من الملفات ويأتي في مقدمة هذه الملفات تسمية مدير فني جديد للفريق &nbsp;الأول لكرة القدم &nbsp;خلفا للفرنسي &quot; دينيس لافاني &quot; والذي تمت إقالته &nbsp;امس بجانب مساعده الجزائري عمارة مرواني &nbsp;بسبب سوء نتائج &nbsp; الفريق في الدوري الممتاز .</p>\r\n\r\n<p>ويناقش الإجتماع كذلك أمر رئاسة البعثة الهلالية المتجهة الي مدينة نيالا &nbsp;لمواجهة فريقي &quot;حي الوادي ومريخ نيالا &quot; ضمن مباريات الجولة السابعة والثامنة من الدوري الممتاز &nbsp;.</p>\r\n', '', '', 'مجلس الهلال يعقد أﺟﺘﻤﺎﻋﺎً لمناقشة التدريب ورحلة نيالا .!\r\n', 'ميسي', 'ميسي', 'تأجيلزيارةميسيلمصر', '', '', 1, '2017-02-15 09:19:32', '2017-02-20 13:18:01', NULL),
(38, 19, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-15 09:19:32', '2017-02-20 13:18:01', NULL),
(39, 20, 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '', '', 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '<p>إنريكي: النتيجة انعكاس لما حدث في الملعب<br />\r\n&nbsp;</p>\r\n', '', '', 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '', '', 'إنريكيالنتيجةانعكاسلماحدثفيالملعب', '', '', 1, '2017-02-15 10:43:44', '2017-02-15 10:43:44', NULL),
(40, 20, 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '', '', 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '<p>إنريكي: النتيجة انعكاس لما حدث في الملعب<br />\r\n&nbsp;</p>\r\n', '', '', 'إنريكي: النتيجة انعكاس لما حدث في الملعب\r\n', '', '', 'إنريكيالنتيجةانعكاسلماحدثفيالملعب', '', '', 2, '2017-02-15 10:43:44', '2017-02-15 10:43:44', NULL),
(41, 16, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-02-22 18:44:25', '2017-02-22 18:44:25', NULL),
(42, 18, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-02-22 18:45:26', '2017-02-22 18:45:26', NULL),
(43, 21, 'new', '', '', 'new', '<p>new</p>\r\n', '', '', 'new', '', '', '', '', '', 1, '2017-02-27 18:37:18', '2017-02-27 18:37:18', NULL),
(44, 21, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-02-27 18:37:18', '2017-02-27 18:37:18', NULL),
(45, 21, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-02-27 18:37:18', '2017-02-27 18:37:18', NULL),
(46, 22, 'اوتوباش تفوز بحقوق رعاية المريخ حصرياً', '', '', '', '<p style="text-align: right;"><br />\r\nديربي سبورت : الخرطوم<br />\r\nحَظَت شركة اوتوباش للسيارات برعاية حصرية لنادي المريخ طوال الموسم الرياضي على أن تقوم الشركة بوضع شعارها على تيشيرت اللاعبين علاوة على لوحاتها الترويجية داخل الاستاد.<br />\r\nالشركة التي تعتبر الوكيل الحصري لسيارات&rdquo; جيلي&rdquo; دَعَّمت جِدِّيَتها بالتقاط القفاز كأحد رعاة مباراة الفريق بالبطولة العربية امام تفرغ زينة الموريتاني في إياب الدور الثاني من تصفيات دوري أبطال العرب.<br />\r\nوأنهت اوتوباش اتفاقها بتوقيع العقد مع المريخ الذي مَثَّلَهُ رئيس قطاع التسويق &ldquo;محمد الريح &rdquo; ونائبه طارق الشيخ &rdquo; نائب رئيس قطاع التسويق ومن جانب شركة &ldquo;اوتوباش &rdquo; مديرها العام أمين نجيب وعماد عبدالحفيظ مدير التسويق والمبيعات.<br />\r\nيذكر أن نادي المريخ اتفق مع الشركة على شراء سبع سيارات &rdquo; جيلي &rdquo; بقيمة الإعلان والرعاية .</p>\r\n', '', '', 'اوتوباش تفوز بحقوق رعاية المريخ حصرياً', '', '', '', '', '', 1, '2017-03-02 09:55:16', '2017-03-02 09:56:13', NULL),
(47, 22, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-03-02 09:55:16', '2017-03-02 09:56:13', NULL),
(48, 22, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-03-02 09:55:16', '2017-03-02 09:56:13', NULL),
(49, 23, 'new page', '', '', 'new page', '<p>new page</p>\r\n', '', '', '', '', '', 'new-page', '', '', 1, '2017-03-20 12:34:56', '2017-03-22 15:37:57', NULL),
(50, 23, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-03-20 12:34:56', '2017-03-20 12:34:56', NULL),
(51, 23, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-03-20 12:34:56', '2017-03-20 12:34:56', NULL),
(52, 24, 'Barcelona', 'Barcelona', '7day 6 night', 'Barcelona', '<h5>&nbsp;</h5>\r\n\r\n<p><u>Rates include:</u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Flight ticket Cairo / Barcelona / &nbsp;Cairo By Egypt Air Economy Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;06 nights (Bed Only) 3 stars &amp; Bed and breakfast 4 stars at any of the above mentioned hotels</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Transfer airport / Hotel Airport by A/C coach&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p dir="LTR"><u>Rates Do Not Include:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Early check ins and late check outs</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Tips to guides, representatives and drivers</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Entry SCHENGEN visa &amp; Medical insurance (cost EGP 1250 per person)</p>\r\n\r\n<p dir="LTR"><u>Do Not Include&nbsp;</u><u>&quot;Optional Tours and Excursion Package&quot;&nbsp;</u><u>:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Day Port Aventura Park&nbsp;&nbsp;(<strong>optional:</strong>&nbsp;Adult 65&nbsp; Euros / Child 60 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Flaminco Show (including soft drink)&nbsp; &nbsp;&nbsp;(<strong>optional:</strong>&nbsp;Adult 40&nbsp; Euros / Child&nbsp; 35 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Visit FC BARCELONE Stadium &nbsp;(<strong>optional:</strong>&nbsp;Adult 40&nbsp; Euros / Child&nbsp; 35 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;"><u>Flight Details:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">MS 767&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 August 2016&nbsp; Cairo/ Barcelona &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10:50&nbsp;&nbsp;&nbsp; 14:20</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">MS 768&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 23 August 2016&nbsp; Barcelona /Cairo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15:20&nbsp;&nbsp;&nbsp; 20:20&nbsp;</p>\r\n\r\n<h5>&nbsp;</h5>\r\n\r\n<p><u>Rates include:</u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Flight ticket Cairo / Barcelona / &nbsp;Cairo By Egypt Air Economy Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;06 nights (Bed Only) 3 stars &amp; Bed and breakfast 4 stars at any of the above mentioned hotels</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Transfer airport / Hotel Airport by A/C coach&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p dir="LTR"><u>Rates Do Not Include:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Early check ins and late check outs</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Tips to guides, representatives and drivers</p>\r\n\r\n<p dir="LTR" style="margin-left:36pt;">v&nbsp;&nbsp;Entry SCHENGEN visa &amp; Medical insurance (cost EGP 1250 per person)</p>\r\n\r\n<p dir="LTR"><u>Do Not Include&nbsp;</u><u>&quot;Optional Tours and Excursion Package&quot;&nbsp;</u><u>:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Day Port Aventura Park&nbsp;&nbsp;(<strong>optional:</strong>&nbsp;Adult 65&nbsp; Euros / Child 60 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Flaminco Show (including soft drink)&nbsp; &nbsp;&nbsp;(<strong>optional:</strong>&nbsp;Adult 40&nbsp; Euros / Child&nbsp; 35 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Visit FC BARCELONE Stadium &nbsp;(<strong>optional:</strong>&nbsp;Adult 40&nbsp; Euros / Child&nbsp; 35 Euros)</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;"><u>Flight Details:</u></p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">MS 767&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 August 2016&nbsp; Cairo/ Barcelona &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10:50&nbsp;&nbsp;&nbsp; 14:20</p>\r\n\r\n<p dir="LTR" style="margin-left:18pt;">MS 768&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 23 August 2016&nbsp; Barcelona /Cairo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15:20&nbsp;&nbsp;&nbsp; 20:20&nbsp;</p>\r\n', '["Inclusions :","Exclusions :"]', '["<h5>&nbsp;<\\/h5>\\r\\n\\r\\n<p><u>Rates include:<\\/u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Flight ticket Cairo \\/ Barcelona \\/ &nbsp;Cairo By Egypt Air Economy Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;06 nights (Bed Only) 3 stars &amp; Bed and breakfast 4 stars at any of the above mentioned hotels<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Transfer airport \\/ Hotel Airport by A\\/C coach&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\"><u>Rates Do Not Include:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Early check ins and late check outs<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Tips to guides, representatives and drivers<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Entry SCHENGEN visa &amp; Medical insurance (cost EGP 1250 per person)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\"><u>Do Not Include&nbsp;<\\/u><u>&quot;Optional Tours and Excursion Package&quot;&nbsp;<\\/u><u>:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Day Port Aventura Park&nbsp;&nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 65&nbsp; Euros \\/ Child 60 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Flaminco Show (including soft drink)&nbsp; &nbsp;&nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 40&nbsp; Euros \\/ Child&nbsp; 35 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Visit FC BARCELONE Stadium &nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 40&nbsp; Euros \\/ Child&nbsp; 35 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\"><u>Flight Details:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">MS 767&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 August 2016&nbsp; Cairo\\/ Barcelona &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10:50&nbsp;&nbsp;&nbsp; 14:20<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">MS 768&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 23 August 2016&nbsp; Barcelona \\/Cairo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15:20&nbsp;&nbsp;&nbsp; 20:20&nbsp;<\\/p>\\r\\n","<h5>&nbsp;<\\/h5>\\r\\n\\r\\n<p><u>Rates include:<\\/u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Flight ticket Cairo \\/ Barcelona \\/ &nbsp;Cairo By Egypt Air Economy Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;06 nights (Bed Only) 3 stars &amp; Bed and breakfast 4 stars at any of the above mentioned hotels<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Transfer airport \\/ Hotel Airport by A\\/C coach&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\"><u>Rates Do Not Include:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Early check ins and late check outs<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Tips to guides, representatives and drivers<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:36pt;\\">v&nbsp;&nbsp;Entry SCHENGEN visa &amp; Medical insurance (cost EGP 1250 per person)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\"><u>Do Not Include&nbsp;<\\/u><u>&quot;Optional Tours and Excursion Package&quot;&nbsp;<\\/u><u>:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Day Port Aventura Park&nbsp;&nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 65&nbsp; Euros \\/ Child 60 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Flaminco Show (including soft drink)&nbsp; &nbsp;&nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 40&nbsp; Euros \\/ Child&nbsp; 35 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Half Day Visit FC BARCELONE Stadium &nbsp;(<strong>optional:<\\/strong>&nbsp;Adult 40&nbsp; Euros \\/ Child&nbsp; 35 Euros)<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\"><u>Flight Details:<\\/u><\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">MS 767&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17 August 2016&nbsp; Cairo\\/ Barcelona &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10:50&nbsp;&nbsp;&nbsp; 14:20<\\/p>\\r\\n\\r\\n<p dir=\\"LTR\\" style=\\"margin-left:18pt;\\">MS 768&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 23 August 2016&nbsp; Barcelona \\/Cairo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15:20&nbsp;&nbsp;&nbsp; 20:20&nbsp;<\\/p>\\r\\n"]', 'Lake Nasser Trip one Lake Nasser Trip one', 'Lake Nasser Trip one Lake Nasser Trip one', 'Lake Nasser Trip one Lake Nasser Trip one', 'Barcelona', '', '', 1, '2017-03-20 13:10:10', '2017-04-17 09:50:50', NULL),
(53, 24, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-03-20 13:10:10', '2017-04-17 09:50:50', NULL),
(54, 24, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 3, '2017-03-20 13:10:10', '2017-04-17 09:50:50', NULL);
INSERT INTO `pages_translate` (`id`, `page_id`, `page_title`, `page_city`, `page_period`, `page_short_desc`, `page_body`, `page_header_arr`, `page_body_arr`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`, `page_slug`, `included_services_and_amenities`, `excluded_services_and_amenities`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(55, 25, 'Lake Nasser Trip two', 'cairo', '20days', '', '<p><span style="font-size:14px;"><strong><span style="color:#ff0000;"><u>&nbsp;Day 1:Arrival Cairo</u></span></strong></span></p>\r\n\r\n<p><strong>Welcome to Cairo, Egypt&nbsp;</strong></p>\r\n\r\n<p><strong>&nbsp;</strong></p>\r\n\r\n<p><strong>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle. At hotel the tour manager will assist with a smooth check-in and review your holiday itinerary with you to establish and confirm pick-up times for each tour. Overnight in Cairo.</strong></p>\r\n\r\n<p><strong>Welcome Drink</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 2:Pyramids / Sakkara</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast at your hotel in Cairo and then met by your personal guide who will accompany you to Giza Plateau to visit the Great Pyramids of Cheops, Chefren and Mykerinus - Famous Sphinx and Valley temple facing the great statue. Additional visit to the Solar Boat infront of Cheops Pyramid (extra ticket on spot) Lunch included during the tour and then proceed to Sakkara area to visit Sakkara Complex and first pyramid ever built (Djoser Pyramid). Move to Memphis; Old kingdom&#39;s capital and famous Necropolis. Overnight in Cairo.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 3:Cairo Sightseeing / Train to Aswan</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Check out after breakfast at your hotel in Cairo and and then you will be escorted by your private guide to visit: Egyptian Museum, Treasures room for the child king Tutankhamen, alongside many other fascinating artifacts. Additional visit to the mummies room at the museum (extra ticket on spot) Lunch through out the tour at local restaurant, then moving to visit Saladin Citadel including Mohamed Ali Alabaster Mosque inside. Continue to Coptic Cairo to visit the Hanging Church and Ben Ezra Synagogue. Continue driving &nbsp;to explore Khan El Khalili, Cairo&rsquo;s old bazaar. Later, you will be transferred to Giza railway station to board the sleeper train to Aswan. Dinner and breakfast will be served on board - overnight inside your compartment.</strong></p>\r\n\r\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 4:Embark/ Aswan Sightseeing</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>You will be met and assisted at Aswan station by our representative, embark your Nile River Cruise , before Lunch at 11:00 a.m, then enjoy visiting the world famous High Dam, The awesome Philae temple, &nbsp;which is devoted to the two goddesses Isis and Hathor , and &nbsp;the largest known ancient obelisk, located in the northern region of the stone quarries of ancient Egypt, the Unfinished Obelisk. Dinner on board and overnight in Aswan.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 5:Kom Ombo / Edfu</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sail to Kom Ombo, have breakfast on board and Visit the Temple shared by two gods Sobek &amp; Haeroris in Kom Ombo. Sail to Edfu, lunch on board after that Visit the best-preserved cult temple in Egypt, Horus Temple in Edfu. Take afternoon tea during sailing to Esna. Dinner and overnight on board.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 6:Luxor West Bank</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sail to Luxor, have breakfast on board then Visit the West Bank, &nbsp;take an excursion to the royal cemetery for 62 Pharaohs, Valley of the Kings, then to mortuary Temple of Queen Hatshepsut, which was built by the architects of the New Kingdom Pharaoh Hatshepsut approximately in the 15th century BC, at El-Deir El-Bahari &amp; the Colossi of Memnon. Lunch on board. Enjoy afternoon tea during sailing. Dinner on board Overnight in Luxor.</strong></p>\r\n\r\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 7:Luxor East Bank / Train to Cairo</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast aboard cruise before disembarkation and then you will be accompanied by your guide to visit Karnak &amp; Luxor temples at the East bank of the River Nile and later you will be &nbsp;transferred &nbsp;to Luxor railway station to board the sleeper train back to Cairo. Have your dinner &nbsp;and overnight on board sleeper train in your private compartment ( twin beds).</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 8:Back to Cairo</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast aboard sleeper train before arriving Giza train station, where you will be met and assisted by Memphis Tours representative who will transfer you to your hotel for free time at leisure( you will check into your room as early as possible ) with some optional excursions available in Cairo and overnight stay at your hotel in Cairo</strong></p>\r\n\r\n<p><strong>Meals: Breakfast</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 9:Optional Alexandria</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Free day in Cairo or optional tour to Alexandria visiting Qaitbay Citadel, built in the 15th century by Sultan Qaitbay on the spot of Alexandria&rsquo;s ancient lighthouse. Then move on to the new and much acclaimed Alexandria Library. Also visit the Roman cemetery cut out of the rock, the Catacombs. Return to Cairo and overnight.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 10:Final Departure</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast at the hotel in Cairo - free time before your flight - check out and then you will be transferred to Cairo International Airport for final departure.</strong></p>\r\n', '["Inclusions:","Exclusions :"]', '["<p><u>&nbsp;<strong>Day 1:Arrival Cairo<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p><strong>Welcome to Cairo, Egypt&nbsp;<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>&nbsp;<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle. At hotel the tour manager will assist with a smooth check-in and review your holiday itinerary with you to establish and confirm pick-up times for each tour. Overnight in Cairo.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Welcome Drink<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 2:Pyramids \\/ Sakkara<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Breakfast at your hotel in Cairo and then met by your personal guide who will accompany you to Giza Plateau to visit the Great Pyramids of Cheops, Chefren and Mykerinus - Famous Sphinx and Valley temple facing the great statue. Additional visit to the Solar Boat infront of Cheops Pyramid (extra ticket on spot) Lunch included during the tour and then proceed to Sakkara area to visit Sakkara Complex and first pyramid ever built (Djoser Pyramid). Move to Memphis; Old kingdom&#39;s capital and famous Necropolis. Overnight in Cairo.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast, Lunch<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 3:Cairo Sightseeing \\/ Train to Aswan<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Check out after breakfast at your hotel in Cairo and and then you will be escorted by your private guide to visit: Egyptian Museum, Treasures room for the child king Tutankhamen, alongside many other fascinating artifacts. Additional visit to the mummies room at the museum (extra ticket on spot) Lunch through out the tour at local restaurant, then moving to visit Saladin Citadel including Mohamed Ali Alabaster Mosque inside. Continue to Coptic Cairo to visit the Hanging Church and Ben Ezra Synagogue. Continue driving &nbsp;to explore Khan El Khalili, Cairo&rsquo;s old bazaar. Later, you will be transferred to Giza railway station to board the sleeper train to Aswan. Dinner and breakfast will be served on board - overnight inside your compartment.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 4:Embark\\/ Aswan Sightseeing<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>You will be met and assisted at Aswan station by our representative, embark your Nile River Cruise , before Lunch at 11:00 a.m, then enjoy visiting the world famous High Dam, The awesome Philae temple, &nbsp;which is devoted to the two goddesses Isis and Hathor , and &nbsp;the largest known ancient obelisk, located in the northern region of the stone quarries of ancient Egypt, the Unfinished Obelisk. Dinner on board and overnight in Aswan.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast, Lunch, Dinner<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 5:Kom Ombo \\/ Edfu<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Sail to Kom Ombo, have breakfast on board and Visit the Temple shared by two gods Sobek &amp; Haeroris in Kom Ombo. Sail to Edfu, lunch on board after that Visit the best-preserved cult temple in Egypt, Horus Temple in Edfu. Take afternoon tea during sailing to Esna. Dinner and overnight on board.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast, Lunch, Dinner<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 6:Luxor West Bank<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Sail to Luxor, have breakfast on board then Visit the West Bank, &nbsp;take an excursion to the royal cemetery for 62 Pharaohs, Valley of the Kings, then to mortuary Temple of Queen Hatshepsut, which was built by the architects of the New Kingdom Pharaoh Hatshepsut approximately in the 15th century BC, at El-Deir El-Bahari &amp; the Colossi of Memnon. Lunch on board. Enjoy afternoon tea during sailing. Dinner on board Overnight in Luxor.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 7:Luxor East Bank \\/ Train to Cairo<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Breakfast aboard cruise before disembarkation and then you will be accompanied by your guide to visit Karnak &amp; Luxor temples at the East bank of the River Nile and later you will be &nbsp;transferred &nbsp;to Luxor railway station to board the sleeper train back to Cairo. Have your dinner &nbsp;and overnight on board sleeper train in your private compartment ( twin beds).<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast, Dinner<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 8:Back to Cairo<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Breakfast aboard sleeper train before arriving Giza train station, where you will be met and assisted by Memphis Tours representative who will transfer you to your hotel for free time at leisure( you will check into your room as early as possible ) with some optional excursions available in Cairo and overnight stay at your hotel in Cairo<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 9:Optional Alexandria<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Free day in Cairo or optional tour to Alexandria visiting Qaitbay Citadel, built in the 15th century by Sultan Qaitbay on the spot of Alexandria&rsquo;s ancient lighthouse. Then move on to the new and much acclaimed Alexandria Library. Also visit the Roman cemetery cut out of the rock, the Catacombs. Return to Cairo and overnight.<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Meals: Breakfast<\\/strong><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><u><strong>Day 10:Final Departure<\\/strong><\\/u><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Breakfast at the hotel in Cairo - free time before your flight - check out and then you will be transferred to Cairo International Airport for final departure.<\\/strong><\\/p>\\r\\n","<p>Lake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip twoLake Nasser Trip two<\\/p>\\r\\n"]', 'Lake Nasser Trip two', 'Lake Nasser Trip two', 'Lake Nasser Trip two', 'Lake-Nasser-Trip-two', '', '', 1, '2017-03-21 11:13:51', '2017-04-20 13:15:00', NULL),
(56, 25, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-03-21 11:13:51', '2017-04-20 13:15:00', NULL),
(57, 25, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 3, '2017-03-21 11:13:51', '2017-04-20 13:15:00', NULL),
(58, 26, 'new article', '', '', 'new article', '<p>new article</p>\r\n', '', '', '', '', '', 'new-article', '', '', 1, '2017-03-21 14:46:26', '2017-03-21 14:46:26', NULL),
(59, 26, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-03-21 14:46:26', '2017-03-21 14:46:26', NULL),
(60, 26, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-03-21 14:46:26', '2017-03-21 14:46:26', NULL),
(61, 27, 'nre_art', '', '', 'nre_art', '<p>nre_art</p>\r\n', '', '', '', '', '', 'nre_art', '', '', 1, '2017-03-22 10:33:35', '2017-03-22 10:39:48', NULL),
(62, 27, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-03-22 10:33:36', '2017-03-22 10:39:48', NULL),
(63, 27, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-03-22 10:33:36', '2017-03-22 10:39:48', NULL),
(64, 28, 'المعادي حلوة', '', '', 'المعادي حلوة', '<p>المعادي حلوة</p>\r\n', '', '', '', '', '', 'المعادي-حلوة', '', '', 1, '2017-03-22 11:48:19', '2017-03-22 11:48:19', NULL),
(65, 28, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-03-22 11:48:19', '2017-03-22 11:48:19', NULL),
(66, 28, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-03-22 11:48:19', '2017-03-22 11:48:19', NULL),
(67, 29, 'Pyramids Of Giza Attractions\r\n', '', '', 'Pyramids Of Giza Attractions\r\n', '<p><strong>The Pyramids Of Giza</strong></p>\r\n\r\n<p>The most famous and popular monument in the whole world, the most astonishing landmark of Egypt, and one of the oldest buildings in history,&nbsp;<strong>the Pyramids of Giza</strong>&nbsp;are the most magnificent evidence of the greatness of the Pharaohs.&nbsp;Situated at the West Bank of the Nile in Giza, about 20 kilometers to the South West of the heart of&nbsp;Cairo, the pyramids of Giza are located on a high plateau at the end of a street that holds the same name; El Ahram or the pyramids in the Arabic language. The pyramids welcome a large number of tourists who&nbsp;visit Egypt<strong>.&nbsp;</strong>The ancient Egyptians constructed pyramids in fact to be the royal burial chambers of their kings, queens, and some of the high-ranked royal family members.&nbsp;<strong>The pyramids of Giza</strong>&nbsp;were the last successful step in a complicated process that remained for hundreds of years.&nbsp;However, why did the Pharaohs prefer the shape of the pyramid in particular? The shape of the pyramid represented the idea of the cosmogony or the origination of the world. They believed that the pyramid is the mean that takes the soul of deceased use to reach the sky and the afterlife according to the Pharaonic religious texts. This is why we observe the shape of the pyramid at the top of some obelisks and even smaller tombs in Southern Egypt.</p>\r\n\r\n<p><strong>The Grand Pyramid of Cheops</strong></p>\r\n\r\n<p>King Cheops, the second Pharaoh of the 4th dynasty of the Old Kingdom of ancient Egypt, constructed the grand pyramid of Giza in 2560 BC. The construction work of the pyramid took more than 20 years to be completed. The building of&nbsp;<strong>the Pyramid of Cheops</strong>&nbsp;still amazes the tourists who&nbsp;travel to Egypt<strong>.&nbsp;</strong><strong>The Pyramid of Cheops</strong>, or&nbsp;<strong>the Grand Pyramid</strong>&nbsp;in Giza is surly one of the most astonishing constructions in the whole world and one of the Seven Wonders of the Old World. This huge pyramid weighs approximately more than six million tons. Some of the stone that were used in its construction weighs from 3 to 25 tons. However, the pyramid was accurately built with exact measurements that you couldn&rsquo;t find even a half-millimeter mistake. Maybe this is why many travellers&nbsp;visit Egypt&nbsp;to view the greatness of&nbsp;<strong>the Pyramid of Cheops</strong>. The original height of the Grand Pyramid of Giza was 146 meters while now this remarkable monument is no higher than 138 meters. Each side of the pyramid is exactly 230.4 meters. To be accomplished&nbsp;in 20 years, the ancient Egyptian workers had to transfer, cut, and build more than 800 tons of stones every single day. The great pyramid of Giza; along with many other Pharaonic monuments attract travelers from all over the world to spend their&nbsp;vacations in Egypt.&nbsp;Explored everyday by numerous tourists who spend their&nbsp;holidays in Egypt,&nbsp;<strong>the Grand pyramid of Giza</strong>&nbsp;remained as the highest building in the whole world for more than 3800 years. The pyramid became the second highest construction in the world until the completion of the Lincoln Cathedral, built in England in the 13<sup>th</sup>&nbsp;century AD.&nbsp;The accuracy of the sides of&nbsp;<strong>the Pyramid of Cheops</strong>&nbsp;is extraordinarily amazing. The measurements of the three sides of the pyramid are: 230.252, 230.454, and 230.391. Another amazing fact about the Pyramid of King Cheops in Giza is that four sides of the pyramid are accurately parallel to the four directions of the compass; West, East, North, and South. Many travelers who&nbsp;tour Egypt&nbsp;would love to explore the wonders of&nbsp;<strong>the Pyramids of Giza</strong>.&nbsp;The original entrance to the Grand Pyramid of Giza is situated 17 meters above sea level in the Northern side of the pyramid. Above the original entrance, there are two layers of huge stones that are believed to be roofing the passageway from the entrance to the burial chamber down below.&nbsp;Tourists who&nbsp;travel to Egypt&nbsp;enter the pyramid today through a tunnel that was established by the Fatimid Caliph, El Ma&rsquo;moun, in 820 AD. This tunnel enters inside the pyramid horizontally for 27 meters until it meets the ascending passageway inside the pyramid.</p>\r\n\r\n<p>Some of the travelers who&nbsp;tour Egypt&nbsp;would like to go on an even wilder adventure by entering inside&nbsp;<strong>the Grand pyramid of Giza</strong>. They would walk for 27 meters in the passageway constructed By El Ma&rsquo;moun and then walk again for around 37 meters to reach the royal burial chamber of King Cheops. This chamber is around 10 square meters with a roof that is around 6 meters in height. Many of the passageways and the chambers inside the grand pyramid of Cheops in Giza are still undeclared by hundreds of scientists and historians who studied the pyramid.&nbsp;Other than the pyramid itself, each mortuary complex in Giza has a Valley Temple, a mortuary Temple, and corridor that links all these components together. The only remaining item of the mortuary temple of&nbsp;<strong>the Pyramid of Cheops</strong>&nbsp;is the basalt stones&rsquo; ground.</p>\r\n\r\n<p><strong>The Pyramid of Khafre or Chephren</strong></p>\r\n\r\n<p>The second largest pyramid in Giza was the second to be constructed in this ancient necropolis. King Khafre who ruled Egypt from 2558 till 2532 BC established this pyramid. The Pyramid of Khafre, which still preserves some of its outer cast, is 143 meters in height and each of its sides is 215.5 meters long. Travelers who&nbsp;visit Egypt&nbsp;love exploring ancient monuments and pyramids while they are in the land of the Pharaohs.</p>\r\n\r\n<p>The Mortuary Temple of&nbsp;<strong>the Pyramid of Khafre</strong>&nbsp;is located to the East of the pyramid. Historical records assert that this has been a magnificent construction that was made out of limestone and granite rocks. On the other hand,&nbsp;<strong>the Valley Temple</strong>&nbsp;consists of a passageway that leads to a large open courtyard that has 16 pillars with each of them about 4 meters in height.&nbsp;</p>\r\n\r\n<p><strong>The Pyramid of Menkaure</strong></p>\r\n\r\n<p>The smallest pyramid in Giza is that of Menkaure that ruled Egypt from 2532 until 2502 BC. The pyramid is only 66 meters in height and the base is around 109 meters. The first 16 layers of stones in the pyramid of Menkaure were covered with blocks of light pink rocks. &nbsp;&nbsp;</p>\r\n\r\n<p><strong>The Sphinx</strong></p>\r\n\r\n<p>Among the most amazing elements to be viewed by travelers who spend their&nbsp;tours in Egypt&nbsp;while visiting the pyramids of Giza is the astonishing sphinx. This huge statue of a lion with a human head has defended the pyramid for years.&nbsp;Made out of limestone, the sphinx was constructed in the reign of king Khafre. The sphinx is 20 feet in its length and 70 feet in its height. These dimensions make it one of the largest statues in the entire globe.</p>\r\n', '', '', '', '', '', 'Pyramids-Of-Giza-Attractions', '', '', 1, '2017-03-22 15:57:29', '2017-03-22 15:57:29', NULL),
(68, 30, 'The Egyptian Museum of Antiquities Attractions\r\n', '', '', 'The Egyptian Museum of Antiquities Attractions\r\n', '<p>The Egyptian Museum of Antiquities Attractions<br />\r\n&nbsp;</p>\r\n', '', '', '', '', '', 'The-Egyptian-Museum-of-Antiquities-Attractions', '', '', 1, '2017-03-22 16:05:07', '2017-03-22 16:05:07', NULL),
(69, 31, 'Luxury Trip one', 'cairo', '15 days', 'Luxury Trip oneLuxury Trip oneLuxury Trip one', '<p><u>&nbsp;<strong>Day 1:Arrival Cairo</strong></u></p>\r\n\r\n<p><strong>Welcome to Cairo, Egypt&nbsp;</strong></p>\r\n\r\n<p><strong>&nbsp;</strong></p>\r\n\r\n<p><strong>Your tour manager will meet and assist you at Cairo International Airport (Arrival procedures) and then he will escort you to the hotel by exclusive air-conditioned deluxe vehicle. At hotel the tour manager will assist with a smooth check-in and review your holiday itinerary with you to establish and confirm pick-up times for each tour. Overnight in Cairo.</strong></p>\r\n\r\n<p><strong>Welcome Drink</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 2:Pyramids / Sakkara</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast at your hotel in Cairo and then met by your personal guide who will accompany you to Giza Plateau to visit the Great Pyramids of Cheops, Chefren and Mykerinus - Famous Sphinx and Valley temple facing the great statue. Additional visit to the Solar Boat infront of Cheops Pyramid (extra ticket on spot) Lunch included during the tour and then proceed to Sakkara area to visit Sakkara Complex and first pyramid ever built (Djoser Pyramid). Move to Memphis; Old kingdom&#39;s capital and famous Necropolis. Overnight in Cairo.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 3:Cairo Sightseeing / Train to Aswan</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Check out after breakfast at your hotel in Cairo and and then you will be escorted by your private guide to visit: Egyptian Museum, Treasures room for the child king Tutankhamen, alongside many other fascinating artifacts. Additional visit to the mummies room at the museum (extra ticket on spot) Lunch through out the tour at local restaurant, then moving to visit Saladin Citadel including Mohamed Ali Alabaster Mosque inside. Continue to Coptic Cairo to visit the Hanging Church and Ben Ezra Synagogue. Continue driving &nbsp;to explore Khan El Khalili, Cairo&rsquo;s old bazaar. Later, you will be transferred to Giza railway station to board the sleeper train to Aswan. Dinner and breakfast will be served on board - overnight inside your compartment.</strong></p>\r\n\r\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 4:Embark/ Aswan Sightseeing</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>You will be met and assisted at Aswan station by our representative, embark your Nile River Cruise , before Lunch at 11:00 a.m, then enjoy visiting the world famous High Dam, The awesome Philae temple, &nbsp;which is devoted to the two goddesses Isis and Hathor , and &nbsp;the largest known ancient obelisk, located in the northern region of the stone quarries of ancient Egypt, the Unfinished Obelisk. Dinner on board and overnight in Aswan.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 5:Kom Ombo / Edfu</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sail to Kom Ombo, have breakfast on board and Visit the Temple shared by two gods Sobek &amp; Haeroris in Kom Ombo. Sail to Edfu, lunch on board after that Visit the best-preserved cult temple in Egypt, Horus Temple in Edfu. Take afternoon tea during sailing to Esna. Dinner and overnight on board.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 6:Luxor West Bank</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sail to Luxor, have breakfast on board then Visit the West Bank, &nbsp;take an excursion to the royal cemetery for 62 Pharaohs, Valley of the Kings, then to mortuary Temple of Queen Hatshepsut, which was built by the architects of the New Kingdom Pharaoh Hatshepsut approximately in the 15th century BC, at El-Deir El-Bahari &amp; the Colossi of Memnon. Lunch on board. Enjoy afternoon tea during sailing. Dinner on board Overnight in Luxor.</strong></p>\r\n\r\n<p><strong>Meals: &nbsp;Breakfast, Lunch, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 7:Luxor East Bank / Train to Cairo</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast aboard cruise before disembarkation and then you will be accompanied by your guide to visit Karnak &amp; Luxor temples at the East bank of the River Nile and later you will be &nbsp;transferred &nbsp;to Luxor railway station to board the sleeper train back to Cairo. Have your dinner &nbsp;and overnight on board sleeper train in your private compartment ( twin beds).</strong></p>\r\n\r\n<p><strong>Meals: Breakfast, Dinner</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 8:Back to Cairo</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast aboard sleeper train before arriving Giza train station, where you will be met and assisted by Memphis Tours representative who will transfer you to your hotel for free time at leisure( you will check into your room as early as possible ) with some optional excursions available in Cairo and overnight stay at your hotel in Cairo</strong></p>\r\n\r\n<p><strong>Meals: Breakfast</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 9:Optional Alexandria</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Free day in Cairo or optional tour to Alexandria visiting Qaitbay Citadel, built in the 15th century by Sultan Qaitbay on the spot of Alexandria&rsquo;s ancient lighthouse. Then move on to the new and much acclaimed Alexandria Library. Also visit the Roman cemetery cut out of the rock, the Catacombs. Return to Cairo and overnight.</strong></p>\r\n\r\n<p><strong>Meals: Breakfast</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Day 10:Final Departure</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Breakfast at the hotel in Cairo - free time before your flight - check out and then you will be transferred to Cairo International Airport for final departure.</strong></p>\r\n', '["Exclusions:"]', '["<p>Luxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip oneLuxury Trip one<\\/p>\\r\\n"]', 'Luxury-Trip-one', 'Luxury-Trip-one', 'Luxury-Trip-one', 'Luxury-Trip-one', '', '', 1, '2017-03-22 16:05:58', '2017-04-20 14:56:49', NULL),
(70, 32, 'Luxury Trip two', 'egypt luxur', '10 days', 'Luxury Trip twoLuxury Trip twoLuxury Trip two', '<p>Luxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip two</p>\r\n', '["Inclusions :","Exclusions:"]', '["<p>Luxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip two<\\/p>\\r\\n","<p>Luxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip twoLuxury Trip two<\\/p>\\r\\n"]', '', '', '', 'Luxury-Trip-two', '', '', 1, '2017-03-22 16:08:15', '2017-03-22 16:08:15', NULL),
(71, 33, 'Karnak Temple Attractions\r\n', '', '', 'Karnak Temple Attractions\r\n', '<p><strong>The Karnak Temple Attractions</strong></p>\r\n\r\n<p><strong>The Karnak Temple</strong>&nbsp;is the largest ancient Egyptian construction in the whole world. The Temple, that was mainly dedicated to the worship of the god Amun, the king of gods of ancient Egypt, is one of the most important, or perhaps the most significant touristic attraction of the East Bank of&nbsp;<strong>the city of Luxor</strong>&nbsp;included in many of the Egypt travel packages<strong>.</strong></p>\r\n\r\n<p><strong>The Temple of Karnak</strong>&nbsp;with all its huge pillars, various sections, huge statues, and complexity, and large sacred lake makes it an overwhelming complex constructed over more than 1000 years in ancient Egypt. Today&nbsp;<strong>the Temple of Karnak</strong>&nbsp;is highlight of&nbsp;<strong>the city of Luxor</strong>&nbsp;explored by hundreds of tourists who<strong>&nbsp;<a href="http://www.maestroegypttours.com/Egypt-Nile-Cruises">travel to Egypt</a>.</strong></p>\r\n\r\n<p>The construction process with each Pharaoh and ancient Egyptian king adding his contribution to&nbsp;<strong>the Temple of Karnak</strong>&nbsp;started in the reign of the Middle Kingdom in the 21<sup>st</sup>&nbsp;century BC and it remained until the end of the Ptolemaic period in Egypt. This means it took more than 2000 years to be completed.&nbsp;<strong>The Temple of Luxor</strong>&nbsp;is the heart of the East Bank of the city of Luxor. The temple is located only three kilometers to the North of<strong>&nbsp;the Temple of Luxor</strong>&nbsp;that was once linked to it by the famous avenue of sphinxes. The wall of&nbsp;<strong>the Temple of Karnak</strong>&nbsp;was made out of mud bricks with its length being more than 500 meters and its width around 480 meters. The Temple in ancient times had eight huge gates.</p>\r\n\r\n<p>It was mentioned in historical records that during the reign of the 19<sup>th</sup>&nbsp;dynasty in the 13<sup>th</sup>&nbsp;century BC, more than 80,000 people worked in&nbsp;<strong>the Karnak Temple</strong>. Today many tourists who travel to Egypt are keen to explore such ancient constructions in Egypt.&nbsp;The first ancient Egyptian king who thought about establishing a temple for the worship of the god Amun at<strong>&nbsp;the Karnak</strong>&nbsp;was Amenhotep I at the middle of the 16th century BC. He chose this particular location as the ruins of other religious structures built in the Middle Kingdom and occupied the area. However, the king died before achieving his goal. His ancestor, Tuthmosis I was the one to be credited with the first establishment in the Karnak complex.</p>\r\n\r\n<p>The guests enjoying their vacations in Egypt enter&nbsp;<strong>the Karnak Temple</strong>&nbsp;after they walk in&nbsp;<strong>the Avenue of Sphinxes</strong>. This long rout that once connected&nbsp;<strong>the Luxor Temple</strong>&nbsp;with the Karnak Temple used to host more than 1200 statues of sphinxes that are featured with the body of a lion and the head of a human.&nbsp;Ramses III constructed a temple to the South East of this pylon and it was dedicated to the worship of the divine trinity of Thebes, Amun, Khunsu, and Mut. After this relatively small temple, there is the pre style hall that was open for all the public in ancient times; afterwards, there was the hypostyle hall that was specified for the coronation of the various kings of ancient Egypt. At the very end, there was the sanctuary of the temple where the highest priests of Amun were only allowed to enter.</p>\r\n\r\n<p>Among the most marvelous sections of&nbsp;<strong>the Temple of Karnak</strong>&nbsp;is the hypostyle hall that is featured with its huge pillars. King Seti I established this section in the very beginning. With a surface area of more than 500 meters, many kings and Pharaohs had their contributions of huge pillars to the hypostyle hall. This section always amazes travelers who spend their vacations in Egypt.</p>\r\n\r\n<p>The most wonderful set of huge pillars is that which as constructed by Ramses II. These huge columns are fascinating in its magnitude and its height and it is considered to be the largest and most remarkable construction of its type in the whole world. These types of constructions attract travelers from various regions in the globe to spend their&nbsp;<strong><a href="http://www.maestroegypttours.com/Egypt-Travel-Packages/Honeymoon/Egypt-Honeymoon">holidays in Egypt.</a></strong>&nbsp;The hypostyle hall of&nbsp;<strong>the Temple of Karnak</strong>&nbsp;is 52 meters in length and its width is more than 100 meters. There are 134 huge pillars that that are around 20 meters in height, their diameter is more than 3 meters, and their width is more than 10 meters. Ramses II, Seti I, and Tuthmosis III constructed most of the pillars we see today.</p>\r\n\r\n<p>One of the remarkable sections of&nbsp;<strong>the Karnak Temple</strong>&nbsp;is the chapel of King Senusert I. This chapel was damaged in different sections of history but it was rebuilt in 1936. Due to its construction out of white limestone, this section was called the white chapel that is featured with many scenes of different religious rituals of the cult of the god Amun.&nbsp;There is also the chapel of King Amenhotep I that is featured with scenes showing the king presenting offerings to the god Amun and many other portraits of religious practices and rituals. Although smaller than that of Senusert I, the chapel of Amenhotep is never less impressive.</p>\r\n\r\n<p>Another interesting feature of the Temple of Karnak is the famous obelisk of&nbsp;<strong>Queen Hatshepsut</strong>&nbsp;that is around 30 meters in height. It was made out of red granite and it weighs more than 320 tons. Queen Hatshepsut in fact constructed two obelisks at&nbsp;<strong>the Karnak Temple</strong>.&nbsp;However, When the ancestor of Hatshepsut, King Tuthmosis III became the king of Egypt and due to his hatred of the queen, he surrounded one of her obelisks with huge walls and it was damaged afterwards except for its top. Obelisks are among the most interesting features of the Pharaohs that grab the attention of many travelers who tour Egypt<strong>.&nbsp;</strong>The guests enjoying their&nbsp;<strong><a href="http://www.maestroegypttours.com/Excursions/Luxor-Day-Tours/Tour-To-The-East-And-West-Bank-in-Luxor">trips in Egypt</a></strong>&nbsp;pass by many pylons and various sections of&nbsp;<strong>the Karnak Temple</strong>&nbsp;until they reach the Sacred Lake. This large lake was used for ablution means and it is one of a kind in Egypt due to its size. Located beside the sacred lake, there is the famous scarab statue that was built in the reign of Tuthmosis III out of granite. The scarab was placed on a huge base that has some scenes of the kings kneeling in respect for the god Amun.&nbsp;Guests who visit&nbsp;<strong>the Karnak Temple</strong>&nbsp;would see some tourists who visit Egypt moving in circles around the scarab, as there is an ancient myth that says this would bring them the best luck in their lives. However, of course, there is no historical evidence behind this ritual.</p>\r\n', '', '', '', '', '', 'Karnak-Temple-Attractions', '', '', 1, '2017-03-22 16:08:45', '2017-03-22 16:08:45', NULL),
(72, 34, 'Aswan Day Tour Trip one', 'egypt', '10 days', 'Aswan Day Tour Trip oneAswan Day Tour Trip one', '<p>Aswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip one</p>\r\n', '["Inclusions :","Exclusions :"]', '["<p>Aswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip one<\\/p>\\r\\n","<p>Aswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip oneAswan Day Tour Trip one<\\/p>\\r\\n"]', 'Aswan Day Tour Trip one', 'Aswan Day Tour Trip one', 'Aswan Day Tour Trip one', 'Aswan-Day-Tour-Trip-one', '["1","2","3","4"]', '["5","6","7","8"]', 1, '2017-03-22 16:09:49', '2017-05-09 08:41:40', NULL),
(73, 35, 'Aswan Day Tour Trip two', 'cairo', '20days', 'Aswan Day Tour Trip twoAswan Day Tour Trip two', '<p>Aswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip two</p>\r\n', '["Inclusions :","Exclusions :"]', '["<p>Aswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip two<\\/p>\\r\\n","<p>Aswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip twoAswan Day Tour Trip two<\\/p>\\r\\n"]', 'Aswan', 'Aswan', 'Aswan', 'Aswan-Day-Tour-Trip-two', '', '', 1, '2017-03-22 16:10:52', '2017-04-23 11:31:46', NULL);
INSERT INTO `pages_translate` (`id`, `page_id`, `page_title`, `page_city`, `page_period`, `page_short_desc`, `page_body`, `page_header_arr`, `page_body_arr`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`, `page_slug`, `included_services_and_amenities`, `excluded_services_and_amenities`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(74, 36, 'Luxur Day Tour Trip one', 'cairo', '20days', 'Luxur Day Tour Trip one Luxur Day Tour Trip one', '<p>Luxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip one</p>\r\n', '["Inclusions :","Exclusions :"]', '["<p>Luxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip one<\\/p>\\r\\n","<p>Luxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip oneLuxur Day Tour Trip one Luxur Day Tour Trip one<\\/p>\\r\\n"]', '', '', '', 'Luxur-Day-Tour-Trip-one', '', '', 1, '2017-03-22 16:12:40', '2017-03-22 16:12:40', NULL),
(75, 37, 'Luxur Day Tour Trip two', 'cairo', '20days', 'Luxur Day Tour Trip two Luxur Day Tour Trip two', '<p>Luxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip two</p>\r\n', '["Inclusions :"]', '["<p>Luxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip twoLuxur Day Tour Trip two Luxur Day Tour Trip two<\\/p>\\r\\n"]', '', '', '', 'Luxur-Day-Tour-Trip-two', '', '', 1, '2017-03-22 16:13:34', '2017-03-22 16:13:34', NULL),
(76, 38, 'Lake Nasser Cruise Trip one', 'cairo', '20days', 'Lake Nasser Cruise Trip one Lake Nasser Cruise Trip one', '<p>Lake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip one</p>\r\n', '["Inclusions :"]', '["<p>Lake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip oneLake Nasser Cruise Trip one Lake Nasser Cruise Trip one<\\/p>\\r\\n"]', '', '', '', 'Lake-Nasser-Cruise-Trip-one', '', '', 1, '2017-03-22 16:15:32', '2017-03-22 16:15:32', NULL),
(77, 39, 'Lake Nasser Cruise Trip two', 'cairo', '20days', 'Lake Nasser Cruise Trip two Lake Nasser Cruise Trip two', '<p>Lake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip two</p>\r\n', '["Inclusions :"]', '["<p>Lake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip twoLake Nasser Cruise Trip two Lake Nasser Cruise Trip two<\\/p>\\r\\n"]', '', '', '', 'Lake-Nasser-Cruise-Trip-two', '', '', 1, '2017-03-22 16:16:22', '2017-03-22 16:16:22', NULL),
(78, 40, 'Nile Cruise Luxur Aswan Trip one', 'cairo', '20days', 'Nile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip one', '<p>Nile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip one</p>\r\n', '["Inclusions :"]', '["<p>Nile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip oneNile Cruise Luxur Aswan Trip one Nile Cruise Luxur Aswan Trip one<\\/p>\\r\\n"]', '', '', '', 'Nile-Cruise-Luxur-Aswan-Trip-one', '', '', 1, '2017-03-22 16:17:22', '2017-03-22 16:17:22', NULL),
(79, 41, 'Nile Cruise Luxur Aswan Trip two', 'cairo', '20days', 'Nile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip two', '<p>Nile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip two</p>\r\n', '["Inclusions :"]', '["<p>Nile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip twoNile Cruise Luxur Aswan Trip two Nile Cruise Luxur Aswan Trip two<\\/p>\\r\\n"]', '', '', '', 'Nile-Cruise-Luxur-Aswan-Trip-two', '', '', 1, '2017-03-22 16:18:42', '2017-03-22 16:18:42', NULL),
(80, 42, 'Eiffel Tower', '', '', 'The Eiffel Tower is a wrought iron lattice tower on the Champ de Mars in Paris, France. It is named after the engineer Gustave Eiffel, whose company designed and built the tower', '<p>The Eiffel Tower is a wrought iron lattice tower on the Champ de Mars in Paris, France. It is named after the engineer Gustave Eiffel, whose company designed and built the tower</p>\r\n', '', '', 'Eiffel Tower', 'Eiffel Tower', 'Eiffel Tower', 'Eiffel-Tower', '', '', 1, '2017-03-22 16:23:28', '2017-03-22 16:23:28', NULL),
(81, 43, 'The Louvre\r\n', '', '', 'The Louvre or the Louvre Museum is the world''s largest museum and a historic monument in Paris, France. A central landmark of the city, it is located on the Right Bank of the Seine in the city''s 1st arrondissement', '<p>The Louvre or the Louvre Museum is the world&#39;s largest museum and a historic monument in Paris, France. A central landmark of the city, it is located on the Right Bank of the Seine in the city&#39;s 1st arrondissement</p>\r\n', '', '', 'The Louvre', 'The Louvre\r\n', 'The Louvre\r\n', 'The-Louvre', '', '', 1, '2017-03-22 16:25:20', '2017-03-22 16:25:20', NULL),
(82, 44, 'New Trip Name Test', 'Cairo, Luxor, Aswan', '12 Days / 11 Nights', 'Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. Short Des. ', '<p>Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;Desc&nbsp;</p>\r\n', '["Itinerary","Inclusions","Exclusions"]', '["<p><strong>Day 1: day one here<\\/strong><\\/p>\\r\\n\\r\\n<p>New Trip Name Test&nbsp;New Trip Name Test&nbsp;New Trip Name Test&nbsp;New Trip Name Test<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Day<\\/strong> :<\\/p>\\r\\n","<p>New Trip Name Test<\\/p>\\r\\n","<p>New Trip Name Test<\\/p>\\r\\n"]', 'New Trip Name Test ', 'New Trip Name Test ', 'New Trip Name Test', 'New-Trip-Name-Test', '', '', 1, '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(83, 44, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-04-03 10:18:08', '2017-04-03 10:18:08', NULL),
(84, 45, 'Article Test', '', '', 'Article Test Article Test Article Test', '<p>Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;Article Test&nbsp;</p>\r\n', '', '', 'Article Test ', 'Article Test ', 'Article Test ', 'Article-Test', '', '', 1, '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(85, 45, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-04-03 10:40:51', '2017-04-03 10:40:51', NULL),
(86, 46, 'Pyramids', '', '', 'PyramidsPyramids', '<p>Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;</p>\r\n', '', '', 'pyramids', 'pyramids', 'pyramids', '-', '', '', 1, '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(87, 46, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(88, 46, '', '', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-04-13 13:08:57', '2017-04-13 13:08:57', NULL),
(89, 31, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-04-20 14:51:03', '2017-04-20 14:56:49', NULL),
(90, 31, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 3, '2017-04-20 14:51:03', '2017-04-20 14:56:49', NULL),
(91, 47, 'عن الموقع', '', '', 'About Us', '<p>Since the very early beginning we have been committed to offer all our clientes the best quality of services and to provide them with all solutions and facilities to meet with all their requirements.</p>\r\n\r\n<p dir="RTL"><span dir="LTR">&nbsp;As we believe that man power is the core of every success and is the most important asset for any business, so our target has always been recruiting the most efficient employees who are bilingual &amp; high calibers and very well qualified. Our know-how and willingness to attend and serve our clients and to help them to tailor-make their packages and to get them the best available &amp; competitive prices, have led us to be of a great credibility in the market...</span></p>\r\n\r\n<p dir="RTL"><span dir="LTR">&nbsp;Our product is carefully selected and covers all the most important attractive sites in Egypt&nbsp;as well as the new featured sites converted into new ideas and itineraries .Nevertheless, we are also specialized in Outbound Tourism so we offer flexible solutions&nbsp;to combine Egypt with other destinations in the Middle East, Europe, Far East and&nbsp;Africa&nbsp;</span><br />\r\n&nbsp;</p>\r\n\r\n<p dir="RTL"><span dir="LTR">We also believe in the importance of having our vision and we have chosen to be distinguished in the field of both Incoming &amp; Outgoing Tourism and always committed ourselves to quality to ensure the best value for money and cost effectiveness to all our&nbsp;</span></p>\r\n', '', '', 'عن الموقع', 'عن الموقع', 'عن الموقع', 'عن-الموقع_47', '', '', 1, '2017-04-23 10:48:25', '2017-11-06 20:25:08', NULL),
(92, 47, '', '', '', '', '', '', '', '', '', '', '', '', '', 2, '2017-04-23 10:48:25', '2017-05-06 21:43:29', NULL),
(93, 47, 'Sobre Nosotros', '', '', 'Sobre Nosotros', '<p>Since the very early beginning we have been committed to offer all our clientes the best quality of services and to provide them with all solutions and facilities to meet with all their requirements.</p>\r\n\r\n<p dir="RTL"><span dir="LTR">&nbsp;As we believe that man power is the core of every success and is the most important asset for any business, so our target has always been recruiting the most efficient employees who are bilingual &amp; high calibers and very well qualified. Our know-how and willingness to attend and serve our clients and to help them to tailor-make their packages and to get them the best available &amp; competitive prices, have led us to be of a great credibility in the market...</span></p>\r\n\r\n<p dir="RTL"><span dir="LTR">&nbsp;Our product is carefully selected and covers all the most important attractive sites in Egypt&nbsp;as well as the new featured sites converted into new ideas and itineraries .Nevertheless, we are also specialized in Outbound Tourism so we offer flexible solutions&nbsp;to combine Egypt with other destinations in the Middle East, Europe, Far East and&nbsp;Africa&nbsp;</span><br />\r\n&nbsp;</p>\r\n\r\n<p dir="RTL"><span dir="LTR">We also believe in the importance of having our vision and we have chosen to be distinguished in the field of both Incoming &amp; Outgoing Tourism and always committed ourselves to quality to ensure the best value for money and cost effectiveness to all our&nbsp;</span></p>\r\n', '', '', 'Sobre Nosotros', 'Sobre Nosotros', 'Sobre Nosotros', 'Sobre-Nosotros', '', '', 3, '2017-04-23 10:48:25', '2017-05-06 21:43:29', NULL),
(94, 35, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-04-23 11:31:46', '2017-04-23 11:31:46', NULL),
(95, 35, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 3, '2017-04-23 11:31:46', '2017-04-23 11:31:46', NULL),
(96, 48, 'mahdy', 'aswan', 'mahdy', 'mahdyyyyy', '<p>mahdyyyyy</p>\r\n', '["mahdyyyyy"]', '["<p>mahdyyyyy<\\/p>\\r\\n"]', 'mahdyyyyy', 'mahdyyyyy', 'mahdyyyyy', 'mahdy', '', '', 1, '2017-05-06 22:48:23', '2017-05-07 13:25:33', NULL),
(97, 48, '', '', '', '', '', '[]', '[]', '', '', '', '', '', '', 2, '2017-05-06 22:48:23', '2017-05-07 13:25:33', NULL),
(98, 48, 'mahdyz', 'aswan', 'mahdy', 'mahdyz', '<div>\r\n<p><strong><u>DIA 1. EL CAIRO.</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Llegada a El Cairo. Tramites de entrada a Egipto y traslado con asistencia del personal de habla hispana de EGYPT TRAVEL CORNER al hotel seleccionado. Alojamiento en</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>el Palacio Hotel Mena</u></strong>&nbsp;<strong>House construido originalmente como un pabell&oacute;n de caza en</strong>&nbsp;<strong>1869 por el rey Ismail, abri&oacute; al p&uacute;blico como hotel en 1886 bajo el nombre de The Mena House, todav&iacute;a en los albores de la egiptoman&iacute;a, El Palacete Mena fue edificado para alojar a reyes,empereadores y presidentes que llegaban a Egipto, desde sus balcones pueden observarse las pir&aacute;mides de Giza ,adem&aacute;s de sus 16 hect&aacute;reas de jardines arom&aacute;ticos, plantados con jazmines.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>En &eacute;l se han alojado primeras personalidades del siglo XX como varios pr&iacute;ncipes de Gales y duques de Windsor, el General Montgomery, Winston Churchill, Roosevelt, Nixon, Chiang Kai Chek, Jimmy Carter, Sadat, Menachem Beguin, Rezah Pahlavi, el Aga Khan; artistas de la talla de Arthur Conan Doyle, Cecil B. DeMille, Frank Sinatra y Agatha Christie; actores y actrices c&eacute;lebres, desde Jane Fonda a Roger Moore, pasando por Charlton Heston, Chaplin y Omar Sharif. Si&eacute;ntete como una personalidad de este siglo,</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>O alojamiento&nbsp;<u>en el Palacio Hotel Cairo Marriot</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Este hotel est&aacute; formado por dos torres y un palacio que data de 1869. Este sofisticado y lujoso 5 estrellas est&aacute; situado en pleno centro del Cairo, a unos 15 minutos a pie del Museo Egipcio. El palacio est&aacute; lleno de historia, en &eacute;l se alojaban el emperador de Francia Napole&oacute;n III y su esposa Eugenia de Montijo. El palacio (Gezira Palace) fue construido bajo el mandato de Khedive Ismail para acoger a los arist&oacute;cratas y tambi&eacute;n con motivo de la inauguraci&oacute;n del canal de Suez. Este palacio est&aacute; lleno de historia. Sin duda sus hu&eacute;spedes m&aacute;s emblem&aacute;ticos han sido el emperador de Francia Napole&oacute;n III y su esposa Eugenia de Montijo. La decoraci&oacute;n de los interiores del edificio fue en su d&iacute;a remodelada seg&uacute;n los gustos de los emperadores para que &eacute;stos se sintiesen como en casa. El edificio est&aacute; decorado con gran gusto y entre el mobiliario encontramos obras art&iacute;sticas originales.</strong></p>\r\n</div>\r\n\r\n<div>\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 2. EL CAIRO.</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Estancia en el hotel en r&eacute;gimen de alojamiento y desayuno. Ma&ntilde;ana dedicada a realizar la visita de las Pir&aacute;mides de Giza , una de las siete maravillas del mundo Antiguo y la Esfinge de Giza que es una combinaci&oacute;n entre la fuerza f&iacute;sica del le&oacute;n y la mental del ser humano. Tambien visitaremos el Templo del Valle de Kefren junto al lado de la esfinge. Resto de tarde libre. Posibilidad de realizar una excursi&oacute;n opcional a Sakkara, donde se encuentra la Piramide Escalonada y Menphis, la primera capital del Egipto Unido.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 3. EL CAIRO / LUXOR</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Desayuno en el hotel y ma&ntilde;ana libre para disfrutar de tan animada y cosmopolita ciudad, donde podr&aacute;n visitar el Museo Arqueol&oacute;gico del Cairo, el m&aacute;s grande del mundo de antig&uuml;edades de la &eacute;poca fara&oacute;nica; barrio copto, el barrio ortodoxo con el sabor del Egipto cristiano, donde la historia cuenta que estuvo la Sagrada Familia; la Ciudadela de Saladino, dentro encontramos la famosa Mezquita de Alabastro y para terminar el d&iacute;a disfrutar del animado y colorido bazar de Khan Khalili, con sus animadas calles, tiendas. A la hora prevista, traslado al aeropuerto para salir en vuelo domestico hacia Luxor. Llegada y traslado al Hotel Winter Palace. Alojamiento.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>El Hotel Winter Palace Luxor</u></strong>,&nbsp;<strong>es un hotel de lujo de 5 estrellas construido en 1886 por</strong>&nbsp;<strong>exploradores brit&aacute;nicos y ubicado junto al r&iacute;o Nilo entre exuberantes jardines tropicales y ancestrales templos.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Este opulento hotel de Luxor combina un rico dise&ntilde;o colonial con los gloriosos tiempos fara&oacute;nicos. En este palacio del s. XIX fue la morada de invierno de la familia real egipcia y Agatha Christie escribi&oacute; aqu&iacute; su novela &quot;Muerte en el Nilo&quot; en 1937.Ilustres dignatarios, desde presidentes y pol&iacute;ticos hasta artistas y personalidades de fama internacional se han alojado en las suntuosas suites de este hotel de Luxor con impresionantes vistas al valle de los Reyes.El arque&oacute;logo Howard Carter, descubridor de la tumba de Tutankam&oacute;n, paseaba por los jardines de este suntuoso hotel de Luxor hace casi un siglo.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 4. LUXOR</u></strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Estancia en el hotel en r&eacute;gimen de alojamiento y desayuno.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>D&iacute;a dedicado a la visita de la Necr&oacute;polis de Tebas en la orilla oeste del Nilo que, contiene el Valle de los Reyes con las tumbas de los faraones del Imperio Nuevo.A continuaci&oacute;n, visita del Templo de la reina Hatchepsut, templo de estilo revolucionario por sus tres alturas unidas por dos rampas, seguiremos hacia los Colosos de Memnon aislados en la puerta del templo funerario de Amenofis III, lo &uacute;nico que se conserva del templo, el resto fue destruido por el tiempo.</strong></p>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 5. LUXOR / ESNA / EDFU / KOM OMBO /ASWAN</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Desayuno en el hotel. Inicio de nuestra visita al Templo de Edfu templo mejor conservado de Egipto y el m&aacute;s importante despu&eacute;s del templo de Karnak. Continuamos nuestra visita hacia Kom Ombo para visitar a pie el Templo dedicado a los dioses Sobek y Haroeris. Tras la misma salida hacia la ciudad de Aswan. Alojamiento</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>en&nbsp;<u>el Hotel Old Cataract</u></strong>,<strong>participe en la leyenda de un lujoso hotel de 5 estrellas que se asienta en un palacio victoriano del siglo XIX a orillas del r&iacute;o Nilo.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Con una grandiosa ubicaci&oacute;n sobre una formaci&oacute;n de granito rosa en el desierto de Nubia y fant&aacute;sticas vistas a la Isla Elefantina, este elegante hotel combina los tesoros fara&oacute;nicos con el arte de la hospitalidad francesa, sofisticados interiores dise&ntilde;ados por Sybille de Margerie en este hist&oacute;rico palacio con arcos moriscos, l&aacute;mparas de ara&ntilde;a en color rojo, suaves alfombras persas, mullidos sillones y muebles tallados a mano. Al mismo tiempo podr&aacute; adentrarse en el Egipto del siglo XXI en las alas Nilo , que ofrecen una moderna interpretaci&oacute;n de la cultura tradicional.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Rel&aacute;jese en nuestra piscina sin borde con vistas al Nilo, organice un recorrido en fal&uacute;ca al atardecer, un masaje con piedras calientes en el So SPA,y mucho m&aacute;s.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 6. ASWAN.</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Desayuno en el hotel .Paseo en fal&uacute;ca por el Nilo para admirar desde la embarcaci&oacute;n una vista panor&aacute;mica del Mausoleo de Agha Khan, la Isla Elefantina y el Jard&iacute;n Bot&aacute;nico. Continuaremos con la visita de la presa de Aswan. Resto de la tarde libre. Alojamiento</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 7. ASWAN / EL CAIRO</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Desayuno en el hotel. Antes de coger el vuelo con destino a El Cairo, realizaremos la excursi&oacute;n a los templos de Abu Simbel, complejo de templos excavados en la roca. Traslado al aeropuerto para salir hacia El Cairo. Llegada y traslado al hotel. Alojamiento.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>DIA 8. EL CAIRO / AEROPUERTO</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Desayuno en el hotel y a la hora prevista traslado al aeropuerto, tramites de facturaci&oacute;n y embarque.</strong></p>\r\n', '["mahdyz","tesssst"]', '["<p>tesssst<\\/p>\\r\\n","<p>tesssst<\\/p>\\r\\n"]', 'mahdyz', 'mahdyz', 'mahdyz', 'mahdy', '', '', 3, '2017-05-06 22:48:23', '2017-05-07 13:25:33', NULL),
(99, 34, '', '', '', '', '', '', '', '', '', '', '', '[]', '[]', 2, '2017-05-09 08:41:40', '2017-05-09 08:41:40', NULL),
(100, 34, '', '', '', '', '', '', '', '', '', '', '', '[]', '[]', 3, '2017-05-09 08:41:40', '2017-05-09 08:41:40', NULL),
(101, 49, 'فني صحي', '', '', '', '<h2 align="center">افضل خدمات&nbsp;<strong>فني صحي</strong>&nbsp;بالكويت وتسليك مجاري وسباك صحي و ادوات صحية نغطي جميع مناطق الكويت ونعمل 24 ساعة ويسعدنا الرد على استفسارتكم طول اليوم ويوجد كفالة على جميع الاعمال والتركيبات</h2>\r\n\r\n<h3 align="center">: خدماتنا</h3>\r\n\r\n<h3 align="center">تسليك مجاري - ادوات صحية - سباك -&nbsp;<strong><em>فني صحي</em></strong>&nbsp;- فني صحي الكويت</h3>\r\n\r\n<h3 align="center">فنى صحى سباك ماذا تعنى كلمة فنى صحى وماذا تعنى كلمة سباك صحى وفى هذا المقال نقول لكم ان الشخص المنوط بتركيب و إصلاح أنظمة الأنابيب او المواسير ومواسير المياه والخزانات وطلمبات المياه و تركيبات السباكة مثل سخانات المياه وتعتبر مهنة السباكة جزءاً أساسياً لا يمكن الإستغناء عنه في أساسيات أي مبني سواء كانت منشأت &ndash; فلل &ndash; عقارات &ndash; طرق &ndash; فنادق ولذلك ومن اجلكم فأنن شركتنا نعمل في نفس المجال الخاص بالخدمات عموماً وجميع اعمال سباك صحى فى الكويت سباك او فنى صحى فى الكويت يقول افضل فنى صحى بالكويت انة يقوم بعملة بأعلى جودة وارخص سعر موجود فى الكويت ونعطى للعميل ضمان على القطعة المستبدلة وعلى الاعمال التى تم انجازها من عدم رجوع نفس السبب الذى تم اصلاحة ولدينا كما قلنا خدمات عديدة فى عالم السباكة لان للسباكة عنوان وهو شركتنا ومنن اعمالنا خدمة السباك الفني كجزء من خدماتنا وتجد لينا أفضل المهرة في كل الخدمات وبأقل الأسعار والدقة في التنفيذ فقط اتصل بنا وسوف يصلك المندوب&nbsp;</h3>\r\n', '', '', 'فني صحي', 'فني صحي', 'فني صحي', 'فني-صحي_49', '', '', 1, '2017-11-05 19:50:58', '2017-11-06 20:27:55', NULL),
(102, 50, 'صباغ', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'صباغ', 'صباغ', 'صباغ', 'صباغ_50', '', '', 1, '2017-11-06 20:29:36', '2017-11-06 20:29:36', NULL),
(103, 51, 'نقل عفش', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'نقل عفش', 'نقل عفش', 'نقل عفش', 'نقل-عفش_51', '', '', 1, '2017-11-06 20:31:31', '2017-11-06 20:46:32', NULL),
(104, 52, 'ونش سيارات', '', '', '', '<p style="text-align: center;"><strong>ونش كرين سطحة 1807080 بدالة ونشات الكويت سطحه و ونش هيدروليك جميع مناطق الكويت اسرع بدالة ونشات الكويت &ndash; خدمة 24 ساعة</strong></p>\r\n\r\n<p style="text-align: center;"><strong>ونش لإنقاذ السيارات حيث نعتمد فى المقام الأول على تقديم افضل الخدمات لكل عملائها بتوفير عدة عوامل اهمها سرعة الاستجابة للعميل وذلك لإننا نقوم بتغطية معظم مناطق الكويت بعدد كبير من السيارات, كمان نعتمد ايضا على الكفاءةالعاملة على السيارات الخاصة بنا حيث نلتزم بشروط الأمان الكاملة لإيصال سيارتكم بأفضل&nbsp;</strong></p>\r\n', '', '', 'ونش سيارات', 'ونش سيارات', 'ونش سيارات', 'ونش-سيارات_52', '', '', 1, '2017-11-06 20:35:01', '2017-11-06 20:35:01', NULL),
(105, 53, 'مكافحة حشرات', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'مكافحة حشرات', 'مكافحة حشرات', 'مكافحة حشرات', 'مكافحة-حشرات_53', '', '', 1, '2017-11-06 20:36:34', '2017-11-06 20:46:44', NULL),
(106, 54, 'مدرسين', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'مدرسين', 'مدرسين', 'مدرسين', 'مدرسين_54', '', '', 1, '2017-11-06 20:37:11', '2017-11-06 20:46:50', NULL),
(107, 55, 'طيران وحجوزات', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'طيران وحجوزات', 'طيران وحجوزات', 'طيران وحجوزات', 'طيران-وحجوزات_55', '', '', 1, '2017-11-06 20:38:12', '2017-11-06 20:46:57', NULL),
(108, 56, 'فنى ستلايت', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'فنى ستلايت', 'فنى ستلايت', 'فنى ستلايت', 'فنى-ستلايت_56', '', '', 1, '2017-11-06 20:39:10', '2017-11-06 20:47:04', NULL),
(109, 57, 'مكتب عقاري', '', '', '', '<h1 align="center"><strong>صباغ بالكويت</strong></h1>\r\n\r\n<h2 align="center">صباغين بالكويت اهلا وسهلا بك عزيزى العميل . هل تبحث عن صباغين بالكويت ؟ هل تأبي ارقام صباغين بالكويت ؟ لاتحاتى او تحتار فأنت هنا فى المكان الصحيح ، فنحن نتشرف بتقديم خدمات الصباغة والأصباغ وتركيب الباركية وتركيب ورق الجدران ويوجد لدينا جميع انواع الاصباغ السادة والمنقوش وجميع الالوان الحديثة وتشكيلة حديثة من ورق الجدران فلا تتردد بالاتصال بنا لدينا أحدث التصاميم في عالم الأصباغ وأحدث ورق جدران,اصباغ المسيليم وأجمل وأرقى الرسمات على الجدرانجميع انواع الاصباغ والترخيم &ndash; اسبنس ايطالي &ndash; سان ماركو &ndash; روشن &ndash; صبغ صدفي &ndash; ارابيسك &ndash; افكت &ndash; شمواه &ndash; اكسده &ndash; تدخين &ndash; ديكور جبس بورد &ndash; صبغ مطفي &ndash; تقليم &ndash; نبرتيه &ndash; رسم غرف اطقال &ndash; دريم &ndash; فلفت &ndash; ارضيات باركيه &ndash; تعتيق &ndash; معجون بارز &ndash; زيبرا &ndash; يوجد لدينا جميع الاصباغ الايطالية</h2>\r\n', '', '', 'مكتب عقاري', 'مكتب عقاري', 'مكتب عقاري', 'مكتب-عقاري_57', '', '', 1, '2017-11-06 20:40:42', '2017-11-06 20:47:09', NULL),
(110, 58, 'تأجير سيارات', '', '', '', '<p style="text-align: center;"><strong>أجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سياراتأجير سيارات&nbsp;أجير سيارات&nbsp;أجير سيارات</strong></p>\r\n', '', '', 'تأجير سيارات', 'تأجير سيارات', 'تأجير سيارات', 'تأجير-سيارات_58', '', '', 1, '2017-11-06 20:42:37', '2017-11-06 20:42:37', NULL),
(111, 59, 'تعليم قيادة', '', '', '', '<p style="text-align: center;"><strong>تعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادةتعليم قيادة&nbsp;تعليم قيادة&nbsp;تعليم قيادة</strong></p>\r\n', '', '', 'تعليم قيادة ', 'تعليم قيادة ', 'تعليم قيادة ', 'تعليم-قيادة_59', '', '', 1, '2017-11-06 20:44:01', '2017-11-06 20:44:01', NULL);
INSERT INTO `pages_translate` (`id`, `page_id`, `page_title`, `page_city`, `page_period`, `page_short_desc`, `page_body`, `page_header_arr`, `page_body_arr`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`, `page_slug`, `included_services_and_amenities`, `excluded_services_and_amenities`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(112, 60, 'كهربائي', '', '', '', '<p style="text-align: center;"><strong>كهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائيكهربائي&nbsp;كهربائي&nbsp;كهربائي</strong></p>\r\n', '', '', 'كهربائي', 'كهربائي', 'كهربائي', 'كهربائي_60', '', '', 1, '2017-11-06 20:45:15', '2017-11-06 20:45:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `per_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `per_page_id` int(11) NOT NULL,
  `show_action` tinyint(1) NOT NULL,
  `add_action` tinyint(1) NOT NULL,
  `edit_action` tinyint(1) NOT NULL,
  `delete_action` tinyint(1) NOT NULL,
  `additional_permissions` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`per_id`, `user_id`, `per_page_id`, `show_action`, `add_action`, `edit_action`, `delete_action`, `additional_permissions`) VALUES
(2, 1, 1, 1, 1, 1, 1, '["manage_permissions"]'),
(3, 1, 2, 1, 1, 1, 1, 'null'),
(4, 1, 4, 1, 1, 1, 1, 'null'),
(5, 1, 5, 1, 1, 1, 1, 'null'),
(6, 1, 6, 1, 1, 1, 1, 'null'),
(7, 1, 7, 1, 1, 1, 1, 'null'),
(8, 1, 8, 1, 1, 1, 1, 'null'),
(9, 1, 9, 1, 1, 1, 1, 'null'),
(12, 1, 12, 1, 1, 1, 1, 'null'),
(13, 1, 13, 1, 1, 1, 1, 'null'),
(14, 1, 14, 1, 1, 1, 1, '["export_subscribe","email_settings","send_custom_email","send_all_subscribers_email","stop","pause","resume"]'),
(15, 1, 15, 1, 1, 1, 1, 'null'),
(16, 1, 16, 1, 1, 1, 1, 'null'),
(17, 1, 18, 1, 1, 1, 1, 'null'),
(18, 2, 1, 0, 0, 0, 0, ''),
(19, 2, 2, 0, 0, 0, 0, ''),
(20, 2, 4, 0, 0, 0, 0, ''),
(21, 2, 5, 0, 0, 0, 0, ''),
(22, 2, 6, 0, 0, 0, 0, ''),
(23, 2, 7, 1, 1, 1, 1, ''),
(24, 2, 8, 1, 1, 1, 1, ''),
(25, 2, 9, 1, 1, 1, 1, ''),
(28, 2, 12, 1, 1, 1, 1, ''),
(29, 2, 13, 1, 1, 1, 1, ''),
(30, 2, 14, 1, 1, 1, 1, ''),
(31, 2, 15, 1, 1, 1, 1, ''),
(32, 2, 16, 1, 1, 1, 1, ''),
(33, 2, 18, 1, 1, 0, 0, ''),
(34, 1, 19, 1, 1, 1, 1, 'null'),
(35, 1, 20, 1, 1, 1, 1, 'null'),
(36, 1, 21, 1, 1, 1, 1, '["add_match_events","remove_match_event","add_match_result","remove_match_result","add_match_points"]'),
(37, 1, 22, 1, 1, 1, 1, 'null'),
(38, 1, 23, 1, 1, 1, 1, 'null'),
(39, 1, 24, 1, 1, 1, 1, '["generate_matches"]'),
(40, 1, 25, 1, 1, 1, 1, 'null'),
(41, 1, 26, 1, 1, 1, 1, '["champion_group_clubs"]'),
(42, 1, 27, 1, 1, 1, 1, 'null'),
(43, 1, 28, 1, 1, 1, 1, 'null'),
(44, 2, 19, 0, 0, 0, 0, ''),
(45, 2, 20, 0, 0, 0, 0, ''),
(46, 2, 21, 0, 0, 0, 0, ''),
(47, 2, 22, 0, 0, 0, 0, ''),
(48, 2, 23, 0, 0, 0, 0, ''),
(49, 2, 24, 0, 0, 0, 0, ''),
(50, 2, 25, 0, 0, 0, 0, ''),
(51, 2, 26, 0, 0, 0, 0, ''),
(52, 2, 27, 0, 0, 0, 0, ''),
(53, 2, 28, 0, 0, 0, 0, ''),
(54, 10, 1, 0, 0, 0, 0, 'null'),
(55, 10, 2, 1, 1, 1, 1, 'null'),
(56, 10, 4, 0, 0, 0, 0, 'null'),
(57, 10, 5, 1, 1, 1, 1, 'null'),
(58, 10, 6, 1, 1, 1, 1, 'null'),
(59, 10, 7, 1, 1, 1, 1, 'null'),
(60, 10, 8, 0, 0, 0, 0, 'null'),
(61, 10, 9, 0, 0, 0, 0, 'null'),
(62, 10, 12, 1, 1, 1, 1, 'null'),
(63, 10, 13, 0, 0, 0, 0, 'null'),
(64, 10, 14, 0, 0, 0, 0, 'null'),
(65, 10, 15, 0, 0, 0, 0, 'null'),
(66, 10, 16, 1, 1, 1, 1, 'null'),
(67, 10, 18, 0, 0, 0, 0, 'null'),
(68, 10, 19, 1, 1, 1, 1, 'null'),
(69, 10, 20, 1, 1, 1, 1, 'null'),
(70, 10, 21, 1, 1, 1, 1, '["add_match_events","remove_match_event","add_match_result","remove_match_result","add_match_points"]'),
(71, 10, 22, 1, 1, 1, 1, 'null'),
(72, 10, 23, 1, 1, 1, 1, 'null'),
(73, 10, 24, 1, 1, 1, 1, 'null'),
(74, 10, 25, 1, 1, 1, 1, 'null'),
(75, 10, 26, 1, 1, 1, 1, 'null'),
(76, 10, 27, 1, 1, 1, 1, 'null'),
(77, 10, 28, 0, 0, 0, 0, 'null'),
(78, 11, 1, 0, 0, 0, 0, ''),
(79, 11, 2, 1, 1, 1, 1, ''),
(80, 11, 4, 0, 0, 0, 0, ''),
(81, 11, 5, 1, 1, 1, 1, ''),
(82, 11, 6, 0, 0, 0, 0, ''),
(83, 11, 7, 0, 0, 0, 0, ''),
(84, 11, 8, 0, 0, 0, 0, ''),
(85, 11, 9, 0, 0, 0, 0, ''),
(86, 11, 12, 1, 1, 1, 1, ''),
(87, 11, 13, 0, 0, 0, 0, ''),
(88, 11, 14, 0, 0, 0, 0, ''),
(89, 11, 15, 0, 0, 0, 0, ''),
(90, 11, 16, 1, 1, 1, 1, ''),
(91, 11, 18, 0, 0, 0, 0, ''),
(92, 11, 19, 1, 1, 1, 1, ''),
(93, 11, 20, 1, 1, 1, 1, ''),
(94, 11, 21, 1, 1, 1, 1, ''),
(95, 11, 22, 1, 1, 1, 1, ''),
(96, 11, 23, 1, 1, 1, 1, ''),
(97, 11, 24, 1, 1, 1, 1, ''),
(98, 11, 25, 1, 1, 1, 1, ''),
(99, 11, 26, 1, 1, 1, 1, ''),
(100, 11, 27, 1, 1, 1, 1, ''),
(101, 11, 28, 0, 0, 0, 0, ''),
(102, 12, 1, 0, 0, 0, 0, ''),
(103, 12, 2, 0, 0, 0, 0, ''),
(104, 12, 4, 0, 0, 0, 0, ''),
(105, 12, 5, 1, 1, 1, 1, ''),
(106, 12, 6, 0, 1, 0, 0, ''),
(107, 12, 7, 0, 0, 0, 0, ''),
(108, 12, 8, 0, 0, 0, 0, ''),
(109, 12, 9, 0, 0, 0, 0, ''),
(110, 12, 12, 1, 1, 1, 1, ''),
(111, 12, 13, 0, 0, 0, 0, ''),
(112, 12, 14, 0, 0, 0, 0, ''),
(113, 12, 15, 0, 0, 0, 0, ''),
(114, 12, 16, 0, 0, 0, 0, ''),
(115, 12, 18, 0, 0, 0, 0, ''),
(116, 12, 19, 1, 1, 1, 1, ''),
(117, 12, 20, 1, 1, 1, 1, ''),
(118, 12, 21, 1, 1, 1, 1, ''),
(119, 12, 22, 1, 1, 1, 1, ''),
(120, 12, 23, 1, 1, 1, 1, ''),
(121, 12, 24, 1, 1, 1, 1, ''),
(122, 12, 25, 1, 1, 1, 1, ''),
(123, 12, 26, 1, 1, 1, 1, ''),
(124, 12, 27, 1, 1, 1, 1, ''),
(125, 12, 28, 0, 0, 0, 0, ''),
(126, 14, 1, 0, 0, 0, 0, ''),
(127, 14, 2, 1, 1, 1, 1, ''),
(128, 14, 4, 0, 0, 0, 0, ''),
(129, 14, 5, 1, 1, 1, 1, ''),
(130, 14, 6, 0, 0, 0, 0, ''),
(131, 14, 7, 0, 0, 0, 0, ''),
(132, 14, 8, 0, 0, 0, 0, ''),
(133, 14, 9, 0, 0, 0, 0, ''),
(134, 14, 12, 1, 1, 1, 1, ''),
(135, 14, 13, 0, 0, 0, 0, ''),
(136, 14, 14, 0, 0, 0, 0, ''),
(137, 14, 15, 0, 0, 0, 0, ''),
(138, 14, 16, 0, 0, 0, 0, ''),
(139, 14, 18, 0, 0, 0, 0, ''),
(140, 14, 19, 1, 1, 1, 1, ''),
(141, 14, 20, 1, 1, 1, 1, ''),
(142, 14, 21, 1, 1, 1, 1, ''),
(143, 14, 22, 1, 1, 1, 1, ''),
(144, 14, 23, 1, 1, 1, 1, ''),
(145, 14, 24, 1, 1, 1, 1, ''),
(146, 14, 25, 1, 1, 1, 1, ''),
(147, 14, 26, 1, 1, 1, 1, ''),
(148, 14, 27, 1, 1, 1, 1, ''),
(149, 14, 28, 0, 0, 0, 0, ''),
(150, 15, 1, 0, 0, 0, 0, ''),
(151, 15, 2, 0, 0, 0, 0, ''),
(152, 15, 4, 0, 0, 0, 0, ''),
(153, 15, 5, 1, 1, 1, 1, ''),
(154, 15, 6, 0, 0, 0, 0, ''),
(155, 15, 7, 0, 0, 0, 0, ''),
(156, 15, 8, 0, 0, 0, 0, ''),
(157, 15, 9, 0, 0, 0, 0, ''),
(158, 15, 12, 1, 1, 1, 1, ''),
(159, 15, 13, 0, 0, 0, 0, ''),
(160, 15, 14, 0, 0, 0, 0, ''),
(161, 15, 15, 0, 0, 0, 0, ''),
(162, 15, 16, 1, 1, 1, 1, ''),
(163, 15, 18, 0, 0, 0, 0, ''),
(164, 15, 19, 1, 1, 1, 1, ''),
(165, 15, 20, 1, 1, 1, 1, ''),
(166, 15, 21, 1, 1, 1, 1, ''),
(167, 15, 22, 1, 1, 1, 1, ''),
(168, 15, 23, 1, 1, 1, 1, ''),
(169, 15, 24, 1, 1, 1, 1, ''),
(170, 15, 25, 1, 1, 1, 1, ''),
(171, 15, 26, 1, 1, 1, 1, ''),
(172, 15, 27, 1, 1, 1, 1, ''),
(173, 15, 28, 0, 0, 0, 0, ''),
(174, 1, 29, 1, 1, 1, 1, ''),
(175, 1, 30, 1, 1, 1, 1, ''),
(176, 2, 29, 1, 1, 1, 0, ''),
(177, 2, 30, 1, 1, 1, 1, ''),
(178, 3, 1, 0, 0, 0, 0, ''),
(179, 3, 2, 0, 0, 0, 0, ''),
(180, 3, 4, 0, 0, 0, 0, ''),
(181, 3, 5, 0, 0, 0, 0, ''),
(182, 3, 6, 0, 0, 0, 0, ''),
(183, 3, 7, 0, 0, 0, 0, ''),
(184, 3, 8, 0, 0, 0, 0, ''),
(185, 3, 9, 0, 0, 0, 0, ''),
(186, 3, 12, 0, 0, 0, 0, ''),
(187, 3, 13, 0, 0, 0, 0, ''),
(188, 3, 14, 0, 0, 0, 0, ''),
(189, 3, 15, 0, 0, 0, 0, ''),
(190, 3, 16, 0, 0, 0, 0, ''),
(191, 3, 18, 0, 0, 0, 0, ''),
(192, 3, 29, 0, 0, 0, 0, ''),
(193, 3, 30, 1, 1, 1, 1, ''),
(194, 4, 1, 0, 0, 0, 0, ''),
(195, 4, 2, 0, 0, 0, 0, ''),
(196, 4, 4, 0, 0, 0, 0, ''),
(197, 4, 5, 0, 0, 0, 0, ''),
(198, 4, 6, 0, 0, 0, 0, ''),
(199, 4, 7, 0, 0, 0, 0, ''),
(200, 4, 8, 0, 0, 0, 0, ''),
(201, 4, 9, 0, 0, 0, 0, ''),
(202, 4, 12, 0, 0, 0, 0, ''),
(203, 4, 13, 0, 0, 0, 0, ''),
(204, 4, 14, 0, 0, 0, 0, ''),
(205, 4, 15, 0, 0, 0, 0, ''),
(206, 4, 16, 0, 0, 0, 0, ''),
(207, 4, 18, 0, 0, 0, 0, ''),
(208, 4, 29, 0, 0, 0, 0, ''),
(209, 4, 30, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `permission_pages`
--

CREATE TABLE IF NOT EXISTS `permission_pages` (
  `per_page_id` int(11) NOT NULL,
  `page_name` varchar(200) NOT NULL,
  `sub_sys` varchar(200) NOT NULL,
  `show_in_admin_panel` tinyint(1) NOT NULL,
  `all_additional_permissions` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_pages`
--

INSERT INTO `permission_pages` (`per_page_id`, `page_name`, `sub_sys`, `show_in_admin_panel`, `all_additional_permissions`) VALUES
(1, 'admin/admins', 'admin', 1, '["manage_permissions"]'),
(2, 'uploader', 'admin', 1, '[]'),
(4, 'admin/ads', 'admin', 1, '[]'),
(5, 'admin/category', 'admin', 1, '[]'),
(6, 'admin/edit_content', 'admin', 1, '[]'),
(7, 'admin/langs', 'admin', 1, '[]'),
(8, 'admin/menus', 'admin', 1, '[]'),
(9, 'admin/notifications', 'admin', 1, '[]'),
(12, 'admin/pages', 'admin', 1, '[]'),
(13, 'admin/settings', 'admin', 1, '[]'),
(14, 'admin/subscribe', 'admin', 1, '["export_subscribe","email_settings","send_custom_email","send_all_subscribers_email","stop","pause","resume"]'),
(15, 'admin/support_messages', 'admin', 1, '[]'),
(16, 'admin/uploader', 'admin', 1, '[]'),
(18, 'admin/users', 'admin', 1, '[]'),
(29, 'admin/trip', 'admin', 1, '[]'),
(30, 'admin/currencies', 'admin', 1, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('09086d2d82a61d3702bce6381e065f8ca1c44693', NULL, '192.254.138.161', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.21 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiZ25ONWtFUnJoZWtLTDk1TVZiNFBLZkpLRW54ajFMazBqOTZzTzJnSSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDE6Imh0dHA6Ly93d3cuZG10Y2FsbC5jb20vc3Vic2NyaWJlX2Nyb25fam9wIjt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODUzMzg0MTM7czoxOiJjIjtpOjE0ODUzMzg0MTM7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1485338414),
('10767531b518e49b6e355231db93bc3afe1359b9', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiZVlGTHlXTVlpUGtOQzg3WEZsbVdBUk1vWUd0dG5XcDI0NTBTb2ZyUCI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4OTMwODcyOTtzOjE6ImMiO2k6MTQ4OTMwODEyNjtzOjE6ImwiO3M6MToiMCI7fX0=', 1489308730),
('1d9111ece6496e463669c06739f929a8f8e43b1e', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiWXR0d1ZkVGNLY0pWMnJsS3pqWkxkYzhTcmhTcXhTRExxajlyRDU2OCI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODY5OTQ1MzU7czoxOiJjIjtpOjE0ODY5OTQ1MzU7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1486994535),
('29fe3d4953bb8ed48be0e939736a2131ef231474', NULL, '169.53.142.99', 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiaVRWWXJYTllpaWVhalVOdmUxcU05UUVFQzA4YVUyTVJnRG5MSWxWdiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHA6Ly93d3cuZG10Y2FsbC5jb20iO31zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4NTMzOTMwMjtzOjE6ImMiO2k6MTQ4NTMzOTMwMjtzOjE6ImwiO3M6MToiMCI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1485339303),
('44434f5252387b0b24169f85313b0b0671e36dbe', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoieXpxbm9IQ0tjWW9LclBjN3l6YVNudFJJcElNRnhTWlBYaWdYQ0xqTSI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4OTMwNjkxMDtzOjE6ImMiO2k6MTQ4OTMwNjI0MztzOjE6ImwiO3M6MToiMCI7fX0=', 1489306910),
('462a66ab9ac9fdd9b2507665f96ca530cb8fd5b2', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiWThCRmVXOXd2eVlpa1hGc3FpSG91dm9pRGRKZnNNNWU5YjhHT05XbSI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODcwMTQwMTU7czoxOiJjIjtpOjE0ODcwMTQwMTU7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1487014016),
('61eeae121874e0f9ae7836a98e0bfa2335547214', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiN0tkZGZyWnYxWjZpRmFCdkJiYW9RTU1ZY09aUVZmT1JCVElTRnZCOSI7czo1OiJmbGFzaCI7YToyOntzOjM6Im5ldyI7YTowOnt9czozOiJvbGQiO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4NTg1Mjc4MztzOjE6ImMiO2k6MTQ4NTg1Mjc4MztzOjE6ImwiO3M6MToiMCI7fX0=', 1485852783),
('6b21a692306760e1c5e52853a5beca125e5a17c4', 1, '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoid2s4anR0a0QyQURFRkR0dG9UaVNVUXRsWE53MVZ5RWRBa0p1WVJJRCI7czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjIyOiJQSFBERUJVR0JBUl9TVEFDS19EQVRBIjthOjA6e31zOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czoyOToiaHR0cDovL2xvY2FsaG9zdC9zZW9lcmEvYWltbm0iO31zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4NTQyOTE1NDtzOjE6ImMiO2k6MTQ4NTQyOTE1NDtzOjE6ImwiO3M6MToiMCI7fXM6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1485429158),
('7a29015ed92153aa5fa7550875ff2988ea6ee7b1', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiWFdmbEFUaXdGQjk4T3JoNk81d0JOZ2tONnlUSU1MYnl1MExJbEJwOCI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0OTAwMDgyMTE7czoxOiJjIjtpOjE0OTAwMDgyMTE7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1490008211),
('8341f4d833997e47f061b11bb653f7d357746b8a', NULL, '41.69.87.64', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoieURSWTF5RTFudzZPbm4xUXRqVEdEVlNGeXFWeDRTYUpMdUQzM3MyWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHA6Ly93d3cuZG10Y2FsbC5jb20iO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDg1MzM5MjMxO3M6MToiYyI7aToxNDg1MzM5MjI3O3M6MToibCI7czoxOiIwIjt9fQ==', 1485339231),
('858652dfa1acedbc06d858cc36b69dfeeae77234', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoieWNjY2p6TzVuU25ZRkdVelJoU2o1bnoxWHdwZWFIWjc1RVFoNXV2YiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTM6Imh0dHA6Ly9sb2NhbGhvc3Qvc2VvZXJhL2NzY3Nfc2hvcHBpbmcvYWRtaW4vZGFzaGJvYXJkIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MjI6IlBIUERFQlVHQkFSX1NUQUNLX0RBVEEiO2E6MDp7fXM6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDg1MzQ1MjYyO3M6MToiYyI7aToxNDg1MzQyMzE2O3M6MToibCI7czoxOiIwIjt9fQ==', 1485345262),
('b2f227334af52ff911c64462d24e926240e5dd9f', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiQVN4RVhTZU03aEQ0cGg5ZjdZMUNhSGZTOEFCb0NqVUdaaW9SeTgzayI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ4OTMxMjU2NjtzOjE6ImMiO2k6MTQ4OTMxMDQ1MTtzOjE6ImwiO3M6MToiMCI7fX0=', 1489312566),
('b48a8832d4bb947c6817bdcc7f4157b0491663c3', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiQWIxbjdNZGhJbzBORUw4TVlPWjU4VGo5dW0yVThuN0RIYjBNa3VJayI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTUwOTkwODcxNTtzOjE6ImMiO2k6MTUwOTkwODEzNDtzOjE6ImwiO3M6MToiMCI7fX0=', 1509908715),
('d4d0c94feaf60bb357c665486ad4cd2d55c9b8f6', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoib2I3WUlBckpXRDJpWE5VaUZVeU9YbU40NTJ2aUY0VVNvTjBPMUpJciI7czoyMjoiUEhQREVCVUdCQVJfU1RBQ0tfREFUQSI7YTowOnt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ5NDE3MTE4NTtzOjE6ImMiO2k6MTQ5NDE3MTE4NDtzOjE6ImwiO3M6MToiMCI7fX0=', 1494171185);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `set_id` int(11) NOT NULL,
  `general_currency` varchar(10) NOT NULL COMMENT 'kol kam mn el purchases ex. all user purchases is 100, every 10 of them he can take 1 pound',
  `rate` decimal(10,7) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`set_id`, `general_currency`, `rate`) VALUES
(1, 'EGP', '17.6100010');

-- --------------------------------------------------------

--
-- Table structure for table `site_content`
--

CREATE TABLE IF NOT EXISTS `site_content` (
  `id` int(11) NOT NULL,
  `content_title` varchar(200) NOT NULL,
  `content_json` text NOT NULL,
  `lang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_content`
--

INSERT INTO `site_content` (`id`, `content_title`, `content_json`, `lang_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'support', '{"meta_title":"\\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a \\u0644\\u0644\\u0645\\u0648\\u0642\\u0639","meta_desc":"\\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a \\u0644\\u0644\\u0645\\u0648\\u0642\\u0639","meta_keywords":"\\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a \\u0644\\u0644\\u0645\\u0648\\u0642\\u0639","header":"\\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a \\u0644\\u0644\\u0645\\u0648\\u0642\\u0639","form_header":"\\u0627\\u0631\\u0633\\u0644 \\u0631\\u0633\\u0627\\u0644\\u0629 \\u0627\\u0644\\u064a \\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a \\u0644\\u0644\\u0645\\u0648\\u0642\\u0639","form_name":"\\u0627\\u0644\\u0627\\u0633\\u0645","form_email":"\\u0627\\u0644\\u0625\\u064a\\u0645\\u064a\\u0644 \\u0627\\u0646 \\u0648\\u062c\\u062f","form_msg":"\\u0646\\u0635 \\u0627\\u0644\\u0631\\u0633\\u0627\\u0644\\u0629","form_btn":"\\u0623\\u0631\\u0633\\u0644","form_success_msg":"\\u0644\\u0642\\u062f \\u062a\\u0645 \\u0625\\u0631\\u0633\\u0627\\u0644 \\u0627\\u0644\\u0631\\u0633\\u0627\\u0644\\u0629 \\u0628\\u0646\\u062c\\u0627\\u062d","form_title":"\\u0639\\u0646\\u0648\\u0627\\u0646 \\u0627\\u0644\\u0631\\u0633\\u0627\\u0644\\u0629","form_phone":"\\u0631\\u0642\\u0645 \\u0627\\u0644\\u062d\\u0648\\u0627\\u0644","map":"<iframe src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m10!1m8!1m3!1d13822.986417652915!2d31.3141289!3d29.9867115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1489065450898\\" width=\\"100%\\" height=\\"300px\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen=\\"\\"><\\/iframe>","sidebar_header1":"\\u0628\\u064a\\u0627\\u0646\\u0627\\u062a \\u0627\\u0644\\u062a\\u0648\\u0627\\u0635\\u0644","sidebar_header2":"\\u0645\\u0648\\u0627\\u0639\\u064a\\u062f \\u0627\\u0644\\u0639\\u0645\\u0644","sidebar_header3":"","sidebar_section3_desc":"","img_ids":{"background":1765},"contact_data_label":["<i class=\\"fa fa-home\\"><\\/i>","<i class=\\"fa fa-envelope\\"><\\/i>","<i class=\\"fa fa-phone\\"><\\/i>","<i class=\\"fa fa-globe\\"><\\/i>"],"contact_data_value":["\\u0639\\u0646\\u0648\\u0627\\u0646 \\u0627\\u0644\\u0645\\u0648\\u0642\\u0639 \\u064a\\u0643\\u062a\\u0628 \\u0647\\u0646\\u0627","info@kuwait.com","0123456789","http:\\/\\/www.kuwait.com",""],"contact_data_url":["#","#","#","http:\\/\\/www.kuwait.com",""],"sidebar_section2_data_label":["\\u0645\\u0646 \\u0627\\u0644\\u062d\\u062f \\u0627\\u0644\\u064a \\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647 \\u0648\\u0627\\u0644\\u0633\\u0628\\u062a"],"sidebar_section2_data_value":["10 am to 8 pm","\\u0623\\u062c\\u0627\\u0632\\u0629",""],"sidebar_section2_data_url":["","",""],"sidebar_section3_data_label":[],"sidebar_section3_data_value":[""],"sidebar_section3_data_url":[""]}', 1, NULL, NULL, NULL),
(2, 'edit_index_page', '{"index_meta_title":"\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0643\\u0648\\u064a\\u062a","index_meta_desc":"\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0643\\u0648\\u064a\\u062a","index_meta_keywords":"\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0643\\u0648\\u064a\\u062a","website_name":"\\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0643\\u0648\\u064a\\u062a","website_info":"\\u064a\\u0643\\u062a\\u0628 \\u0647\\u0646\\u0627 \\u0646\\u0628\\u0630\\u0629 \\u0645\\u062e\\u062a\\u0635\\u0631\\u0629 \\u0639\\u0646 \\u0645\\u0648\\u0642\\u0639 \\u0627\\u0644\\u0643\\u0648\\u064a\\u062a","menu_homepage":"\\u0627\\u0644\\u0631\\u0626\\u064a\\u0633\\u064a\\u0629","menu_contact":"\\u0635\\u0641\\u062d\\u0647 \\u0627\\u0644\\u062f\\u0639\\u0645 \\u0627\\u0644\\u0641\\u0646\\u064a","fb_link":"#","gplus_link":"#","instagram_link":"#","youtube_link":"#","whatsapp_link":"","twitter_link":"","img_ids":{"logo":1667,"icon":1668}}', 1, NULL, NULL, NULL),
(3, 'general_static_keywords', '{"homepage":"Home","more":"Read More","search_txt":"Search","trip_price":"Price","close":"Close","booking_now":"Booking Now","related_tours":"Related Tours\\r\\n","check_availability_header":"Check Availability Now","check_availability_text":"Check Availability ","check_availability_name":"Name","check_availability_email":"Email","check_availability_country":"Country","check_availability_address":"Address","check_availability_telephone":"Mobile","check_availability_fax":"Phone","check_availability_message":"Message","check_availability_button_text":"Check Availability Now","img_ids":[]}', 1, NULL, NULL, NULL),
(4, 'all_cats_page', '{"all_cats_header":"All Categories","meta_title":"All Categories","meta_desc":"All Categories","meta_keywords":"All Categories","img_ids":[],"slider1":{"img_ids":[1672,1784,1785,1786]}}', 1, NULL, NULL, NULL),
(5, 'search_page', '{"meta_title":"Search","meta_desc":"Search","meta_keywords":"Search","search_header":"Search","img_ids":[]}', 1, NULL, NULL, NULL),
(6, 'email_page', '{"copyright":"Copyright \\u00a9 2017, BTM All Rights Reserved\\r\\n","img_ids":{"logo_img":1766},"social_imgs":[],"social_links":[]}', 1, NULL, NULL, NULL),
(7, 'general_static_keywords', '{"homepage":"Home","more":"Read More","search_txt":"Search","trip_price":"Price","close":"Close","booking_now":"Booking","related_tours":"Related Tours\\r\\n","check_availability_header":"Check Availability Now","check_availability_text":"Check Availability","check_availability_name":"Name","check_availability_email":"Email","check_availability_country":"Country","check_availability_address":"Address","check_availability_telephone":"Telephone","check_availability_fax":"Fax","check_availability_message":"Message","check_availability_button_text":"Check Availability ","img_ids":[]}', 2, NULL, NULL, NULL),
(8, 'verification_messages', '', 1, NULL, NULL, NULL),
(9, 'pages_seo', '', 1, NULL, NULL, NULL),
(10, 'general_static_keywords', '{"homepage":"P\\u00e1gina Principal","more":"Leer M\\u00e1s","search_txt":"Buscar","trip_price":"Precio Del Tour","close":"Cerrar","booking_now":"Reserva Ahora","related_tours":"Tours Relacionados","check_availability_header":"p\\u00e1gina principal","check_availability_text":"Texto","check_availability_name":"Nombre","check_availability_email":"Correo Electr\\u00f3nico","check_availability_country":"Pa\\u00eds","check_availability_address":"Direcci\\u00f3n","check_availability_telephone":"Tel\\u00e9fono ","check_availability_fax":"Fax","check_availability_message":"Mensaje","check_availability_button_text":"Bot\\u00f3n De Texto","img_ids":[]}', 3, NULL, NULL, NULL),
(11, 'edit_index_page', '{"index_meta_title":"Travel Corner Egypt","index_meta_desc":"Travel Corner","index_meta_keywords":"Travel Corner","select_language":"Idioma","select_currency":"Moneda","open_map_txt":"Mapa","contact_menu_txt":"Con\\u00e9ctanos","articles_menu_text":"Art\\u00edculo","latest_4_trips_on_menu":"Ultimas Ofertas","see_more_trips":"Ver M\\u00e1s Tours","latest_tours_header":"Ultimas Ofertas","search_header":"Buscar","search_btn":"Buscar","build_your_trip_header":"Haz Tu Viaje","build_your_trip_desc":"Encu\\u00e9ntrate Tu Viaje De Sue\\u00f1o Hoy","follow_facebook_header":"Con\\u00e9ctanos Por Facebook","follow_facebook_script":"<iframe src=\\"https:\\/\\/www.facebook.com\\/plugins\\/page.php?href=https%3A%2F%2Fwww.facebook.com%2FEgyTravelCorner%2F&tabs=timeline&width=300&height=350&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId\\" width=\\"300\\" height=\\"300\\" style=\\"border:none;overflow:hidden\\" scrolling=\\"no\\" frameborder=\\"0\\" allowTransparency=\\"true\\"><\\/iframe>","related_sites_header":"Sitios Relacionados","internation_currencies_header":"Monedas Internacionales","internation_currencies_desc":"Valor De la Libra Egipcia","subscribe_placeholder":"Escribe Tu Correo","subscribe_btn_text":"Suscr\\u00edbete","contact_support_btn_text":"Envia Un Correo Ahora","copyright":"Copyright \\u00a9 2014, <a href=\\"http:\\/\\/www.seoera.net\\" target=\\"_blank\\">SeoEra<\\/a>  All Rights Reserved","index_video_iframe":"<iframe width=\\"100%\\" height=\\"450\\" src=\\"http:\\/\\/www.youtube.com\\/embed\\/FH9op_tsSd4?frameborder=0\\" allowfullscreen><\\/iframe>","map_motion":"true","map_zoom":"9","center_location_lat":"28.1312482","center_location_lng":"30.7168979","img_ids":{"logo":1823,"icon":1824,"payment_methods":1818,"video_thumbnail":1819},"contacts_icon":["<i class=\\"material-icons dp48\\">call<\\/i>","<i class=\\"material-icons\\">mail_outline<\\/i>"],"contacts_text":["(002) 01155858580","info@egytravelcorner.com"],"related_sites_text":["Luxor Day Tours","tripadvisor"],"related_sites_url":["#","#"],"slider1":{"img_ids":[1825,1826,1827,1828],"other_fields":{"title":["Facebook","Google+","Twitter","Linkin"],"link":["#","#","#","#"]}},"slider2":{"img_ids":[1829],"other_fields":{"header":["Bruges - Belgium"],"description":["Bruges, the capital of West Flanders in northwest Belgium, "],"link_text":["See More"],"link_url":["#"]}}}', 3, NULL, NULL, NULL),
(12, 'edit_index_page', '{"index_meta_title":"Travel Corner Egypt","index_meta_desc":"Travel Corner","index_meta_keywords":"Travel Corner","select_language":"Language","select_currency":"Currency","open_map_txt":"Open Map","contact_menu_txt":"Contact Us","articles_menu_text":"Articles","latest_4_trips_on_menu":"Latest 4 Trips","see_more_trips":"See More","latest_tours_header":"Latest Trips","search_header":"Search about Trips","search_btn":"Search","build_your_trip_header":"Build Your Trip","build_your_trip_desc":"Find your dream tour today!","follow_facebook_header":"Follow us on facebook","follow_facebook_script":"<iframe src=\\"https:\\/\\/www.facebook.com\\/plugins\\/page.php?href=https%3A%2F%2Fwww.facebook.com%2FEgyTravelCorner%2F&tabs=timeline&width=300&height=350&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId\\" width=\\"300\\" height=\\"300\\" style=\\"border:none;overflow:hidden\\" scrolling=\\"no\\" frameborder=\\"0\\" allowTransparency=\\"true\\"><\\/iframe>","related_sites_header":"Related Sites","internation_currencies_header":"","internation_currencies_desc":"","subscribe_placeholder":"","subscribe_btn_text":"","contact_support_btn_text":"","copyright":"","index_video_iframe":"","map_motion":"","map_zoom":"","center_location_lat":"","center_location_lng":"","img_ids":{"logo":0,"icon":0,"payment_methods":0,"video_thumbnail":0},"contacts_icon":[],"contacts_text":[""],"related_sites_text":[],"related_sites_url":[""],"slider1":{"img_ids":[],"other_fields":{"title":[""],"link":[""]}},"slider2":{"img_ids":[],"other_fields":{"header":[""],"description":[""],"link_text":[""],"link_url":[""]}}}', 2, NULL, NULL, NULL),
(13, 'support', '{"meta_title":"Contactos","meta_desc":"Contactos","meta_keywords":"Contactos","header":"Contactos","form_header":"Si\\u00e9ntente Libre De Llamarnos Cuando Quieras","form_name":"Tu Nombre","form_email":"Tu Correo Electr\\u00f3nico","form_msg":"Tu Mensaje","form_btn":"Envia","form_success_msg":"Tu Mensaje Se Ha Enviado Correctamente","form_title":" T\\u00edtulo Del Mensaje","form_phone":"Num\\u00e9ro De  Tel\\u00e9fono","map":"<iframe src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m10!1m8!1m3!1d13822.986417652915!2d31.3141289!3d29.9867115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1489065450898\\" width=\\"100%\\" height=\\"300px\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen=\\"\\"><\\/iframe>","img_ids":{"background":0},"label":["Tel\\u00e9fono"],"value":["(002) 00 122 391 1092"]}', 3, NULL, NULL, NULL),
(14, 'pages_seo', '', 3, NULL, NULL, NULL),
(15, 'trip_keywords', '', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sortable_menus`
--

CREATE TABLE IF NOT EXISTS `sortable_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `menu_json` text NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sortable_menus`
--

INSERT INTO `sortable_menus` (`menu_id`, `menu_title`, `menu_json`, `lang_id`) VALUES
(4, 'Main Menu', '[{"level_data":{"item_name":"\\u0645\\u0639\\u0644\\u0648\\u0645\\u0627\\u062a","item_slug":"#","item_class":""},"level_childs":[{"level_data":{"item_name":"\\u0645\\u0646 \\u0646\\u062d\\u0646","item_slug":"About-Us","item_class":""}},{"level_data":{"item_name":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","item_slug":"support","item_class":""}},{"level_data":{"item_name":"\\u0647\\u062f\\u0641\\u0646\\u0627 \\u0648\\u0631\\u0624\\u064a\\u062a\\u0646\\u0627","item_slug":"\\u0647\\u062f\\u0641\\u0646\\u0627-\\u0648-\\u0631\\u0624\\u064a\\u062a\\u0646\\u0627","item_class":""}}]},{"level_data":{"item_name":"\\u062d\\u0645\\u0627\\u064a\\u0629 \\u062d\\u0642\\u0648\\u0642 \\u0627\\u0644\\u0639\\u0645\\u064a\\u0644","item_slug":"\\u062d\\u0645\\u0627\\u064a\\u0629-\\u062d\\u0642\\u0648\\u0642-\\u0627\\u0644\\u0639\\u0645\\u064a\\u0644","item_class":""}},{"level_data":{"item_name":"\\u0633\\u064a\\u0627\\u0633\\u0629 \\u0627\\u0644\\u062e\\u0635\\u0648\\u0635\\u064a\\u0629","item_slug":"\\u0633\\u064a\\u0627\\u0633\\u0629-\\u0627\\u0644\\u062e\\u0635\\u0648\\u0635\\u064a\\u0629","item_class":""}},{"level_data":{"item_name":"\\u0637\\u0631\\u0642 \\u0627\\u0644\\u062f\\u0641\\u0639","item_slug":"\\u0637\\u0631\\u0642-\\u0627\\u0644\\u062f\\u0641\\u0639","item_class":""}},{"level_data":{"item_name":"\\u0627\\u0644\\u0623\\u0633\\u0626\\u0644\\u0629 \\u0627\\u0644\\u0623\\u0643\\u062b\\u0631 \\u0634\\u064a\\u0648\\u0639\\u0627","item_slug":"\\u0627\\u0644\\u0623\\u0633\\u0626\\u0644\\u0629-\\u0627\\u0644\\u0623\\u0643\\u062b\\u0631-\\u0634\\u064a\\u0648\\u0639\\u0627","item_class":""}}]', 1),
(11, 'footer en menu', '[{"level_data":{"item_name":"HomePage","item_slug":"","item_class":""}}]', 2),
(12, 'footer ar menu', '[{"level_data":{"item_name":"\\u0645\\u0639\\u0644\\u0648\\u0645\\u0627\\u062a","item_slug":"#","item_class":""},"level_childs":[{"level_data":{"item_name":"\\u0645\\u0646 \\u0646\\u062d\\u0646","item_slug":"About-Us","item_class":""}},{"level_data":{"item_name":"\\u062d\\u0633\\u0627\\u0628\\u0627\\u062a\\u0646\\u0627\\u0627\\u0644\\u0628\\u0646\\u0643\\u064a\\u0629","item_slug":"\\u062d\\u0633\\u0627\\u0628\\u0627\\u062a\\u0646\\u0627\\u0627\\u0644\\u0628\\u0646\\u0643\\u064a\\u0629","item_class":""}},{"level_data":{"item_name":"\\u0633\\u064a\\u0627\\u0633\\u0629 \\u0627\\u0644\\u062e\\u0635\\u0648\\u0635\\u064a\\u0629","item_slug":"\\u0633\\u064a\\u0627\\u0633\\u0629-\\u0627\\u0644\\u062e\\u0635\\u0648\\u0635\\u064a\\u0629","item_class":""}},{"level_data":{"item_name":"\\u0647\\u062f\\u0641\\u0646\\u0627 \\u0648\\u0631\\u0624\\u064a\\u062a\\u0646\\u0627","item_slug":"\\u0647\\u062f\\u0641\\u0646\\u0627-\\u0648-\\u0631\\u0624\\u064a\\u062a\\u0646\\u0627","item_class":""}},{"level_data":{"item_name":"\\u062d\\u0645\\u0627\\u064a\\u0629 \\u062d\\u0642\\u0648\\u0642 \\u0627\\u0644\\u0639\\u0645\\u064a\\u0644","item_slug":"\\u062d\\u0645\\u0627\\u064a\\u0629-\\u062d\\u0642\\u0648\\u0642-\\u0627\\u0644\\u0639\\u0645\\u064a\\u0644","item_class":""}}]},{"level_data":{"item_name":"\\u062e\\u062f\\u0645\\u0627\\u062a \\u0627\\u0644\\u0639\\u0645\\u0644\\u0627\\u0621","item_slug":"#","item_class":""},"level_childs":[{"level_data":{"item_name":"\\u0627\\u062a\\u0635\\u0644 \\u0628\\u0646\\u0627","item_slug":"Contactus","item_class":""}},{"level_data":{"item_name":"\\u0648\\u0633\\u0627\\u0626\\u0644 \\u0627\\u0644\\u062f\\u0641\\u0639","item_slug":"Payment-methods","item_class":""}},{"level_data":{"item_name":"\\u0627\\u0644\\u0623\\u0633\\u0626\\u0644\\u0629 \\u0627\\u0644\\u0623\\u0643\\u062b\\u0631 \\u0634\\u064a\\u0648\\u0639\\u0627","item_slug":"\\u0627\\u0644\\u0623\\u0633\\u0626\\u0644\\u0629-\\u0627\\u0644\\u0623\\u0643\\u062b\\u0631-\\u0634\\u064a\\u0648\\u0639\\u0627","item_class":""}},{"level_data":{"item_name":"support","item_slug":"support","item_class":""}}]}]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `last_message` text NOT NULL COMMENT 'last message sent from admin',
  `message_viewed` tinyint(1) NOT NULL COMMENT 'if message seen or not',
  `submit_msg` tinyint(1) NOT NULL COMMENT 'if message has submitted from admin or not',
  `message_send` tinyint(1) NOT NULL COMMENT 'if message send success to user email',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`id`, `email`, `last_message`, `message_viewed`, `submit_msg`, `message_send`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ahmedbakr152@gmail.com', '', 0, 1, 0, '2016-08-17 22:00:00', '2017-04-03 09:39:55', NULL),
(2, 'test@test.com', '', 0, 0, 0, '2016-08-18 11:58:13', '2017-01-27 10:50:42', '2017-01-27 10:50:42'),
(3, 'aaaa@aaa.com', '', 0, 1, 0, '2017-02-13 18:33:51', '2017-04-03 09:39:55', NULL),
(4, 'sadasd@sdasd.com', '', 0, 1, 0, '2017-02-13 18:35:54', '2017-04-03 09:39:55', NULL),
(5, 'a@a.com', '', 0, 1, 0, '2017-03-21 12:18:31', '2017-04-03 09:39:55', NULL),
(6, 'admin@seoera.net', '', 0, 1, 0, '2017-04-03 09:39:22', '2017-04-03 09:39:55', NULL),
(7, 'sdfsdf@asdsd.com', '', 0, 0, 0, '2017-04-09 13:18:52', '2017-04-09 13:18:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `support_messages`
--

CREATE TABLE IF NOT EXISTS `support_messages` (
  `id` int(11) NOT NULL,
  `msg_type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tel` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `current_url` varchar(400) NOT NULL,
  `source` varchar(200) NOT NULL,
  `other_data` text NOT NULL,
  `trip_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `support_messages`
--

INSERT INTO `support_messages` (`id`, `msg_type`, `name`, `tel`, `email`, `message`, `current_url`, `source`, `other_data`, `trip_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '', 'a', 'sda', 'a@a.com', 'adasad', 'http://localhost/seoera/seoera/', 'adwards', '', 0, NULL, '2017-01-27 11:03:08', '2017-01-27 11:03:08'),
(3, '', 'abanoub', '012', 'abanoub.metyas.btm@gmail.com', '0d1asdadd', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(4, '', 'الاسم', 'dasd', 'abanoub.metyas.btm@gmail.com', 'sacsadas', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(5, '', 'abanoub', '01200941060', 'abanoub.metyas.btm@gmail.com', 'sdasd', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(6, '', 'seo', '01200941060', 'abanoub.metyas.btm@gmail.com', 'dasdas', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(7, '', 'seo', '01200941060', 'abanoub.metyas.btm@gmail.com', 'dasdas', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(8, '', 'abanoub', '1231', 'abanoub.metyas.btm@gmail.com', 'sadasdsadadad', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(9, '', 'afadsfa', 'dasdas', 'abanoub.metyas.btm@gmail.com', 'sdasddsadd', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(10, '', 'aa', 'asdad', 'aa@aa.com', 'dasda', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(11, '', 'dasd', 'ASDADS', 'a@a.com', 'SADA', 'http://localhost/seoera/seoera/', '', '', 0, NULL, '2016-08-18 12:04:05', '2016-08-18 12:04:05'),
(12, '', 'seo', '01200941060', 'a@a.com', 'sadasda', 'http://localhost/seoera/seoera/', '', '', 0, NULL, NULL, NULL),
(13, '', 'name', '0', 'email@email.com', 'message', '0', '', '', 0, NULL, NULL, NULL),
(14, '', 'name', 'tel', 'email@email.com', 'message', '0', '', '', 0, NULL, NULL, NULL),
(15, '', 'name', 'tel', 'email@email.com', 'message', '0', '', '', 0, NULL, NULL, NULL),
(16, '', 'name', 'tel', 'email@email.com', 'message', '0', '', '', 0, NULL, NULL, NULL),
(17, '', 'name', 'tel', 'email@email.com', 'message', '0', '', '', 0, NULL, NULL, NULL),
(18, '', 'name', 'tel', 'email@email.com', 'message', '0', '', '', 0, NULL, '2016-08-03 09:46:30', '2016-08-03 09:46:30'),
(19, '', 'aa', '123156', 'ahmedbakr152@gmail.com', 'test', 'http://www.seoera.net/demo/dmt_8/support', '', '', 0, '2016-10-04 09:26:51', '2016-10-04 09:26:51', NULL),
(20, '', 'abanoub metyas', '', 'abanoub.metyas.btm@gmail.com', '1200941060', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', '', 0, '2017-03-21 13:12:47', '2017-03-21 13:12:47', NULL),
(21, '', 'abanoub metyas', '', 'abanoub.metyas.btm@gmail.com', '1200941060', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', 'Array', 0, '2017-03-21 13:17:22', '2017-03-21 13:17:22', NULL),
(22, '', 'abanoub metyas', '', 'abanoub.metyas.btm@gmail.com', '1200941060', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', 'Array', 0, '2017-03-21 13:17:52', '2017-03-21 13:17:52', NULL),
(23, '', 'abanoub metyas', '', 'abanoub.metyas.btm@gmail.com', '1200941060', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', '{"fax":"","address":""}', 0, '2017-03-21 13:18:17', '2017-03-21 13:18:17', NULL),
(24, 'check_availability', 'abanoub metyas', '01200941060', 'abanoub.metyas.btm@gmail.com', 'fvxcvxvxvx', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', '{"fax":"","address":"cairo"}', 25, '2017-03-21 13:21:43', '2017-03-21 13:21:43', NULL),
(25, 'check_availability', 'abanoub metyas', '01200941060', 'abanoub.metyas.btm@gmail.com', 'fvxcvxvxvx', 'http://localhost/seoera/travel_corner/Travel-Packages/Egypt-Lake-Nasser-Cruises/new-trip', '', '{"fax":"asdsad","address":"cairo"}', 25, '2017-03-21 13:22:31', '2017-03-21 13:22:31', NULL),
(26, 'check_availability', 'Hossam testttttttttttttttttttttttttt', '010', 'admin@seoera.net', 'testing', 'http://www.seoera.net/demo/travel_corner/Travel-Packages/Egypt-Luxury-Tours/Luxury-Trip-one#chek', '', '{"fax":"0222","address":"222 frf"}', 31, '2017-04-03 09:36:45', '2017-04-03 09:36:45', NULL),
(27, 'support', 'asdaa', '', 'aaa@aaa.com', 'sdfsdf', 'http://localhost/seoera/cali_4_travel/contact', '', '{"fax":"","address":""}', 0, '2017-05-09 08:22:55', '2017-11-05 19:35:24', '2017-11-05 19:35:24'),
(28, 'support', 'aaasdsad', '23121', 'ahmedbakr152@gmail.com', 'asdasdasd', 'http://localhost/seoera/kuwait/contact', '', '{"fax":"","address":""}', 0, '2017-11-06 19:52:21', '2017-11-06 19:52:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL,
  `logo_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'not used in site',
  `user_can_login` tinyint(1) NOT NULL DEFAULT '1',
  `contacts` text COLLATE utf8_unicode_ci NOT NULL,
  `allowed_lang_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `allowed_champ_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `logo_id`, `email`, `username`, `full_name`, `role`, `user_type`, `related_id`, `password`, `remember_token`, `user_active`, `user_can_login`, `contacts`, `allowed_lang_ids`, `allowed_champ_ids`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'admin@admin.com', 'Super Admin', 'Admin', 'Administrator', 'dev', 0, '$2y$10$LK.gxcEoRgVDHKYG.wE6TO8N/3v4WwBpGZjehjFXUf/P8y6QPi3kK', 'mmAcXAslhgkBVtBsBLZmIGCYERAcrwzwfZyTOTfYwgAZUUzJpzfJ7RVcDvED', 1, 1, '', '["1","2","3"]', '["2","3","4","5","8","11","12","13","14","15"]', '2016-07-28 08:51:57', '2017-11-05 19:25:37', NULL),
(2, 0, 'ahmedbakr152@gmail.com', 'factory', 'Factory', 'Administrator', 'admin', 2, '$2y$10$sFCb1K2R2yJjT6MAW8vdg.79L6ID65TElao5L91uH4M2nF/uwH3si', 'xEgyggrVs88XHRxzuPwLB5oDRItEsiwvZiMg3F7cTh6z92z35SxEMhuacmNF', 1, 1, '', 'null', '', '2016-07-28 08:51:57', '2017-11-05 19:55:50', NULL),
(3, 1768, 'seo@seoera.net', 'seo@seoera.net', 'seo@seoera.net', '', 'admin', 0, '$2y$10$.lk0.LnCk6ZUBTUnpmN9.u1zcK4C6dL1nyG2uvxzvTfXFx3GAU0Tq', NULL, 1, 1, '', '["1"]', '', '2017-04-03 10:02:14', '2017-04-03 10:02:14', NULL),
(4, 1796, 'kmahdy88@gmail.com', 'karim', 'karim mahdy', '', 'admin', 0, '$2y$10$g7hpvTJT0pg1aMtA3X5.W.2kxhzgDdl10JDODPuRX1dSeL4qVKnby', 'l3lNEP6s40gL4WCY8c2gxNSqEJHYyioZDbsQyTbm7IimDWk3de6Z1nDyc6RO', 1, 1, '', '["1"]', '', '2017-04-13 16:31:02', '2017-04-13 16:33:27', '2017-04-13 16:33:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ad_img` (`ad_img`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `cat_type` (`cat_type`);

--
-- Indexes for table `category_translate`
--
ALTER TABLE `category_translate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `lang_id` (`lang_id`),
  ADD KEY `cat_name` (`cat_name`(255));

--
-- Indexes for table `currency_rates`
--
ALTER TABLE `currency_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generate_site_content_methods`
--
ALTER TABLE `generate_site_content_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`m_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`not_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `page_type` (`page_type`);

--
-- Indexes for table `pages_translate`
--
ALTER TABLE `pages_translate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`per_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `per_page_id` (`per_page_id`);

--
-- Indexes for table `permission_pages`
--
ALTER TABLE `permission_pages`
  ADD PRIMARY KEY (`per_page_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`set_id`);

--
-- Indexes for table `site_content`
--
ALTER TABLE `site_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Indexes for table `sortable_menus`
--
ALTER TABLE `sortable_menus`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_messages`
--
ALTER TABLE `support_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1866;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `category_translate`
--
ALTER TABLE `category_translate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `currency_rates`
--
ALTER TABLE `currency_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `generate_site_content_methods`
--
ALTER TABLE `generate_site_content_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `not_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `pages_translate`
--
ALTER TABLE `pages_translate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `per_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=210;
--
-- AUTO_INCREMENT for table `permission_pages`
--
ALTER TABLE `permission_pages`
  MODIFY `per_page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `set_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_content`
--
ALTER TABLE `site_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sortable_menus`
--
ALTER TABLE `sortable_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `support_messages`
--
ALTER TABLE `support_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
